# Docker Documentation

## Introduction to Docker

[Docker](https://www.docker.com/) is a platform designed to make it easier to create, deploy, and run applications by using containers. Containers allow developers to package up an application with all of its dependencies into a standardized unit for software development. This ensures that the application will run consistently across different computing environments.

## DockerFile

The DockerFile provided here defines the steps to build Docker images for different environments.

## Dev Environment

```DockerFile
# DockerFile for building Docker images for different environments

# Dev Environment node-alpine is a lighter docker image than node
FROM node:20-alpine as dev

# Define user and group IDs for Node.js user
ARG USER_ID=1000
ARG GROUP_ID=1000

# Install shadow package for usermod and groupmod commands
RUN apk --no-cache add shadow && \
    # Change the user ID of the 'node' user
    usermod -u ${USER_ID} node && \
    # Change the group ID of the 'node' user
    groupmod -g ${GROUP_ID} node

# Set the working directory for the application
WORKDIR /app

# Remove any existing 'node_modules' directories to ensure fresh dependencies
RUN find . -name 'node_modules' -type d -prune -exec rm -rf '{}' +

# Change ownership of the working directory to the 'node' user
RUN chown -R node:node /app

# Switch to the 'node' user
USER node

# Copy package.json and source code into the container
COPY package.json .
COPY . .

# Install dependencies using yarn
RUN yarn

# Expose the port defined by the environment variable VITE_PORT
EXPOSE ${VITE_PORT}

# Command to start the application in development mode
CMD ["yarn", "dev"]
```

This section of the DockerFile is responsible for setting up the development environment. It installs Node.js, sets up user permissions, installs dependencies, and exposes the specified port for local development. The application is started in development mode using yarn dev.

## Build Environment

```DockerFile
# Build Environment
FROM node:20-alpine as build
# Set the working directory for the application
WORKDIR /app
# Copy the built artifacts from the development environment
COPY --from=dev /app /app
# Build the application
RUN yarn build
# Expose the port defined by the environment variable VITE_PORT
EXPOSE ${VITE_PORT}
# Command to start the application in preview mode
CMD ["yarn", "preview"]
```

This section is for building the application. It copies the source code from the development environment, builds the application, and exposes the specified port for previewing the built code.

## Production Environment

```DockerFile
# Production Environment
# NGINX is used in the production environment to efficiently serve static files
# and handle HTTP requests, providing better performance, security, and scalability
# compared to Node.js as it's specifically designed for these tasks.
FROM nginx:stable-alpine as prod
# Copy the built artifacts from the build environment to NGINX server directory
COPY --from=build /app/dist /usr/share/nginx/html
# Copy NGINX configuration file. It solves reload page issues
COPY nginx.conf /etc/nginx/nginx.conf
# Command to start NGINX server
CMD ["nginx", "-g", "daemon off;"]
```

In the production environment, the built application is copied to an NGINX server container, and NGINX is configured to serve the application.

## Docker Compose

Docker Compose is a tool for defining and running multi-container Docker applications.

### docker-compose.yml

```yaml
services:
  app:
    container_name: react-seed
    image: react-seed-app-dev
    build:
      context: .
      target: dev # uses the dev section of the Dockerfile
      args:
        USER_ID: '1000'
        GROUP_ID: '1000'
    ports:
      - '${VITE_PORT}:${VITE_PORT}' # Map VITE_PORT from host to container
    volumes:
      - $PWD:/app # Mount current directory into the container
    environment:
      - NODE_ENV=development # Set NODE_ENV environment variable to development
    env_file:
      - .env # Load environment variables from .env file
    restart: unless-stopped
```

This docker-compose file defines the service for the development environment. It builds the Docker image using the DockerFile's dev target, sets up port mapping for local development, mounts the current directory into the container for live code reloading, sets environment variables, and specifies a restart policy.

### docker-compose-build.yml

```yaml
services:
  app:
    container_name: react-seed-build
    image: react-seed-app-build
    build:
      context: .
      target: build
    ports:
      - '3020:3019' # Expose the application on port 3020 of the local machine
    restart: unless-stopped
```

This file is used for building the application. It builds the Docker image using the DockerFile's build target and exposes the application on port 3020 of the local machine for testing the minified code.

### docker-compose.prod.yml

```yaml
services:
  app:
    container_name: react-seed-prod
    image: react-seed-app-prod
    build:
      context: .
      target: prod
    restart: unless-stopped
```

For production deployment, this file builds the Docker image using the DockerFile's prod target and sets up a container named react-seed-prod with the application exposed on port 80, which is the default port for accessing the server.

## Testing Production Build Locally

To test the production build locally, add the following line to the docker-compose.prod.yml:

```yaml
ports:
  - '8080:80'
```

This exposes the application on port 8080 of the local machine.

## Docker Image Tagging

To tag the Docker image, use the following commands:

```bash
docker build -t react-seed:1.0.0 . # Change the version number as desired
docker run -d -p 80:80 --name react-server react-seed
```

This builds the Docker image with the specified tag and runs it in detached mode, exposing port 80 of the container on port 80 of the host machine.

## Use pre-written scripts

To make builds and tags easier, you can use the app.sh and tag.sh scripts as following :

```bash
./app.sh [OPTIONS] action

./app.sh up # starts the dev environment
./app.sh -b up # starts the build environment
./app.sh -p up # starts the prod environment

./app.sh down
./app.sh -b down
./app.sh -p down

./app.sh --help
```

```bash
./tag.sh tag

./tag.sh 1.2.6 # creates and pushes the 1.2.6 tag version
```

Be sure you edited the "dockername" in the tag and ensure you are logged it with `docker login` to push the created image
