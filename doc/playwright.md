# Playwright Tests Documentation

### Overview

Playwright is a powerful end-to-end testing library that allows you to automate interactions with web browsers. It provides a robust set of APIs for automating tasks such as clicking, typing, navigating, and asserting page content. Playwright supports multiple browsers including Chromium, Firefox, and WebKit, and allows you to run tests in parallel across different browser environments.

This documentation provides information on how to run and manage Playwright tests for your project.

### Configuration File

The [configuration file](../playwright.config.ts) is located at the root of your project. It defines the following settings:

- Test Directory: Specifies the directory containing your tests (/playwright-tests).
- Local Project URL: Specifies the URL of the project in your local environment. Note that if you change the port in the .env file, ensure it reflects here.
- Browsers: Specifies the browsers on which the tests will be executed. Currently configured browsers are Chromium and Firefox.

### Test Organization

All tests are organized within the /playwright-tests directory and are further organized by views.

### Running Tests

To run the tests, navigate to the root of your project and execute the command:

```sh
yarn p-test
```

This command will execute all tests.

You can also run tests from a specific directory by using the following command:

```sh
yarn p-test:folder <folder-name>
```

For example, yarn p-test:folder home will execute tests located in the /playwright-tests/features/home directory.

## Launching the Test GUI

Playwright provides a graphical user interface (GUI) for running and interacting with tests. To launch the test GUI, you can use the following command:

```sh
yarn p-test:gui
```

### Reporting

Playwright provides a reporting tool. You can generate a visual report in your default browser by using the command:

```sh
yarn p-test:report
```

This report will automatically open in case of errors and can be manually accessed if all tests pass.
