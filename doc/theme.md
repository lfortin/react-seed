# Theme Management Documentation

### Introduction

The application theme management is handled through a combination of CSS files and a colors library. This documentation provides insights into the structure and processes involved in managing themes.

### Common Theme Colors

Colors common to all themes are declared in the [theme.css](/src/css/theme.css) file using the :root selector. All common theme variables are declared within this block.

```css
:root {
	/_ declare all common theme variables _/
}
```

### Specific Theme Colors

Colors specific to each theme are defined in the [colors.ts](/src/libs/colors.ts) librairy. This file includes:

- The possible values for the Theme :

```typescript
export const LIGHT: string = 'LIGHT';
export const DARK: string = 'DARK';

export type Theme = typeof LIGHT | typeof DARK;
export const defaultTheme = DARK; // save the planet, use dark mode
```

- The IColor type, defining specific colors for each theme.
- The colorsTab constant, defining the global variable names for each theme attribute. For example: backgroundColor: "--background-color".
- The lightColors and darkColors constants, specifying the values assigned to the variables. For example:

```typescript
type IColor {
  backgroundColor: string;
}
const colorsTab: Record<keyof IColor, string> = {
  backgroundColor: "--background-color",
};
const lightColors: IColor = {
  backgroundColor: "#ffffff",
};
const darkColors: IColor = {
  backgroundColor: "#000000",
};
```

To add or remove a global theme variable, edit the IColor type, and adjust the colorsTab, lightColors, and darkColors constants accordingly.

### Theme Changing Functionality

The function changeTheme is responsible for changing the application theme. It takes the theme name as a parameter, sets the new colors, and updates the HTML document with the new global variables corresponding to the color variables. This function is designed to be a part of the core theme management and should not be modified under normal circumstances.

### ThemeSelector Component

A [ThemeSelector](/src/tsx/Components/ThemeSelector/ThemeSelector.tsx) component is provided in for changing the theme within the application. The rendering of this component can be edited, and the switchTheme function handles the theme change process. It performs the following steps:

- Determines the new theme (LIGHT if the previous theme was DARK, or vice versa).
- Records this new value in the useThemeStateStore store (cf [store.md](store.md)).
- Calls the changeTheme function from the colors library mentioned above.

### Accessing Theme Colors

To access a theme color in a CSS file, use the following structure:

```css
color: var(--background-color);
```

The variable names are the values defined in the colorsTab object within the colors library.

### Viewing Theme Variables

All theme variables defined above are visible in your browser's inspector under the "Elements" tab.
