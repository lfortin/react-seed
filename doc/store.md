# Store Usage Documentation

This documentation provides guidelines for using stores in the application. The application aims to be as generic as possible, implementing two types of stores, each with its advantages and disadvantages: [Zustand](https://docs.pmnd.rs/zustand/getting-started/introduction) and [Redux](https://react-redux.js.org/). Fell free to use both or to remove one, according to your needs.

**WARNING** As descibed in [tooltips.md](tooltips.md), the provider custom Tooltip component uses the redux store. If you want to use zustand only, make sure to move all the redux logic into a zustand store or use an external library

## Zustand Store

Zustand is simpler to use, with a better learning curve, but offers fewer options compared to Redux. It is generally recommended for smaller applications. For a comparison between Zustand and Redux, refer to [Zustand vs Redux Comparison](https://www.frontendmag.com/insights/zustand-vs-redux-comparison/).

### Store Definition

Zustand is used for global variables such as theme (cf [theme](theme.md)), language (cf [translation](translation.md)), webpage title, etc. Stores are defined in the `/src/store` directory.

`/src/store/Value.ts`

```typescript
import { create } from "zustand";

type ValueState {
  value: string;
  setValue: (newValue: string) => void;
}

export const useValueStateStore = create<ValueState>(
  (set): ValueState => ({
    value: "",
    setValue: (newValue: string) => set({ value: newValue }),
  })
);
```

Any component or library can import this store, use its value, or update it:

```typescript
const { value, setValue } = useValueStateStore();

const onUpdate = (val: string) => setValue(val);
return <div onClick={() => onUpdate(value + 'a')}>{value}</div>;
```

## Redux Store

Redux is used for the rest of the application, providing more complex state management and features like ReduxDevTools browser extensions. It is more challenging to learn initially but offers comprehensive capabilities. Moreover, the most difficult part is to set up the technology in your application, which is, is this case, already done.

### Action Definition

Actions are defined in [actionTypes.ts](/src/actionTypes.ts):

```typescript
export type Action = {
  type: string;
  payload: any;
  values: any;
};
export const SET_VALUES = 'SET_VALUES';
```

These actions are imported into action files (/src/actions/values.ts) and reducers (/src/reducer/values.ts).

The action file defines functions that use actionTypes:

```typescript
import { SET_VALUES } from '../actionTypes';
export const addValues = (values: Array<any>) => ({
  type: SET_VALUES,
  payload: {
    values,
  },
});
```

### Reducer Definition

Reducers are defined in /src/reducers/values.ts

```typescript
import { SET_VALUES } from '../actionTypes';

const initialStateCollections = {};

/**
 * In a state, it could be interessting to store all the values in the "collection" attribut.
 * This way, you are able to add a "filters" attribute or anything else. It helps the selector to do its job : (getAllValues, getFilters) and return the filtered values.
 * So if you clear the filter, you do not need to recalculate or refetch all the values : they are still stored, just not selected.
 * A good example of a structure is the following :
 * {
 *    collection : {
 *      id1: {
 *        id: id1,
 *        name: 'first value'
 *      },
 *      id2: {
 *        id: id2,
 *        name: 'last value'
 *      },
 *    },
 *    filters: {
 *      text: '',
 *      page: 1,
 *      limit: 10
 *    }
 *  }
 * Storing all data by an object with unique key, instead of an array allows to access data by its id with the following structure :
 * `const value = state.collection[id1]` instead of using a complex array.find function.
 */
const initialState: ValuesState = {
  collection: { ...initialStateCollections },
};

const reducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case SET_VALUES: {
      const { values }: { values: any } = action.payload;
      return {
        ...state,
        collection: {
          ...values,
        },
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
```

### Store Integration

Reducers are declared in the [index.ts](/src/reducers/index.ts) file, providing the global state to the entire application. To add a new state store, here are the steps to follow :

- Create its id in the [reduxTypes.ts](/src/constants/reduxTypes.ts) file

```typescript
export const VALUE = 'VALUE';
```

- Add the storage key in the storeKey (e.g 'id' or 'name'); Then import the id in the [index.ts](/src/reducers/index.ts) file.
- Create the reducer as seen previously, and import it, with its type, in the [index.ts](/src/reducers/index.ts) file.

```typescript
import values, { ValuesState } from './values';
```

- Declare the new state in the StateInterface type, the StoreInterface and the global reducer:

```typescript
export type StateInterface {
  // Other states
  [VALUE]: ValuesState;
}

export type StoreInterface {
  // Other stores
  [VALUE]: (state: ValuesState, action: Action) => ValuesState;
}

const reducers: ReducersMapObject<StateInterface, Action> = {
  // Other states
  [VALUE]: values,
};
```

The global reducer is generated using `combineReducers` and imported into the application in the root index.tsx file. You do not need to edit this part :

```typescript
import { Provider } from "react-redux";
import { store } from "./reducers";
...

root.render(
  <BrowserRouter>
    <Provider store={store}>
      <AppRouter />
    </Provider>
  </BrowserRouter>
);
```

### Selecting Values

To select a value from the store, connect the component to the global store and call the available selectors in /src/selectors/values.ts.

```typescript
import { createSelector } from 'reselect';
import { VALUE } from '../constants/reduxTypes';
import { StateInterface } from '../reducers';

const _values = (state: StateInterface) => state[VALUE].collection;
const _id = (_state: StateInterface, props: any) => props.id;

export const selectValues = createSelector(_values, (values) => Object.values(values));

export const selectValue = createSelector([_values, _id], (values, id) => values[id]);
```

The selector can be used in a React component, and its value can be injected via a connector:

```typescript
import { FC } from 'react';
import { connect } from 'react-redux';
import { StateInterface } from '../../../reducers';
import { Value, selectValue } from '../../../selectors/values';

const ValueContainer: FC<Props> = ({ value, id }) => (
  <div>
    Value for id {id} : {value}
  </div>
);

type Props = {
  id: string;
  value?: Value;
};

const mapStateToProps = (state: StateInterface, props: Props) => ({
  value: selectValue(state, props),
});

export default connect(mapStateToProps)(ValueContainer);
```

### Updating the Store

To update the store, use the useDispatch function from react-redux within a React component:

```typescript
import { useDispatch } from 'react-redux';
import { Dispatch } from 'redux';
const dispatch: Dispatch = useDispatch();
```

This dispatch function is automatically passed into the props of a component that connects to it via the connect function. In such a case, you can declare:

```typescript
const ValueContainer: FC<Props> = ({ value, id, dispatch }) => (
  <div>
    Value for id {id} : {value}
  </div>
);

type Props = {
  id: string;
  value?: Value;
  dispatch: Dispatch;
};
```

In cases where access is needed in a file that is not a React component, a library has been created for this purpose: [dispatcher.ts](/src/libs/utils/dispatcher.ts):

```typescript
import { Dispatch } from 'redux';

type Dispatcher = {
  dispatch: Dispatch<any> | null;
};

const dispatcher: Dispatcher = {
  dispatch: null,
};

export default dispatcher;
```

The dispatch value is loaded once by the [AppRouter](/src/AppRouter.tsx) component and is then accessible from any file:

```typescript
import dispatcher from '/src/libs/utils/dispatcher';
const { dispatch } = dispatcher;
dispatch && dispatch(setValues(values));
```

This structure ensures a scalable and efficient state management system in the application. Adjust the paths and details based on your project structure and specific implementation.
