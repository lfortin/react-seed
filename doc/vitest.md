# Overview

[Vitest](https://vitest.dev/) is a simple and effective unit testing framework that allows you to write and execute tests for your JavaScript or TypeScript code. It facilitates the creation of modular test suites and quick execution. With Vitest, you can write clear and concise tests to verify the expected behavior of your functions and modules.

This documentation provides information on how to write and manage unit tests with Vitest for your project.

## Configuration

Vitest configuration is minimal, and tests can be organized according to the folder structure of your choice. The current [configuration file](../vitest.config.ts) is located in the root folder.

## Test Structure

In unit-testing, test files are typically located adjacent to the files they are testing. For example, if you have a file named [Home.tsx](../src/tsx/Views/Home/Home.tsx), its corresponding test file [Home.test.tsx](../src/tsx/Views/Home/Home.test.tsx) would be located in the same directory (Home). This practice helps maintain a clear and intuitive organization of tests, as they are closely associated with the modules or components they are testing.

## Running Tests

To run all unit tests, you can use the following command at the root of your project:

```sh
yarn test
```

This command will run all tests once.

## Automatic Test Reloading

Vitest supports automatic reloading of tests when files are saved. This means that whenever you modify and save a test file or a source file, Vitest will detect the changes and automatically rerun the relevant tests.

## Reporting

Vitest provides simple yet useful reports on the status of tests. These reports are typically displayed in the console after test execution and provide information on tests that passed, failed, or are pending.

## Launching the Test GUI

Vitest provides a graphical user interface (GUI) for running and interacting with tests. To launch the test GUI, you can use the following command:

```sh
yarn test:gui
```

This command will open a graphical interface in your default web browser, allowing you to view and interact with your tests visually. You can use the GUI to run individual tests, view test results, and navigate through your test suite.

## Calculating Code Coverage

Code coverage analysis is essential for evaluating the effectiveness of your tests and identifying areas of your codebase that lack test coverage. Vitest supports calculating code coverage using tools like istanbul or nyc. To calculate code coverage for your project, you can use the following command:

```sh
yarn test:coverage
```

This command will generate a code coverage report for your project, indicating which parts of your code are covered by tests and which are not. You can use this report to assess the quality of your test suite and improve test coverage where necessary.

This documentation should help you understand how to write and execute unit tests with Vitest for your project. If you need more information or assistance, feel free to ask!
