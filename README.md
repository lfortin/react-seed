# React Application Documentation

## Overview

This React application, developed using [Vite.js](https://vitejs.dev/), serves as an open-source boilerplate for creating a public utility bank. It aims to provide an easily reusable template for launching projects with pre-configured settings including TypeScript, [Redux](https://redux.js.org/), Router, and [Docker](https://www.docker.com/).

## React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Installation

To install the application, follow these steps:

Ensure you have [Node.js](https://nodejs.org/) version 18 or higher installed (see [nvm](https://github.com/nvm-sh/nvm)) and Docker in its latest version.

You can use `nvm use` to set the Node.js version to 20 if needed.

Run `yarn` to install dependencies.

Run `docker compose up -d` to start the Docker container.

The container is configured to restart automatically whenever the machine restarts. You only need to run this command once, unless you manually remove the container or stop it using `docker compose down`.

## Building and Running

### Development Build

To build and run the application in development mode, use the following command:

```bash
docker compose up -d
```

you can use the app.sh script as an alternative :

```bash
./app.sh up
```

### Production Build

For a production build, use the following command:

```bash
docker compose -f docker-compose.prod.yml up --build -d
```

If you already have built the docker image, you can use the app.sh script as an alternative :

```bash
./app.sh -p up
```

This command deploys a production-ready version of the application. Refer to the Docker documentation in the /doc folder for deploying a production version locally.

### Preview Build

To access a preview (production build, deployed locally) version of the application, run the following command:

```bash
docker compose -f docker-compose.build.yml up --build -d
```

If you already have built the docker image, you can use the app.sh script as an alternative :

```bash
./app.sh -b up
```

The preview will be available at http://localhost:3020 and it uses a minified version of your javascript and css code.

If you have any issue on understanding how docker works on this projet, please refer to the [documentation](doc/docker.md)

## Testing

### End-to-End Tests

End-to-end tests have been written using [Playwright](https://playwright.dev/). To install Playwright, use the command:

```bash
yarn playwright install
```

Once installed, run the tests using:

```bash
yarn p-test
```

Refer to the [playwright documentation](doc/playwright.md) documentation for Playwright for more information on running specific tests or visualizing results in the browser.

### Unit Tests

Unit tests have been written using [Vitest](https://vitest.dev/). Run the unit tests with the command:

```bash
yarn test
```

Refer to the [vitest documentation](doc/vitest.md) for more information on running and managing unit tests.

## Documentation

Documentation is available in the /doc folder, providing information on the technologies used and how to navigate the application and its tools. If you encounter any issues, feel free to submit a request on the GitLab repository.

## Checking the rendering efficiency

In order to check if your React components do no re-render too often, you can use the React Developer Tools add-on

- [Chrome](https://chromewebstore.google.com/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
- [Firefox](https://addons.mozilla.org/fr/firefox/addon/react-devtools/)

Too Activate the highlights on rendering, please follow these steps :

- Inspect the web page (right click -> inspect or F12)
- Go to the Components tab of your inspector
- In the settings check the "Highlight updates when components render." option

## Checking Performance with Lighthouse

To assess the performance of your React application and ensure optimal user experience, you can utilize Lighthouse, an open-source tool from Google. Lighthouse evaluates various aspects of web pages, including performance, accessibility, SEO, and more.

- [Chrome](https://chromewebstore.google.com/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk)
- [Firefox](https://addons.mozilla.org/fr/firefox/addon/google-lighthouse/)

### How to Use Lighthouse:

- Accessing Lighthouse: Lighthouse can be accessed through the navigator's Developer Tools.
- Launching Lighthouse: Open Developer Tools (right click -> inspect or F12), then navigate to the Audits tab.
- Initiating Audit: Click on "Perform an audit..." and select the categories you want to audit. For performance assessment, make sure to include "Performance."
- Running the Audit: Click "Run audits" to initiate the evaluation process.
- Reviewing Results: Once the audit completes, Lighthouse generates a report with scores and suggestions for improvement in various areas, including performance.

### Objectives of Lighthouse:

- Performance: Lighthouse evaluates the loading performance of your web application, identifying areas where optimizations can be made to reduce loading times and improve user experience.
- Accessibility: It assesses the accessibility of your application, highlighting potential barriers for users with disabilities and suggesting enhancements to ensure inclusivity.
- SEO: Lighthouse examines your site's SEO-friendliness, providing recommendations to improve its visibility and ranking in search engine results.
- Best Practices: It checks for adherence to web development best practices, ensuring your application follows recommended standards for security, performance, and maintainability.

By leveraging Lighthouse, you can gain valuable insights into the performance of your React application and take proactive steps to enhance its efficiency and user satisfaction.
