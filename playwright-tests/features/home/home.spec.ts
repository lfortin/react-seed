import { test, expect } from '@playwright/test';
import { intl } from '../../../src/libs/i18n/initReactIntl';

test.beforeEach(async ({ page }) => {
  await page.goto('/');
});

test.describe('App title', () => {
  test('Should be defined', async ({ page }) => {
    const pageTitle = intl.formatMessage({ id: 'title.home' });
    await expect(page).toHaveTitle(pageTitle);
  });
});

test.describe('Searchbox', () => {
  test('Should be displayed', async ({ page }) => {
    const searchbox = page.getByTestId('home-searchbox');
    await expect(searchbox).toBeVisible();
  });
});

test.describe('loader', () => {
  test('Should be displayed', async ({ page }) => {
    const spinner = page.locator('.spinner');
    await expect(spinner).toBeVisible();
  });

  test('Should be displayed and then disapear', async ({ page }) => {
    const spinner = page.locator('.spinner');
    await expect(spinner).toBeVisible();
    await expect(spinner).toBeHidden();
  });
});

test.describe('List', () => {
  test('Should be displayed', async ({ page }) => {
    const list = page.locator('.list');
    await expect(list).toBeVisible();
  });

  test('Should contain 50 elements', async ({ page }) => {
    const listSelector = '.list';
    const list = page.locator(listSelector);
    const items = await list.locator('.item').count();
    expect(items).toBeLessThanOrEqual(50); // To fix, it should be 50 be virtual list hides some elements
  });
});
