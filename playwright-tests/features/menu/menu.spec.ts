import { test, expect } from '@playwright/test';
import { hexToRgb } from '../../utils/colors';
import { EN, FR, LocaleType, intl } from '../../../src/libs/i18n/initReactIntl';
import en from '../../../src/libs/i18n/en.json' assert { type: 'json' };
import fr from '../../../src/libs/i18n/fr.json' assert { type: 'json' };

test.beforeEach(async ({ page }) => {
  await page.goto('/');
});

test.describe('Layout', () => {
  test('Should be displayed', async ({ page }) => {
    const menu = page.locator('.menu');
    await expect(menu).toBeVisible();
  });

  test('Should collapse and open', async ({ page }) => {
    const menu = page.locator('.menu');
    const collapseIcon = menu.locator('.title').locator('.icon').locator('.app-icon');

    await expect(collapseIcon).toBeVisible();
    await expect(menu).toHaveClass(/open/);

    await collapseIcon.click();
    const burgerIcon = menu.locator('.title').locator('.icon').locator('.app-icon');
    await expect(burgerIcon).toBeVisible();
    await expect(menu).not.toHaveClass(/open/);

    await burgerIcon.click();
    await expect(menu).toHaveClass(/open/);
  });

  test('Should contain links', async ({ page }) => {
    const menu = page.locator('.menu');
    const links = menu.locator('.menu-links');
    await expect(links).toBeVisible();
    await expect(links).toHaveClass(/grow/);
  });

  test('Should contain footer', async ({ page }) => {
    const menu = page.locator('.menu');
    const footer = menu.locator('.menu-bottom');
    await expect(footer).toBeVisible();
  });
});

test.describe('Navigation', () => {
  test('Virtual list item should be default active', async ({ page }) => {
    const menu = page.locator('.menu');
    const vlLink = menu.getByTestId('to-vl');
    await expect(vlLink).toHaveClass(/active/);
  });

  test('Navigate to tabbed view', async ({ page }) => {
    const menu = page.locator('.menu');
    const vlLink = menu.getByTestId('to-vl');
    const targetLink = menu.getByTestId('to-tab');
    await expect(targetLink).not.toHaveClass(/active/);
    await expect(page).toHaveURL(/home/);

    await targetLink.click();
    const pageContent = page.locator('.app-content');

    await expect(vlLink).not.toHaveClass(/active/);
    await expect(targetLink).toHaveClass(/active/);
    await expect(page).toHaveURL(/tab-/);
    expect(pageContent).toBeDefined();
    await expect(pageContent).toBeVisible();
  });

  test('Navigate to form view', async ({ page }) => {
    const menu = page.locator('.menu');
    const vlLink = menu.getByTestId('to-vl');
    const targetLink = menu.getByTestId('to-form');
    await expect(targetLink).not.toHaveClass(/active/);
    await expect(page).toHaveURL(/home/);

    await targetLink.click();
    const pageContent = page.locator('.app-content');

    await expect(vlLink).not.toHaveClass(/active/);
    await expect(targetLink).toHaveClass(/active/);
    await expect(page).toHaveURL(/form/);
    expect(pageContent).toBeDefined();
    await expect(pageContent).toBeVisible();
  });

  test('Navigate to notification view', async ({ page }) => {
    const menu = page.locator('.menu');
    const vlLink = menu.getByTestId('to-vl');
    const targetLink = menu.getByTestId('to-notif');
    await expect(targetLink).not.toHaveClass(/active/);
    await expect(page).toHaveURL(/home/);

    await targetLink.click();
    const pageContent = page.locator('.app-content');

    await expect(vlLink).not.toHaveClass(/active/);
    await expect(targetLink).toHaveClass(/active/);
    await expect(page).toHaveURL(/notifications/);
    expect(pageContent).toBeDefined();
    await expect(pageContent).toBeVisible();
  });

  test('Navigate to system view', async ({ page }) => {
    const menu = page.locator('.menu');
    const vlLink = menu.getByTestId('to-vl');
    const targetLink = menu.getByTestId('to-system');
    await expect(targetLink).not.toHaveClass(/active/);
    await expect(page).toHaveURL(/home/);

    await targetLink.click();
    const pageContent = page.locator('.app-content');

    await expect(vlLink).not.toHaveClass(/active/);
    await expect(targetLink).toHaveClass(/active/);
    await expect(page).toHaveURL(/system/);
    expect(pageContent).toBeDefined();
    await expect(pageContent).toBeVisible();
  });

  test('Navigate to tips view', async ({ page }) => {
    const menu = page.locator('.menu');
    const vlLink = menu.getByTestId('to-vl');
    const targetLink = menu.getByTestId('to-tips');
    await expect(targetLink).not.toHaveClass(/active/);
    await expect(page).toHaveURL(/home/);

    await targetLink.click();
    const pageContent = page.locator('.app-content');

    await expect(vlLink).not.toHaveClass(/active/);
    await expect(targetLink).toHaveClass(/active/);
    await expect(page).toHaveURL(/thank-you/);
    expect(pageContent).toBeDefined();
    await expect(pageContent).toBeVisible();
  });
});

test.describe('Footer', () => {
  test('Should contain the locale selector', async ({ page }) => {
    const footer = page.locator('.menu').locator('.menu-bottom');
    const localeSelector = footer.locator('.locale-selector');
    await expect(localeSelector).toHaveClass(/select/);
  });

  test('Should be able to change the locale', async ({ page }) => {
    const linkTranslation = (locale: LocaleType): string =>
      locale === EN ? en['pages.virtualList'] : fr['pages.virtualList'];

    const vlLink = page.locator('.menu').getByTestId('to-vl');
    const localeSelector = page
      .locator('.menu')
      .locator('.menu-bottom')
      .locator('.locale-selector');
    const targetLanguage = localeSelector.locator('option', {
      hasText: intl.formatMessage({ id: 'locale.fr' }),
    });

    await expect(vlLink).toHaveText(linkTranslation(EN));
    await expect(targetLanguage).toBeHidden();

    await localeSelector.selectOption({ value: FR });
    await expect(vlLink).toHaveText(linkTranslation(FR));
  });

  test('Should contain the theme selector', async ({ page }) => {
    const footer = page.locator('.menu').locator('.menu-bottom');
    const themeSelector = footer.locator('.theme-selector');
    await expect(themeSelector).toBeVisible();
  });

  test('Should be able to change the theme', async ({ page }) => {
    const darkBG = '#1a1a1a';
    const darkColor = '#ffffff';
    const lightBG = '#f5f5f5';
    const lightColor = '#333333';

    const app = page.locator('.app-wrapper');
    const themeSelector = page.locator('.menu').locator('.menu-bottom').locator('.theme-selector');

    await expect(app).toHaveCSS('background-color', hexToRgb(darkBG));
    await expect(app).toHaveCSS('color', hexToRgb(darkColor));

    await themeSelector.click();
    await expect(app).toHaveCSS('background-color', hexToRgb(lightBG));
    await expect(app).toHaveCSS('color', hexToRgb(lightColor));
  });
});
