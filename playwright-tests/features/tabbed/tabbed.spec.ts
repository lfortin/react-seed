import { test, expect } from '@playwright/test';
import { intl } from '../../../src/libs/i18n/initReactIntl';

test.beforeEach(async ({ page }) => {
  await page.goto('/tab-alpha');
});

test.describe('alpha-tab', () => {
  test('Should be active', async ({ page }) => {
    const tabs = page.locator('.all-tabs').locator('.tabs');
    await expect(tabs).toBeVisible();

    const tabTitle = intl.formatMessage({ id: 'tabs.tabbed.tab_alpha' }).toUpperCase();
    const tab = tabs.locator('.tab').getByText(tabTitle);

    await expect(tab).toBeVisible();
    await expect(tab).toHaveClass(/tab/);
    await expect(tab).toHaveClass(/active/);
  });

  test('title', async ({ page }) => {
    const pageTitle = intl.formatMessage({ id: 'title.tabbed.alpha' });
    await expect(page).toHaveTitle(pageTitle);
  });

  test('content', async ({ page }) => {
    const content = page.locator('.tab-view');
    await expect(content).toHaveClass(/tab_alpha/);

    const contentMessage = content.locator('.message');
    const pageText = intl.formatMessage({ id: 'tabbed.message.from_parent' }, { message: 'toto' });
    await expect(contentMessage).toHaveText(pageText);
  });
});

test.describe('beta-tab', () => {
  test('Should not be active', async ({ page }) => {
    const tabs = page.locator('.all-tabs').locator('.tabs');
    await expect(tabs).toBeVisible();

    const tabTitle = intl.formatMessage({ id: 'tabs.tabbed.tab_beta' });
    const tab = tabs.locator('.tab').getByText(tabTitle);

    await expect(tab).toBeVisible();
    await expect(tab).toHaveClass(/tab/);
    await expect(tab).not.toHaveClass(/active/);
    await expect(tab).toHaveClass(/false/);
  });

  test('title', async ({ page }) => {
    const tabs = page.locator('.all-tabs').locator('.tabs');
    const tabTitle = intl.formatMessage({ id: 'tabs.tabbed.tab_beta' });
    const tab = tabs.locator('.tab').getByText(tabTitle);
    await tab.click();

    const pageTitle = intl.formatMessage({ id: 'title.tabbed.beta' });
    await expect(page).toHaveTitle(pageTitle);
  });

  test('content', async ({ page }) => {
    const tabs = page.locator('.all-tabs').locator('.tabs');
    const tabTitle = intl.formatMessage({ id: 'tabs.tabbed.tab_beta' });
    const tab = tabs.locator('.tab').getByText(tabTitle);
    await tab.click();

    const content = page.locator('.tab-view');
    await expect(content).toHaveClass(/tab_beta/);

    const contentMessage = content.locator('.message');
    const pageText = intl.formatMessage({ id: 'tabbed.message.from_parent' }, { message: 'tata' });
    await expect(contentMessage).toHaveText(pageText);
  });
});

test.describe('omega-tab', () => {
  test('Should be disabled', async ({ page }) => {
    const tabs = page.locator('.all-tabs').locator('.tabs');
    await expect(tabs).toBeVisible();

    const tabTitle = intl.formatMessage({ id: 'tabs.tabbed.tab_omega' });
    const tab = tabs.locator('.tab').getByText(tabTitle);

    await expect(tab).toBeVisible();
    await expect(tab).toHaveClass(/tab/);
    await expect(tab).toHaveClass(/disabled-tab/);
  });
});
