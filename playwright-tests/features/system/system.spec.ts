import { test, expect, chromium } from '@playwright/test';
import { intl } from '../../../src/libs/i18n/initReactIntl';

const browserRegex =
  /(Google Chrome|Mozilla Firefox|Safari|Opera|Microsoft Edge|Internet Explorer|unknown)/;
const osRegex = /(Windows|MacOS|Android|iOS|Linux|unknown)/;

test.beforeEach(async ({ page }) => {
  await page.goto('/system');
});

test.describe('Layout', () => {
  test('Two blocks should be visible', async ({ page }) => {
    const lsBlock = page.locator('.sys-container').locator('.fields-container');
    await expect(lsBlock).toBeVisible();
    const systemBlock = page.locator('.sys-container').locator('.system-container');
    await expect(systemBlock).toBeVisible();
  });

  test('LocalStorage block content', async ({ page }) => {
    const lsBlock = page.locator('.sys-container').locator('.fields-container');

    const valueBlock = lsBlock.locator('.field').nth(0);
    const valueContents = valueBlock.locator('span');
    await expect(valueContents.nth(0)).toHaveClass(/header/);
    await expect(valueContents.nth(1)).toHaveClass(/content/);

    const inputBlock = lsBlock.locator('.field').nth(1);
    const inputHeader = inputBlock.locator('.header');
    const inputField = inputBlock.locator('.app-input-text');
    await expect(inputHeader).toBeVisible();
    await expect(inputField).toBeVisible();

    const actionBlock = lsBlock.locator('.field').nth(2);
    const actionHeader = actionBlock.locator('.header');
    const actionButtons = actionBlock.locator('.content').locator('.app-button');
    await expect(actionHeader).toBeVisible();
    await expect(actionButtons).toHaveCount(2);

    const clearAllButton = page.locator('.ls-clear-all');
    await expect(clearAllButton).toBeVisible();
  });

  test('System block content', async ({ page }) => {
    const systemBlock = page.locator('.sys-container').locator('.system-container');
    const contentRows = systemBlock.locator('span');
    await expect(contentRows).toHaveCount(2);

    const browserRow = systemBlock.getByTestId('browser');
    await expect(browserRow).toBeVisible();

    const osRow = systemBlock.getByTestId('os');
    await expect(osRow).toBeVisible();
  });
});

test.describe('Local storage', () => {
  test('The initial local storage content should be empty', async ({ page }) => {
    const lsBlock = page.locator('.sys-container').locator('.fields-container');
    const valueBlock = lsBlock.locator('.field').nth(0);
    const valueContent = valueBlock.locator('.content');
    await expect(valueContent).toHaveText('');
  });

  test('Should be able to set new local storage data', async ({ page }) => {
    const lsBlock = page.locator('.sys-container').locator('.fields-container');
    const valueBlock = lsBlock.locator('.field').nth(0);
    const inputBlock = lsBlock.locator('.field').nth(1);
    const actionBlock = lsBlock.locator('.field').nth(2);
    const valueContent = valueBlock.locator('.content');
    const inputField = inputBlock.locator('.app-input-text').locator('input');
    const setButton = actionBlock
      .locator('.content')
      .locator('.app-button', { hasText: intl.formatMessage({ id: 'system.set' }) });

    await expect(inputField).toBeVisible();
    await expect(setButton).toBeVisible();

    const storedData = 'my-stored-data';
    await inputField.fill(storedData);
    await setButton.click();

    await expect(valueContent).toHaveText(storedData);
  });

  test('Should be able to clear local storage data', async ({ page }) => {
    const lsBlock = page.locator('.sys-container').locator('.fields-container');
    const valueBlock = lsBlock.locator('.field').nth(0);
    const inputBlock = lsBlock.locator('.field').nth(1);
    const actionBlock = lsBlock.locator('.field').nth(2);
    const valueContent = valueBlock.locator('.content');
    const inputField = inputBlock.locator('.app-input-text').locator('input');
    const setButton = actionBlock
      .locator('.content')
      .locator('.app-button', { hasText: intl.formatMessage({ id: 'system.set' }) });
    const clearButton = actionBlock
      .locator('.content')
      .locator('.app-button', { hasText: intl.formatMessage({ id: 'system.clear' }) });

    const storedData = 'my-stored-data';
    await inputField.fill(storedData);
    await setButton.click();
    await expect(valueContent).toHaveText(storedData);

    await clearButton.click();
    await expect(valueContent).toHaveText('');
  });

  test('Should be able to clear all local storage data', async ({ page }) => {
    const lsBlock = page.locator('.sys-container').locator('.fields-container');
    const valueBlock = lsBlock.locator('.field').nth(0);
    const inputBlock = lsBlock.locator('.field').nth(1);
    const actionBlock = lsBlock.locator('.field').nth(2);
    const valueContent = valueBlock.locator('.content');
    const inputField = inputBlock.locator('.app-input-text').locator('input');
    const setButton = actionBlock
      .locator('.content')
      .locator('.app-button', { hasText: intl.formatMessage({ id: 'system.set' }) });
    const clearButton = page.locator('.ls-clear-all');

    const storedData = 'my-stored-data';
    await inputField.fill(storedData);
    await setButton.click();
    await expect(valueContent).toHaveText(storedData);

    await clearButton.click();
    await expect(valueContent).toHaveText('');
  });
});

test.describe('System informations', () => {
  test('browser message should contain the browser name and a version number', async ({
    page,
    browserName,
  }) => {
    test.skip(
      browserName === chromium.name(),
      'Chromium browser is detected as Chrome, therefore does not match.'
    );
    const systemBlock = page.locator('.sys-container').locator('.system-container');
    const browserRow = systemBlock.getByTestId('browser');
    await expect(browserRow).toHaveText(new RegExp(browserName, 'i'));
    await expect(browserRow).toHaveText(/\d{1,3}/); // browser version : between 0 and 999
  });

  test('browser message should contain a browser name and a version number', async ({
    page,
    browserName,
  }) => {
    test.skip(
      browserName !== chromium.name(),
      'Test non-matching browsers names, use all names regex'
    );
    const systemBlock = page.locator('.sys-container').locator('.system-container');
    const browserRow = systemBlock.getByTestId('browser');
    await expect(browserRow).toHaveText(browserRegex);
    await expect(browserRow).toHaveText(/\d{1,3}/); // browser version : between 0 and 999
  });

  test('os message should contain an os name', async ({ page }) => {
    const systemBlock = page.locator('.sys-container').locator('.system-container');
    const osRow = systemBlock.getByTestId('os');
    await expect(osRow).toHaveText(osRegex);
  });
});
