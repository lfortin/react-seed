import { test, expect, Locator } from '@playwright/test';

const cleanDate = (date: Date): void => {
  date.setUTCHours(0);
  date.setMinutes(0);
  date.setSeconds(0);
  date.setMilliseconds(0);
};

const getFirstDayOfMonth = (calendar: Locator): Locator => {
  const daysView = calendar.locator('.react-calendar__month-view__days');
  const days = daysView.locator('.react-calendar__month-view__days__day');
  return days.filter({ hasText: '1' }).first();
};

test.beforeEach(async ({ page }) => {
  await page.goto('/form');
});

test.describe('Layout', () => {
  test('Should contain a clear button', async ({ page }) => {
    const parent = page.locator('.view-form').locator('.app-input').locator('.field-value');
    const datePicker = parent.locator('.react-date-picker');
    const clearBtn = datePicker.locator('.react-date-picker__clear-button');
    await expect(clearBtn).toBeVisible();
  });
  test('Should not contain a calendar button', async ({ page }) => {
    const parent = page.locator('.view-form').locator('.app-input').locator('.field-value');
    const datePicker = parent.locator('.react-date-picker');
    const calendarBtn = datePicker.locator('.react-date-picker__calendar-button');
    await expect(calendarBtn).toBeHidden();
  });
  test('An external calendar button should be visible', async ({ page }) => {
    const parent = page.locator('.view-form').locator('.app-input').locator('.field-icon');
    const calendarBtn = parent.getByTestId('date-picker-icon');
    await expect(calendarBtn).toBeVisible();
  });
});

test.describe('Functions', () => {
  test('Should be able to clear the selected date', async ({ page }) => {
    const currentDate = new Date();
    cleanDate(currentDate);

    const field = page.locator('.app-input').nth(4);
    const calendarIcon = field.getByTestId('date-picker-icon');
    await calendarIcon.click();

    const calendar = field.locator('.react-calendar');
    const calendarNavHeader = calendar.locator('.react-calendar__navigation');
    const nextMonthBtn = calendarNavHeader.locator('.react-calendar__navigation__next-button');
    await nextMonthBtn.click();
    const date = getFirstDayOfMonth(calendar);
    await date.click();

    const clearBtn = field
      .locator('.react-date-picker')
      .locator('.react-date-picker__clear-button');
    await expect(clearBtn).toBeVisible();
    await clearBtn.click();

    const dateInput = field.locator('.react-date-picker').locator('input').first();
    const dateInputValue = new Date(await dateInput.inputValue());
    expect(dateInputValue.toString()).toBe(currentDate.toString());
  });

  test('Calendar should close after picking a date', async ({ page }) => {
    const field = page.locator('.app-input').nth(4);
    const calendarIcon = field.getByTestId('date-picker-icon');
    await calendarIcon.click();

    const calendar = field.locator('.react-calendar');
    await expect(calendar).toBeVisible();

    const calendarNavHeader = calendar.locator('.react-calendar__navigation');
    const nextMonthBtn = calendarNavHeader.locator('.react-calendar__navigation__next-button');
    await nextMonthBtn.click();

    const date = getFirstDayOfMonth(calendar);
    await date.click();

    await expect(calendar).not.toBeVisible();
  });

  test('Calendar should close after clicking on the calendar icon', async ({ page }) => {
    const field = page.locator('.app-input').nth(4);
    const calendarIcon = field.getByTestId('date-picker-icon');
    const calendar = field.locator('.react-calendar');

    await calendarIcon.click();
    await expect(calendar).toBeVisible();

    await calendarIcon.click();
    await expect(calendar).not.toBeVisible();
  });

  test('Calendar should close after clicking out of it', async ({ page }) => {
    const field = page.locator('.app-input').nth(4);
    const calendarIcon = field.getByTestId('date-picker-icon');
    const calendar = field.locator('.react-calendar');
    const outLocator = field.locator('.label');

    await calendarIcon.click();
    await expect(calendar).toBeVisible();

    await outLocator.click();
    await expect(calendar).not.toBeVisible();
  });
});
