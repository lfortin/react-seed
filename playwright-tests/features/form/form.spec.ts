import { test, expect, firefox, Locator } from '@playwright/test';
import { intl } from '../../../src/libs/i18n/initReactIntl';
import { deleteTestFile, generateTestFile } from '../../utils/files';
import { wait } from '../../utils/time';

const data = {
  firstName: 'name',
  email: 'em@il.com',
  confirmEmail: 'em@il.com',
  password: 'P@assw0rd',
};

const cleanDate = (date: Date): void => {
  date.setUTCHours(0);
  date.setMinutes(0);
  date.setSeconds(0);
  date.setMilliseconds(0);
};

const getFirstDayOfMonth = (calendar: Locator): Locator => {
  const daysView = calendar.locator('.react-calendar__month-view__days');
  const days = daysView.locator('.react-calendar__month-view__days__day');
  return days.filter({ hasText: /^1$/ }).first();
};

test.beforeEach(async ({ page }) => {
  await page.goto('/form');
});

test.describe('Layout', () => {
  test('Form should be visible', async ({ page }) => {
    const form = page.locator('.view-form');
    await expect(form).toBeVisible();
  });
  test('Form should contain 4 text input', async ({ page }) => {
    const form = page.locator('.view-form');
    const textInputs = form.locator('.app-input').locator('.app-input-text');
    await expect(textInputs).toHaveCount(4);
  });

  test('Form should contain 1 selector', async ({ page }) => {
    const form = page.locator('.view-form');
    const textInputs = form.locator('.app-selector').locator('select');
    await expect(textInputs).toHaveCount(1);
  });

  test('Form should contain 2 checkboxes', async ({ page }) => {
    const form = page.locator('.view-form');
    const textInputs = form.locator('.app-checkbox').locator('.checkbox-container');
    await expect(textInputs).toHaveCount(2);
  });

  test('Form should contain 1 switch', async ({ page }) => {
    const form = page.locator('.view-form');
    const textInputs = form.locator('.app-checkbox').locator('.switch-container');
    await expect(textInputs).toHaveCount(1);
  });

  test('Form should contain 1 file dropzone', async ({ page }) => {
    const form = page.locator('.view-form');
    const textInputs = form.locator('.app-file-selector').locator('.file-upload-container');
    await expect(textInputs).toHaveCount(1);
  });

  test('Form should contain 1 date picker', async ({ page }) => {
    const form = page.locator('.view-form');
    const datePicker = form.locator('.app-input').locator('.react-date-picker');
    await expect(datePicker).toHaveCount(1);
  });
});

test.describe('First name', () => {
  test('Should be marked as required', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(0);
    await expect(field).toHaveClass(/required/);
  });

  test('Should not be left empty', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(0);
    const input = field.locator('.app-input-text').locator('input');
    const errorBlock = field.locator('.text-danger');
    const errorMsg = intl.formatMessage({ id: 'form.view.name.required' });

    await input.fill(data.firstName);
    await expect(errorBlock).toHaveText('');

    await input.fill('');
    await expect(errorBlock).toHaveText(errorMsg);
  });
});

test.describe('Email', () => {
  test('Should be marked as required', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(1);
    await expect(field).toHaveClass(/required/);
  });

  test('Should not be left empty', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(1);
    const input = field.locator('.app-input-text').locator('input');
    const errorBlock = field.locator('.text-danger');
    const errorMsg = intl.formatMessage({ id: 'form.view.email.required' });

    await input.fill(data.email);
    await expect(errorBlock).toHaveText('');

    await input.fill('');
    await expect(errorBlock).toHaveText(errorMsg);
  });

  test('Should be in email format', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(1);
    const input = field.locator('.app-input-text').locator('input');
    const errorBlock = field.locator('.text-danger');
    const errorMsg = intl.formatMessage({ id: 'form.view.email.pattern' });

    await input.fill('wrong-email');
    await expect(errorBlock).toHaveText(errorMsg);

    await input.fill(data.email);
    await expect(errorBlock).toHaveText('');
  });
});

test.describe('Confirm email', () => {
  test('Should be marked as required', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(2);
    await expect(field).toHaveClass(/required/);
  });

  test('Should not be left empty', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(2);
    const input = field.locator('.app-input-text').locator('input');
    const emailInput = page
      .locator('.view-form')
      .locator('.app-input')
      .nth(1)
      .locator('.app-input-text')
      .locator('input');
    const errorBlock = field.locator('.text-danger');
    const errorMsg = intl.formatMessage({ id: 'form.view.email.required' });

    await emailInput.fill(data.email);
    await input.fill(data.email);
    await expect(errorBlock).toHaveText('');

    await input.fill('');
    await expect(errorBlock).toHaveText(errorMsg);
  });

  test('Should be equal to email field', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(2);
    const input = field.locator('.app-input-text').locator('input');
    const emailInput = page
      .locator('.view-form')
      .locator('.app-input')
      .nth(1)
      .locator('.app-input-text')
      .locator('input');
    const errorBlock = field.locator('.text-danger');
    const errorMsg = intl.formatMessage({ id: 'form.view.email.not_matching' });

    await emailInput.fill(data.email);
    await input.fill('wrong-email');
    await expect(errorBlock).toHaveText(errorMsg);

    await input.fill(data.email);
    await expect(errorBlock).toHaveText('');
  });
});

test.describe('Password', () => {
  test('Should be marked as required', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(3);
    await expect(field).toHaveClass(/required/);
  });

  test('Should not be left empty', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(3);
    const input = field.locator('.app-input-text').locator('input');
    const errorBlock = field.locator('.text-danger');
    const errorMsg = intl.formatMessage({ id: 'form.view.password.required' });

    await input.fill(data.password);
    await expect(errorBlock).toHaveText('');

    await input.fill('');
    await expect(errorBlock).toHaveText(errorMsg);
  });

  test('Should be at least 8 char long', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(3);
    const input = field.locator('.app-input-text').locator('input');
    const errorBlock = field.locator('.text-danger');
    const errorMsg = intl.formatMessage({ id: 'form.view.password.minLength' });

    await input.fill('@@ZZee1');
    await expect(errorBlock).toHaveText(errorMsg);

    await input.fill('@@ZZee12');
    await expect(errorBlock).toHaveText('');
  });

  test('Should have the right format', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(3);
    const input = field.locator('.app-input-text').locator('input');
    const errorBlock = field.locator('.text-danger');
    const errorMsg = intl.formatMessage({ id: 'form.view.password.pattern' });

    // missing maj
    await input.fill('@zerty12');
    await expect(errorBlock).toHaveText(errorMsg);

    // missing min
    await input.fill('@ZERTY12');
    await expect(errorBlock).toHaveText(errorMsg);

    // missing number.
    await input.fill('Azerty@@');
    await expect(errorBlock).toHaveText(errorMsg);

    // missing spe. char.
    await input.fill('Azerty12');
    await expect(errorBlock).toHaveText(errorMsg);

    // right format
    await input.fill('Azerty12!');
    await expect(errorBlock).toHaveText('');
  });
});

test.describe('Gender', () => {
  test('Should be updatable', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-selector');
    const select = field.locator('select');

    await expect(select).toHaveText(new RegExp(intl.formatMessage({ id: 'form.gender.male' })));
    await select.selectOption({ value: 'F' });
    await expect(select).toHaveText(new RegExp(intl.formatMessage({ id: 'form.gender.female' })));
  });
});

test.describe('Opt. checkbox', () => {
  test('Should be checkable', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-checkbox').nth(0);
    const checkbox = field.locator('.checkbox-container');
    const targetBox = checkbox.locator('label').locator('div').first();
    const checkedIcon = targetBox.locator('.app-icon');

    await expect(targetBox).not.toHaveClass(/checked/);

    await targetBox.click();

    await expect(targetBox).toHaveClass(/checked/);
    await expect(checkedIcon).toBeVisible();
  });

  test('Should be un-checkable', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-checkbox').nth(0);
    const checkbox = field.locator('.checkbox-container');
    const targetBox = checkbox.locator('label').locator('div').first();

    await targetBox.click();
    await expect(targetBox).toHaveClass(/checked/);
    await targetBox.click();
    await expect(targetBox).not.toHaveClass(/checked/);
  });
});

test.describe('Mandatory checkbox', () => {
  test('Should be marked as required', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-input').nth(1);
    await expect(field).toHaveClass(/required/);
  });

  test('Should be checkable', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-checkbox').nth(1);
    const checkbox = field.locator('.checkbox-container');
    const targetBox = checkbox.locator('label').locator('div').first();
    const checkedIcon = targetBox.locator('.app-icon');

    await expect(targetBox).not.toHaveClass(/checked/);

    await targetBox.click();

    await expect(targetBox).toHaveClass(/checked/);
    await expect(checkedIcon).toBeVisible();
  });

  test('Should be un-checkable', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-checkbox').nth(1);
    const checkbox = field.locator('.checkbox-container');
    const targetBox = checkbox.locator('label').locator('div').first();

    await targetBox.click();
    await expect(targetBox).toHaveClass(/checked/);
    await targetBox.click();
    await expect(targetBox).not.toHaveClass(/checked/);
  });

  test('Should not be left unchecked', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-checkbox').nth(1);
    const checkbox = field.locator('.checkbox-container');
    const targetBox = checkbox.locator('label').locator('div').first();
    const errorBlock = field.locator('.text-danger');
    const errorMsg = intl.formatMessage({ id: 'form.required' });

    await targetBox.click();
    await expect(errorBlock).toHaveText('');
    await targetBox.click();
    await expect(errorBlock).toHaveText(errorMsg);
  });
});

test.describe('Switch', () => {
  test('Should be checkable', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-checkbox').nth(2);
    const switchBtn = field.locator('.switch-container').locator('#switch');
    const tglBtn = field.locator('.switch-container').locator('.tgl-btn');

    await expect(switchBtn).not.toHaveClass(/checked/);
    await tglBtn.click();
    await expect(switchBtn).toHaveClass(/checked/);
  });

  test('Should be un-checkable', async ({ page }) => {
    const field = page.locator('.view-form').locator('.app-checkbox').nth(2);
    const switchBtn = field.locator('.switch-container').locator('#switch');
    const tglBtn = field.locator('.switch-container').locator('.tgl-btn');

    await expect(switchBtn).not.toHaveClass(/checked/);
    await tglBtn.click();
    await expect(switchBtn).toHaveClass(/checked/);
    await tglBtn.click();
    await expect(switchBtn).not.toHaveClass(/checked/);
  });
});

test.describe('Files', () => {
  test('The empty file input should display a message', async ({ page }) => {
    const field = page.locator('.app-file-selector');
    await expect(field).toBeVisible();

    const emptyDropLine1 = intl.formatMessage({ id: 'files-input.no_file' });
    const emptyDropLine2 = intl.formatMessage({ id: 'files-input.drag_n_drop' });
    const emptyDropLine3 = intl.formatMessage({ id: 'files-input.paste' });
    const container = field.locator('.file-upload-container');

    await expect(container).toHaveText(new RegExp(emptyDropLine1));
    await expect(container).toHaveText(new RegExp(emptyDropLine2));
    await expect(container).toHaveText(new RegExp(emptyDropLine3));
  });

  test('Should be able to add a file', async ({ page, browserName }) => {
    test.skip(browserName === firefox.name(), 'setInputFiles method not working on firefox.');
    const testFile = 'testAdd.txt';
    await generateTestFile(testFile, 'Hello World');
    const container = page.locator('.app-file-selector').locator('.file-upload-container');
    const input = container.getByTestId('file-upload-input');
    await input.setInputFiles(`./playwright-tests/fixtures/${testFile}`);
    await expect(container).toHaveText(testFile);
    await deleteTestFile(testFile);
  });

  test('Should be able to clear files list', async ({ page, browserName }) => {
    test.skip(browserName === firefox.name(), 'setInputFiles method not working on firefox.');

    const testFile = 'testClear.txt';
    await generateTestFile(testFile, 'Hello World');

    const container = page.locator('.app-file-selector').locator('.file-upload-container');
    const input = container.getByTestId('file-upload-input');
    await input.setInputFiles(`./playwright-tests/fixtures/${testFile}`);

    const clearFilesBtn = container.getByTestId('clear-file-upload');
    await clearFilesBtn.click();
    const emptyDropLine = intl.formatMessage({ id: 'files-input.no_file' });
    await expect(container).toHaveText(new RegExp(emptyDropLine));

    await deleteTestFile(testFile);
  });
});

test.describe('Date', () => {
  test('Default date should be the current date', async ({ page }) => {
    const currentDate = new Date();
    cleanDate(currentDate);
    const field = page.locator('.app-input').nth(4);
    const dateInput = field.locator('.react-date-picker').locator('input').first();
    await wait(0.5); // wait for components to be rendered
    const dateInputValue = new Date(await dateInput.inputValue());
    expect(dateInputValue.toString()).toBe(currentDate.toString());
  });

  test('Should be able to set a new date', async ({ page }) => {
    const targetDate = new Date();
    targetDate.setFullYear(targetDate.getFullYear() + 1);
    targetDate.setDate(1);
    cleanDate(targetDate);

    const field = page.locator('.app-input').nth(4);
    const calendarIcon = field.getByTestId('date-picker-icon');
    await calendarIcon.click();

    const calendar = field.locator('.react-calendar');
    await expect(calendar).toBeVisible();

    const calendarNavHeader = calendar.locator('.react-calendar__navigation');
    const nextYearBtn = calendarNavHeader.locator('.react-calendar__navigation__next2-button');
    await nextYearBtn.click();

    const date = getFirstDayOfMonth(calendar);
    await date.click();

    const dateInput = field.locator('.react-date-picker').locator('input').first();
    const dateInputValue = new Date(await dateInput.inputValue());
    expect(dateInputValue.toString()).toBe(targetDate.toString());
  });
});

test.describe('Send Button', () => {
  test('Should have the "send" text', async ({ page }) => {
    const sendBtn = page.locator('.view-form').locator('.app-button').nth(0);
    await expect(sendBtn).toHaveText(intl.formatMessage({ id: 'form.send' }));
  });

  test('Should disabled while form is invalid', async ({ page }) => {
    const sendBtn = page.locator('.view-form').locator('.app-button').nth(0);
    await expect(sendBtn).not.toHaveClass(/reverse/);
    await expect(sendBtn).toBeDisabled();

    // firstName
    const firstNamefield = page.locator('.view-form').locator('.app-input').nth(0);
    const firstNameInput = firstNamefield.locator('.app-input-text').locator('input');
    await firstNameInput.fill(data.firstName);
    await expect(sendBtn).toBeDisabled();

    // email
    const emailfield = page.locator('.view-form').locator('.app-input').nth(1);
    const emailInput = emailfield.locator('.app-input-text').locator('input');
    await emailInput.fill(data.email);
    await expect(sendBtn).toBeDisabled();

    // email
    const emailConfirmfield = page.locator('.view-form').locator('.app-input').nth(2);
    const emailConfirmInput = emailConfirmfield.locator('.app-input-text').locator('input');
    await emailConfirmInput.fill(data.email);
    await expect(sendBtn).toBeDisabled();

    // password
    const passwordfield = page.locator('.view-form').locator('.app-input').nth(3);
    const passwordInput = passwordfield.locator('.app-input-text').locator('input');
    await passwordInput.fill(data.password);
    await expect(sendBtn).toBeDisabled();

    // mandatory checkbox
    const checkboxField = page.locator('.view-form').locator('.app-checkbox').nth(1);
    const checkbox = checkboxField
      .locator('.checkbox-container')
      .locator('label')
      .locator('div')
      .first();
    await checkbox.click();
    await expect(sendBtn).not.toBeDisabled();
  });

  test('Should display a spinner while sending', async ({ page }) => {
    const sendBtn = page.locator('.view-form').locator('.app-button').nth(0);
    const spinner = page.locator('.view-form').locator('.spinner');

    const firstNamefield = page.locator('.view-form').locator('.app-input').nth(0);
    const firstNameInput = firstNamefield.locator('.app-input-text').locator('input');
    const emailfield = page.locator('.view-form').locator('.app-input').nth(1);
    const emailInput = emailfield.locator('.app-input-text').locator('input');
    const emailConfirmfield = page.locator('.view-form').locator('.app-input').nth(2);
    const emailConfirmInput = emailConfirmfield.locator('.app-input-text').locator('input');
    const passwordfield = page.locator('.view-form').locator('.app-input').nth(3);
    const passwordInput = passwordfield.locator('.app-input-text').locator('input');
    const checkboxField = page.locator('.view-form').locator('.app-checkbox').nth(1);
    const checkbox = checkboxField
      .locator('.checkbox-container')
      .locator('label')
      .locator('div')
      .first();

    await firstNameInput.fill(data.firstName);
    await emailInput.fill(data.email);
    await emailConfirmInput.fill(data.email);
    await passwordInput.fill(data.password);
    await checkbox.click();

    await expect(spinner).not.toBeVisible();
    await sendBtn.click();
    await expect(spinner).toBeVisible();
    await expect(sendBtn).toBeDisabled();

    // wait until it's sent
    await expect(spinner).not.toBeVisible();
  });

  test('Should be reset after form is sent', async ({ page }) => {
    const sendBtn = page.locator('.view-form').locator('.app-button').nth(0);

    const firstNamefield = page.locator('.view-form').locator('.app-input').nth(0);
    const firstNameInput = firstNamefield.locator('.app-input-text').locator('input');
    const emailfield = page.locator('.view-form').locator('.app-input').nth(1);
    const emailInput = emailfield.locator('.app-input-text').locator('input');
    const emailConfirmfield = page.locator('.view-form').locator('.app-input').nth(2);
    const emailConfirmInput = emailConfirmfield.locator('.app-input-text').locator('input');
    const passwordfield = page.locator('.view-form').locator('.app-input').nth(3);
    const passwordInput = passwordfield.locator('.app-input-text').locator('input');
    const checkboxField = page.locator('.view-form').locator('.app-checkbox').nth(1);
    const checkbox = checkboxField
      .locator('.checkbox-container')
      .locator('label')
      .locator('div')
      .first();

    await firstNameInput.fill(data.firstName);
    await emailInput.fill(data.email);
    await emailConfirmInput.fill(data.email);
    await passwordInput.fill(data.password);
    await checkbox.click();

    await expect(firstNameInput).toHaveValue(data.firstName);
    await expect(emailInput).toHaveValue(data.email);
    await expect(emailConfirmInput).toHaveValue(data.email);
    await expect(checkbox).toHaveClass(/checked/);

    await sendBtn.click();

    await expect(firstNameInput).toHaveValue('');
    await expect(emailInput).toHaveValue('');
    await expect(emailConfirmInput).toHaveValue('');
    await expect(passwordInput).toHaveValue('');
    await expect(checkbox).not.toHaveClass(/checked/);
    await expect(sendBtn).toBeDisabled();
  });
});

test.describe('Reset Button', () => {
  test('Should have the "reset" text', async ({ page }) => {
    const resetBtn = page.locator('.view-form').locator('.app-button').nth(1);
    await expect(resetBtn).toHaveText(intl.formatMessage({ id: 'form.reset' }));
  });

  test('Should reset all the fields', async ({ page, browserName }) => {
    test.skip(browserName === firefox.name(), 'setInputFiles method not working on firefox.');

    const currentDate = new Date();
    cleanDate(currentDate);
    const resetBtn = page.locator('.view-form').locator('.app-button').nth(1);
    const testFile = 'testClearAll.txt';
    await generateTestFile(testFile, 'Hello World');

    const firstNamefield = page.locator('.view-form').locator('.app-input').nth(0);
    const firstNameInput = firstNamefield.locator('.app-input-text').locator('input');
    const emailfield = page.locator('.view-form').locator('.app-input').nth(1);
    const emailInput = emailfield.locator('.app-input-text').locator('input');
    const emailConfirmfield = page.locator('.view-form').locator('.app-input').nth(2);
    const emailConfirmInput = emailConfirmfield.locator('.app-input-text').locator('input');
    const passwordfield = page.locator('.view-form').locator('.app-input').nth(3);
    const passwordInput = passwordfield.locator('.app-input-text').locator('input');
    const genderField = page.locator('.view-form').locator('.app-selector');
    const genderSelect = genderField.locator('select');
    const optionnalCheckboxField = page.locator('.view-form').locator('.app-checkbox').nth(0);
    const optionnalCheckbox = optionnalCheckboxField
      .locator('.checkbox-container')
      .locator('label')
      .locator('div')
      .first();
    const mandatoryCheckboxField = page.locator('.view-form').locator('.app-checkbox').nth(1);
    const mandatoryCheckbox = mandatoryCheckboxField
      .locator('.checkbox-container')
      .locator('label')
      .locator('div')
      .first();
    const switchField = page.locator('.view-form').locator('.app-checkbox').nth(2);
    const switchBtn = switchField.locator('.switch-container').locator('#switch');
    const tglBtn = switchField.locator('.switch-container').locator('.tgl-btn');
    const fileContainer = page.locator('.app-file-selector').locator('.file-upload-container');
    const fileInput = fileContainer.getByTestId('file-upload-input');
    const dateField = page.locator('.app-input').nth(4);
    const dateInput = dateField.locator('.react-date-picker').locator('input').first();

    await firstNameInput.fill(data.firstName);
    await emailInput.fill(data.email);
    await emailConfirmInput.fill(data.email);
    await passwordInput.fill(data.password);
    await genderSelect.selectOption({ value: 'F' });
    await optionnalCheckbox.click();
    await mandatoryCheckbox.click();
    await tglBtn.click();
    await fileInput.setInputFiles(`./playwright-tests/fixtures/${testFile}`);
    const calendarIcon = dateField.getByTestId('date-picker-icon');
    await calendarIcon.click();
    const calendar = dateField.locator('.react-calendar');
    const calendarNavHeader = calendar.locator('.react-calendar__navigation');
    const nextMonthBtn = calendarNavHeader.locator('.react-calendar__navigation__next-button');
    await nextMonthBtn.click();
    const date = getFirstDayOfMonth(calendar);
    await date.click();
    const dateInputValue = new Date(await dateInput.inputValue());

    await expect(firstNameInput).toHaveValue(data.firstName);
    await expect(emailInput).toHaveValue(data.email);
    await expect(emailConfirmInput).toHaveValue(data.email);
    await expect(genderSelect).toHaveValue('F');
    await expect(optionnalCheckbox).toHaveClass(/checked/);
    await expect(mandatoryCheckbox).toHaveClass(/checked/);
    await expect(switchBtn).toHaveClass(/checked/);
    await expect(fileContainer).toHaveText(testFile);
    expect(dateInputValue.toString()).not.toBe(currentDate.toString());

    await resetBtn.click();
    const clearedDateInputValue = new Date(await dateInput.inputValue());

    await expect(firstNameInput).toHaveValue('');
    await expect(emailInput).toHaveValue('');
    await expect(emailConfirmInput).toHaveValue('');
    await expect(passwordInput).toHaveValue('');
    await expect(genderSelect).toHaveValue('M');
    await expect(optionnalCheckbox).not.toHaveClass(/checked/);
    await expect(mandatoryCheckbox).not.toHaveClass(/checked/);
    await expect(switchBtn).not.toHaveClass(/checked/);
    const noFileMsg = intl.formatMessage({ id: 'files-input.no_file' });
    await expect(fileContainer).toHaveText(new RegExp(noFileMsg));
    expect(clearedDateInputValue.toString()).toBe(currentDate.toString());

    await deleteTestFile(testFile);
  });
});
