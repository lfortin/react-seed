import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('/');
  const heroesList = page.locator('.list');
  const target = heroesList.locator('.item').first();
  const link = target.locator('a');
  await link.click();
});

test.describe('Layout', () => {
  test('Should have the id in the url', async ({ page }) => {
    await expect(page).toHaveURL(/\/hero\/\d/);
  });

  test('Should have a title', async ({ page }) => {
    const title = page.locator('.hero-container').locator('.title');
    /**
     * [a-zA-Z] : any letter
     * . is a point char "."
     * \W any non-alphanumeric char (letters, numbers, underscore, ...)
     */
    const titleRegExp = /[a-zA-Z.\W]+/;
    await expect(title).toBeVisible();
    await expect(title).toHaveText(new RegExp(titleRegExp));
  });

  test('Should have a description', async ({ page }) => {
    const description = page.locator('.hero-container').locator('.description');
    await expect(description).toBeVisible();
  });

  test('Should have a picture', async ({ page }) => {
    const picture = page.locator('.hero-container').locator('img');
    await expect(picture).toBeVisible();
    await expect(picture).toHaveClass(/picture/);
  });

  test('Should have a back button', async ({ page }) => {
    const backBtn = page.locator('.hero-container').locator('.back-button').locator('.app-icon');
    await expect(backBtn).toBeVisible();
  });

  test('Back button should redirect to /home', async ({ page }) => {
    const backBtn = page.locator('.hero-container').locator('.back-button').locator('.app-icon');
    await backBtn.click();
    await expect(page).toHaveURL('/home');
  });
});
