import { test, expect } from '@playwright/test';
import { wait } from '../../utils/time';

test.beforeEach(async ({ page }) => {
  await page.goto('/notifications');
});

test.describe('Layout', () => {
  test('Two notification blocks should be visible', async ({ page }) => {
    const blocks = page.locator('.notif-type');
    await expect(blocks).toHaveCount(2);
  });

  test('The local notifications block should be visible', async ({ page }) => {
    const block = page.locator('.notif-type').first();
    const buttons = block.locator('.app-button');
    await expect(buttons).toHaveCount(4);
  });

  test('The system notifications block should be visible', async ({ page }) => {
    const block = page.locator('.notif-type').nth(1);
    const buttons = block.locator('.app-button');
    await expect(buttons).toHaveCount(1);
  });
});

test.describe('Local notifications', () => {
  test('The local notification should be visible for 5 seconds', async ({ page }) => {
    const block = page.locator('.notif-type').first();
    const button = block.locator('.app-button.notify-button');
    const toaster = page.locator('.Toastify');
    await expect(button).toBeVisible();

    await button.click();
    const toastContainer = toaster.locator('.Toastify__toast-container');
    const toastContent = toastContainer.locator('.Toastify__toast');

    await expect(toastContainer).toBeVisible();
    await expect(toastContainer).toHaveClass(/Toastify__toast-container--top-right/);

    await expect(toastContent).toBeVisible();
    await expect(toastContent).toHaveClass(/Toastify__toast--info/);
    await expect(toastContent).toHaveClass(/Toastify__toast--close-on-click/);

    await wait(5);
    await expect(toastContainer).toBeHidden();
    await expect(toastContent).toBeHidden();
  });

  test('The local success notification should be visible for 5 seconds', async ({ page }) => {
    const block = page.locator('.notif-type').first();
    const button = block.locator('.app-button.success-button');
    const toaster = page.locator('.Toastify');
    await expect(button).toBeVisible();

    await button.click();
    const toastContainer = toaster.locator('.Toastify__toast-container');
    const toastContent = toastContainer.locator('.Toastify__toast');

    await expect(toastContainer).toBeVisible();
    await expect(toastContainer).toHaveClass(/Toastify__toast-container--top-right/);

    await expect(toastContent).toBeVisible();
    await expect(toastContent).toHaveClass(/Toastify__toast--success/);
    await expect(toastContent).toHaveClass(/Toastify__toast--close-on-click/);

    await wait(5);
    await expect(toastContainer).toBeHidden();
    await expect(toastContent).toBeHidden();
  });

  test('The local warning notification should be visible for 5 seconds', async ({ page }) => {
    const block = page.locator('.notif-type').first();
    const button = block.locator('.app-button.warning-button');
    const toaster = page.locator('.Toastify');
    await expect(button).toBeVisible();

    await button.click();
    const toastContainer = toaster.locator('.Toastify__toast-container');
    const toastContent = toastContainer.locator('.Toastify__toast');

    await expect(toastContainer).toBeVisible();
    await expect(toastContainer).toHaveClass(/Toastify__toast-container--top-right/);

    await expect(toastContent).toBeVisible();
    await expect(toastContent).toHaveClass(/Toastify__toast--warning/);
    await expect(toastContent).toHaveClass(/Toastify__toast--close-on-click/);

    await wait(5);
    await expect(toastContainer).toBeHidden();
    await expect(toastContent).toBeHidden();
  });

  test('The local error notification should be visible for 5 seconds', async ({ page }) => {
    const block = page.locator('.notif-type').first();
    const button = block.locator('.app-button.error-button');
    const toaster = page.locator('.Toastify');
    await expect(button).toBeVisible();

    await button.click();
    const toastContainer = toaster.locator('.Toastify__toast-container');
    const toastContent = toastContainer.locator('.Toastify__toast');

    await expect(toastContainer).toBeVisible();
    await expect(toastContainer).toHaveClass(/Toastify__toast-container--top-right/);

    await expect(toastContent).toBeVisible();
    await expect(toastContent).toHaveClass(/Toastify__toast--error/);
    await expect(toastContent).toHaveClass(/Toastify__toast--close-on-click/);

    await wait(5);
    await expect(toastContainer).toBeHidden();
    await expect(toastContent).toBeHidden();
  });
});

// TO DO : write test
// eslint-disable-next-line @typescript-eslint/no-empty-function
test.describe('System notifications', () => {});
