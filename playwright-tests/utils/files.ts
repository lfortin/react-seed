import * as fs from 'fs';
import * as path from 'path';

const __dirname = path.dirname(new URL(import.meta.url).pathname);

const ensureDirectoryExistence = (filePath: string) => {
  const directory = path.dirname(filePath);
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory, { recursive: true });
  }
};

export const generateTestFile = async (fileName: string, fileContent: string): Promise<void> => {
  return new Promise((resolve) => {
    const filePath = path.resolve(__dirname, `../fixtures/${fileName}`);

    // create fixtures folder if not exists
    ensureDirectoryExistence(filePath);

    const testFile = fs.createWriteStream(filePath);
    testFile.write(fileContent);
    testFile.close();
    resolve();
  });
};

export const deleteTestFile = async (fileName: string): Promise<void> => {
  const filePath = path.resolve(__dirname, `../fixtures/${fileName}`);
  fs.unlinkSync(filePath);
  return Promise.resolve();
};
