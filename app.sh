#!/bin/bash
source .env

###
# script to start the app a dev machine
# this script could also be used in production
###

usage () {
  echo "usage: "
  echo "$0 [options] action"
  echo "  -h this help"
  echo "  -b build mode"
  echo "  -p production mode"
  echo "  action: up|stop|down|logs"
}


serv_prod="docker compose -f docker-compose.prod.yml"
serv_build="docker compose -f docker-compose.build.yml"
serv_dev="docker compose"
build_arg=""

# by default dev mode
PROD=0
BUILD=0

ARGS=""
while [ $# -gt 0 ]
do
  unset OPTIND
  unset OPTARG
  while getopts "hpb" arg; do
    case $arg in
      p)
        # production mode
        PROD=1
        build_arg="--build"
        ;;
      b)
        # build mode
        BUILD=1
        build_arg="--build"
        ;;
      h)
        usage
        exit 0
        ;;
    esac
  done
  shift $((OPTIND-1))
  ARGS="${ARGS:+${ARGS} }$1 "
  shift
done

# trim the ARGS
ARGS=`echo $ARGS | sed 's/ *S//'`

if [[ $PROD == 1 ]]; then
  serv=$serv_prod
elif [[ $BUILD == 1 ]]; then
  serv=$serv_build
else
  serv=$serv_dev
fi

case $ARGS in
  up)
    echo -e "Starting Server version ${TAG}"
    $serv up $build_arg -d
    ;;
  down)
    $serv down
    ;;
  stop)
    $serv stop
    ;;
  logs)
    $serv logs -f
    ;;
  *)
    usage
    ;;
esac
