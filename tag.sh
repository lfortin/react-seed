#!/bin/bash
dockername=lfortin

###
# script to create and push a tag
###
usage () {
  echo "usage: "
  echo "$0 <tag>"
  echo "  tag: string (eg 1.1.16)"
}

if [ $# -ne 1 ]; then
	usage
	exit
fi

# Variable to store the tag argument
TAG="$1"
image=$dockername/react-seed:$TAG

# Check if the image already exists
if docker images --format '{{.Repository}}:{{.Tag}}' | grep -q "^$image$"; then
    echo "Image $image already exists."
    exit
fi

# Build Docker image with the provided tag
docker build . -t $image

# Check if Docker build was successful
if [ $? -eq 0 ]; then
    # Push the Docker image
    docker push $image
else
    echo "Docker build failed. Image not pushed."
fi