FROM node:20-alpine as dev

ARG USER_ID=1000
ARG GROUP_ID=1000

RUN apk --no-cache add shadow && \
    usermod -u ${USER_ID} node && \
    groupmod -g ${GROUP_ID} node

WORKDIR /app

RUN find . -name 'node_modules' -type d -prune -exec rm -rf '{}' +

RUN chown -R node:node /app
USER node

WORKDIR /app

COPY package.json .
COPY . .
RUN yarn

EXPOSE ${VITE_PORT}

CMD ["yarn", "dev"]

# --------------------------------------

FROM node:20-alpine as build
WORKDIR /app
COPY --from=dev /app /app
RUN yarn build
EXPOSE ${VITE_PORT}
CMD ["yarn", "preview"]

# --------------------------------------

FROM nginx:stable-alpine as prod
COPY --from=build /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
CMD ["nginx", "-g", "daemon off;"]
