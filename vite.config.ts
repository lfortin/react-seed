import { defineConfig, loadEnv } from 'vite';
import dns from 'dns';
import react from '@vitejs/plugin-react';

dns.setDefaultResultOrder('verbatim');

// https://vitejs.dev/config/
const config = ({ mode }: { mode: string }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };
  const port = Number(process.env.VITE_PORT);
  return defineConfig({
    plugins: [react()],
    preview: {
      port,
      strictPort: true,
    },
    server: {
      host: true,
      port,
      strictPort: true,
    },
  });
};

export default config;
