import { HeroesPayload } from './actions/heroes';
import { TooltipPayload } from './actions/tooltips';

export interface Action {
  type: string;
  payload: HeroesPayload | TooltipPayload;
}

export const ADD_HEROES = 'ADD_HEROES';
export const SET_HEROES = 'SET_HEROES';
export const SET_HERO = 'SET_HERO';
export const RESET_HEROES = 'RESET_HEROES';

export const TOOLTIP_SET = 'TOOLTIP_SET';
export const TOOLTIP_SHOW = 'TOOLTIP_SHOW';
export const TOOLTIP_HIDE = 'TOOLTIP_HIDE';
