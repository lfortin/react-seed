import { ToastOptions, toast } from 'react-toastify';
import { intl } from './i18n/initReactIntl';
import { history } from './utils/navigation';

const toastParams: ToastOptions<unknown> = {
  position: 'top-right',
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
};

export const notify = (message: string) =>
  toast.info(intl.formatMessage({ id: message }), { ...toastParams });

export const success = (message: string) =>
  toast.success(intl.formatMessage({ id: message }), { ...toastParams });

export const warn = (message: string) =>
  toast.warn(intl.formatMessage({ id: message }), { ...toastParams });

export const error = (message: string) =>
  toast.error(intl.formatMessage({ id: message }), { ...toastParams });

const sendSystemNotification = (title: string, message: string, tag: string): void => {
  const onClickFunc = () => {
    window.focus();
    history.navigate && history.navigate('/home'); // arbitrary, because it is possible
  };

  const nt = new Notification(intl.formatMessage({ id: title }), {
    badge: '/favicon-32x32.png',
    body: intl.formatMessage({ id: message }),
    icon: '/favicon-32x32.png',
    tag,
  });
  nt.onclick = () => onClickFunc();
};

export const system = (title: string, message: string, tag = 'react-seed') => {
  // [default, granted, denied]
  switch (Notification.permission) {
    case 'default':
      void Notification.requestPermission((status) => {
        if (status === 'granted') sendSystemNotification(title, message, tag);
      });
      break;
    case 'granted':
      sendSystemNotification(title, message, tag);
      break;
    case 'denied':
      error('notification.system.not_allowed');
      break;
    default:
      break;
  }
};
