export const LIGHT = 'LIGHT';
export const DARK = 'DARK';

export type Theme = typeof LIGHT | typeof DARK;
export const defaultTheme: Theme = DARK; // save the planet, use dark mode

interface IColor {
  backgroundColor: string;
  secondaryBackgroundColor: string;
  textColor: string;
  borderColor: string;
  accentColor: string;
}

const colorsTab: Record<keyof IColor, string> = {
  backgroundColor: '--background-color',
  secondaryBackgroundColor: '--secondary-background-color',
  textColor: '--text-color',
  borderColor: '--border-color',
  accentColor: '--accent-color',
};

const lightColors: IColor = {
  backgroundColor: '#f5f5f5',
  secondaryBackgroundColor: '#d4d4d4',
  textColor: '#333333',
  borderColor: '#cccccc',
  accentColor: '#674EA7',
};

const darkColors: IColor = {
  backgroundColor: '#1a1a1a',
  secondaryBackgroundColor: '#555555',
  textColor: '#ffffff',
  borderColor: '#333333',
  accentColor: '#ff9900',
};

export type ThemeAttr = keyof IColor;

export const changeTheme = (theme: Theme) => {
  const newColors: IColor = theme === LIGHT ? { ...lightColors } : { ...darkColors };
  Object.entries(newColors).forEach(([key, color]: [string, string]) => {
    const colorKey = key as ThemeAttr;
    document.documentElement.style.setProperty(colorsTab[colorKey], color);
  });
};
