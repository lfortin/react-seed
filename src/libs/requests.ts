/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import axios, { AxiosHeaders, AxiosResponse, RawAxiosRequestHeaders } from 'axios';

export interface ApiResponseType<T> {
  results: T;
}

// get doc on https://developer.marvel.com/docs
const requestURL = (import.meta.env.VITE_BASE_URL as string) || '';
const apiToken = (import.meta.env.VITE_API_KEY as string) || '';

const api = axios.create({
  baseURL: requestURL,
});

const getHeaders = (): RawAxiosRequestHeaders => ({ Accept: 'application/json' });

const missingApiToken = async (): Promise<AxiosResponse> => {
  return await new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        data: { code: 'ERR', message: 'Undefined API Token' },
        status: 400,
        statusText: 'Bad Request',
        headers: getHeaders(),
        request: {},
        config: { headers: new AxiosHeaders() },
      });
    }, 300);
  });
};

const request = async <T>(
  method: string,
  url: string,
  data?: unknown,
  params?: string
): Promise<T> => {
  const keyUrl = `${url}?apikey=${apiToken}${params ? `&${params}` : ''}`;
  try {
    return api({ method, url: keyUrl, headers: getHeaders(), data })
      .then((response) => response.data.data)
      .catch((e: Error) => {
        console.log('ERROR : ', e);
        throw e;
      });
  } catch (error: unknown) {
    console.log('ERROR : ', error);
    throw error;
  }
};

export const get = async <T>(url: string, params: string): Promise<T | AxiosResponse> => {
  if (apiToken) {
    return request('GET', url, {}, params);
  } else {
    throw await missingApiToken();
  }
};

export const post = async <T>(url: string, data: unknown): Promise<T | AxiosResponse> => {
  if (apiToken) {
    return request('POST', url, data);
  } else {
    throw await missingApiToken();
  }
};

export const put = async <T>(url: string, data: unknown): Promise<T | AxiosResponse> => {
  if (apiToken) {
    return request('PUT', url, data);
  } else {
    throw await missingApiToken();
  }
};

export const del = async <T>(url: string): Promise<T | AxiosResponse> => {
  if (apiToken) {
    return request('DELETE', url);
  } else {
    throw await missingApiToken();
  }
};
