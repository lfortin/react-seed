export const UNKNWON = 'unknown';

export const CHROME = 'Google Chrome';
export const FIREFOX = 'Mozilla Firefox';
export const SAFARI = 'Safari';
export const OPERA = 'Opera';
export const EDGE = 'Microsoft Edge';
export const IE = 'Internet Explorer';

export const WINDOWS = 'Windows';
export const MACOS = 'MacOS';
export const ANDROID = 'Android';
export const IOS = 'iOS';
export const LINUX = 'Linux';

export type BrowserType =
  | typeof CHROME
  | typeof FIREFOX
  | typeof SAFARI
  | typeof OPERA
  | typeof EDGE
  | typeof IE
  | typeof UNKNWON;

export type OSType =
  | typeof WINDOWS
  | typeof MACOS
  | typeof ANDROID
  | typeof IOS
  | typeof LINUX
  | typeof UNKNWON;

export const detectBrowser = (): BrowserType => {
  const userAgent = navigator.userAgent.toLowerCase();
  if (userAgent.indexOf('chrome') > -1) {
    return CHROME;
  } else if (userAgent.indexOf('firefox') > -1) {
    return FIREFOX; // TO VERIFY
  } else if (userAgent.indexOf('safari') > -1 && userAgent.indexOf('chrome') === -1) {
    return SAFARI; // TO VERIFY
  } else if (userAgent.indexOf('opera') > -1 || userAgent.indexOf('opr') > -1) {
    return OPERA; // TO VERIFY
  } else if (userAgent.indexOf('edge') > -1) {
    return EDGE; // TO VERIFY
  } else if (userAgent.indexOf('msie') > -1 || userAgent.indexOf('trident') > -1) {
    return IE; // TO VERIFY
  } else {
    return UNKNWON;
  }
};

export const detectBrowserVersion = (): number | undefined => {
  const userAgent = navigator.userAgent.toLowerCase();
  const browser = detectBrowser();
  let matchRegExp: RegExp | string = '';

  switch (browser) {
    case CHROME:
      matchRegExp = /(?:chrome|crios)\/(\d+(\.\d+)?)/;
      break;
    case FIREFOX:
      matchRegExp = /(?:firefox|fxios)\/(\d+(\.\d+)?)/;
      break;
    case SAFARI: // TO VERIFY
      matchRegExp = /version\/(\d+(\.\d+)?) safari/;
      break;
    case OPERA: // TO VERIFY
      matchRegExp = /(?:opr|opera)\/(\d+(\.\d+)?)/;
      break;
    case EDGE: // TO VERIFY
      matchRegExp = /edg(?:e|)\/(\d+(\.\d+)?)/;
      break;
    case IE: // TO VERIFY
      matchRegExp = /(?:msie |rv:)(\d+(\.\d+)?)/;
      break;
    default:
      break;
  }
  const match = userAgent.match(matchRegExp);
  return match ? Number(match[1]) : undefined;
};

export const detectOperatingSystem = (): OSType => {
  const userAgent = navigator.userAgent.toLowerCase();

  if (userAgent.includes('windows')) {
    return WINDOWS; // TO VERIFY
  } else if (userAgent.includes('mac os')) {
    return MACOS; // TO VERIFY
  } else if (userAgent.includes('android')) {
    return ANDROID; // TO VERIFY
  } else if (userAgent.includes('iphone') || userAgent.includes('ipad')) {
    return IOS; // TO VERIFY
  } else if (userAgent.includes('linux')) {
    return LINUX;
  } else {
    return UNKNWON;
  }
};
