import { _omit } from './array';

type ObjectReference = Record<string, unknown>;

describe('_omit function', () => {
  it('Should omit specified keys from the object', () => {
    const object: ObjectReference = {
      id: 1,
      name: 'John',
      age: 30,
      email: 'john@example.com',
    };

    const keysToOmit = ['id', 'age'];

    const expectedResult = {
      name: 'John',
      email: 'john@example.com',
    };

    expect(_omit(object, keysToOmit)).toEqual(expectedResult);
  });

  it('Should return the same object if no keys are specified', () => {
    const object: ObjectReference = {
      id: 1,
      name: 'John',
      age: 30,
      email: 'john@example.com',
    };

    const keysToOmit: string[] = [];

    expect(_omit(object, keysToOmit)).toEqual(object);
  });

  it('Should return an empty object if the input object is empty', () => {
    const object: ObjectReference = {};
    const keysToOmit = ['id', 'age'];

    expect(_omit(object, keysToOmit)).toEqual({});
  });

  it('Should handle objects with no keys to omit', () => {
    const object: ObjectReference = {
      id: 1,
      name: 'John',
      age: 30,
      email: 'john@example.com',
    };

    expect(_omit(object, [])).toEqual(object);
  });

  it('Should handle objects with keys that do not exist', () => {
    const object: ObjectReference = {
      name: 'John',
      email: 'john@example.com',
    };

    const keysToOmit = ['id', 'age'];

    expect(_omit(object, keysToOmit)).toEqual(object);
  });
});
