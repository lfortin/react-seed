export const _omit = <T extends Record<string, unknown>>(object: T, keys: (keyof T)[]): T => {
  const rep: T = { ...object };

  keys.forEach((attr) => {
    if (Object.keys(rep).includes(attr as string)) {
      delete rep[attr];
    }
  });

  return rep;
};
