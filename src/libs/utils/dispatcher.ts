import { Dispatch } from 'redux';
import { Action } from '../../actionTypes';

interface Dispatcher {
  dispatch: Dispatch<Action> | null;
}

const dispatcher: Dispatcher = {
  dispatch: null,
};

export default dispatcher;
