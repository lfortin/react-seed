import { setTooltip, showTooltip, hideTooltip } from './tooltips';
import { DimensionsType } from '../models/tooltips';

const mockedDimension: DimensionsType = {
  top: 100,
  left: 200,
  width: 50,
  height: 20,
  bottom: 120,
  right: 250,
  x: 0,
  y: 0,
};

describe('Tooltip Actions', () => {
  it('Should create an action to set a tooltip', () => {
    const id = '1';
    const text = 'Tooltip text';
    const dimensions = mockedDimension;
    const direction = 'TOP';
    const displayTooltip = true;
    const values = { key: 'value' };
    const isText = false;
    const expectedAction = {
      type: 'TOOLTIP_SET',
      payload: {
        id,
        text,
        dimensions,
        direction,
        displayTooltip,
        values,
        isText,
      },
    };
    expect(setTooltip(id, text, dimensions, direction, displayTooltip, values, isText)).toEqual(
      expectedAction
    );
  });

  it('Should create an action to show a tooltip', () => {
    const id = '1';
    const expectedAction = {
      type: 'TOOLTIP_SHOW',
      payload: {
        id,
      },
    };
    expect(showTooltip(id)).toEqual(expectedAction);
  });

  it('Should create an action to hide a tooltip', () => {
    const id = '1';
    const expectedAction = {
      type: 'TOOLTIP_HIDE',
      payload: {
        id,
      },
    };
    expect(hideTooltip(id)).toEqual(expectedAction);
  });
});
