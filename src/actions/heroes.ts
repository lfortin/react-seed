import { ADD_HEROES, SET_HERO, SET_HEROES } from '../actionTypes';
import { Hero } from '../models/heroes';

export interface HeroesListPayload {
  values: Hero[];
}
export interface HeroPayload {
  id: string;
  value: Hero;
}

export type HeroesPayload = HeroesListPayload | HeroPayload;

export const addHeroes = (values: Hero[]) => ({
  type: ADD_HEROES,
  payload: {
    values,
  },
});

export const setHeroes = (values: Hero[]) => ({
  type: SET_HEROES,
  payload: {
    values,
  },
});

export const setHero = (id: string, value: Hero) => ({
  type: SET_HERO,
  payload: {
    id,
    value,
  },
});
