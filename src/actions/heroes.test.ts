import { addHeroes, setHeroes, setHero } from './heroes';
import { mockedHero } from '../models/heroes.test';

describe('Heroes Actions', () => {
  it('Should create an action to add heroes', () => {
    const heroes = [mockedHero(1), mockedHero(2)];
    const expectedAction = {
      type: 'ADD_HEROES',
      payload: {
        values: heroes,
      },
    };
    expect(addHeroes(heroes)).toEqual(expectedAction);
  });

  it('Should create an action to set heroes', () => {
    const heroes = [mockedHero(1), mockedHero(2)];
    const expectedAction = {
      type: 'SET_HEROES',
      payload: {
        values: heroes,
      },
    };
    expect(setHeroes(heroes)).toEqual(expectedAction);
  });

  it('Should create an action to set a hero', () => {
    const id = '1';
    const hero = mockedHero(1);
    const expectedAction = {
      type: 'SET_HERO',
      payload: {
        id,
        value: hero,
      },
    };
    expect(setHero(id, hero)).toEqual(expectedAction);
  });
});
