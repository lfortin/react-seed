import { Action, SET_HEROES, RESET_HEROES, SET_HERO, ADD_HEROES } from '../actionTypes';
import { HeroPayload, HeroesListPayload } from '../actions/heroes';
import { HERO, storeKeys } from '../constants/reduxTypes';
import { Hero } from '../models/heroes';
import { organizePerKey } from './index';

export interface HeroReducer {
  state: HeroesState;
  action: Action;
}

export interface HeroesState {
  collection: HeroesCollection;
}

export type HeroesCollection = Record<string, Hero>;

const initialStateCollections = {};

const initialState: HeroesState = {
  collection: { ...initialStateCollections },
};

/**
 * This reducer keeps references to certain collections of data
 */
const reducer = (state = initialState, action: Action): HeroesState => {
  switch (action.type) {
    case ADD_HEROES: {
      const { values } = action.payload as HeroesListPayload;
      return {
        ...state,
        collection: {
          ...state.collection,
          ...organizePerKey(values, storeKeys[HERO]),
        },
      };
    }
    case SET_HEROES: {
      const { values } = action.payload as HeroesListPayload;
      return {
        ...state,
        collection: organizePerKey(values, storeKeys[HERO]),
      };
    }
    case SET_HERO: {
      const { id, value }: { id: string; value: Hero } = action.payload as HeroPayload;
      return {
        ...state,
        collection: {
          ...state.collection,
          [id]: value,
        },
      };
    }
    case RESET_HEROES: {
      return { ...initialState };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
