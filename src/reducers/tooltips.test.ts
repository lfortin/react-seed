import reducer, { TooltipsState } from './tooltips';
import { Action } from '../actionTypes';
import { DimensionsType } from '../models/tooltips';

const mockedDimension: DimensionsType = {
  top: 100,
  left: 200,
  width: 50,
  height: 20,
  bottom: 120,
  right: 250,
  x: 0,
  y: 0,
};

describe('Tooltips Reducer', () => {
  const initialState = {};

  describe('Initial State', () => {
    it('Should return the initial mockState', () => {
      expect(reducer(undefined, {} as Action)).toEqual(initialState);
    });
  });

  describe('TOOLTIP_SET', () => {
    it('Should handle TOOLTIP_SET', () => {
      const tooltip = {
        id: 'tooltip1',
        text: 'Tooltip Text',
        dimensions: { top: 100, left: 200, width: 150, height: 50 },
        direction: 'RIGHT',
        displayTooltip: true,
        values: {},
        isText: true,
      };
      const action = {
        type: 'TOOLTIP_SET',
        payload: tooltip,
      };
      const expectedState = {
        tooltip1: {
          ...tooltip,
          visible: false,
        },
      };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  });

  describe('TOOLTIP_SHOW', () => {
    const mockState: TooltipsState = {
      tooltip1: {
        id: 'tooltip1',
        visible: false,
        text: '',
        dimensions: mockedDimension,
        direction: 'RIGHT',
      },
      tooltip2: {
        id: 'tooltip2',
        visible: false,
        text: '',
        dimensions: mockedDimension,
        direction: 'RIGHT',
      },
    };
    it('Should handle action', () => {
      const action = { type: 'TOOLTIP_SHOW', payload: { id: 'tooltip1' } };
      const expectedState = {
        tooltip1: { ...mockState.tooltip1, visible: true },
        tooltip2: { ...mockState.tooltip2, visible: false },
      };
      expect(reducer(mockState, action)).toEqual(expectedState);
    });
    it('Should hide all not targeted tooltips', () => {
      const action = { type: 'TOOLTIP_SHOW', payload: { id: 'tooltip1' } };
      const initialState = {
        tooltip1: { ...mockState.tooltip1, visible: true },
        tooltip2: { ...mockState.tooltip2, visible: true },
      };
      const expectedState = {
        tooltip1: { ...mockState.tooltip1, visible: true },
        tooltip2: { ...mockState.tooltip2, visible: false },
      };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  });

  describe('TOOLTIP_HIDE', () => {
    it('Should handle action', () => {
      const mockState: TooltipsState = {
        tooltip1: {
          id: 'tooltip1',
          visible: true,
          text: '',
          dimensions: mockedDimension,
          direction: 'RIGHT',
        },
        tooltip2: {
          id: 'tooltip2',
          visible: true,
          text: '',
          dimensions: mockedDimension,
          direction: 'RIGHT',
        },
      };
      const action = { type: 'TOOLTIP_HIDE', payload: { id: 'tooltip1' } };
      const expectedState = {
        tooltip1: { ...mockState.tooltip1, visible: false },
        tooltip2: { ...mockState.tooltip2, visible: true },
      };
      expect(reducer(mockState, action)).toEqual(expectedState);
    });
  });
});
