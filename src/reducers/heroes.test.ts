import reducer from './heroes';
import { Action } from '../actionTypes';
import { HeroesPayload } from '../actions/heroes';
import { mockedHero } from '../models/heroes.test';

describe('Heroes Reducer', () => {
  const initialState = {
    collection: {},
  };

  describe('Initial State', () => {
    it('Should return the initial state', () => {
      expect(reducer(undefined, {} as Action)).toEqual(initialState);
    });
  });

  describe('ADD_HEROES', () => {
    it('Should handle action', () => {
      const action = {
        type: 'ADD_HEROES',
        payload: {
          values: [mockedHero(1), mockedHero(2)],
        },
      };
      const expectedState = {
        collection: {
          '1': mockedHero(1),
          '2': mockedHero(2),
        },
      };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  });

  describe('SET_HEROES', () => {
    it('Should handle action', () => {
      const action = {
        type: 'SET_HEROES',
        payload: {
          values: [mockedHero(3), mockedHero(4)],
        },
      };
      const expectedState = {
        collection: {
          '3': mockedHero(3),
          '4': mockedHero(4),
        },
      };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  });

  describe('SET_HERO', () => {
    it('Should handle action', () => {
      const state = {
        collection: {
          '1': mockedHero(1),
          '2': mockedHero(2),
        },
      };
      const mockUpdatedHero = {
        ...mockedHero(1),
        name: 'Updated Hero 1',
      };
      const action = {
        type: 'SET_HERO',
        payload: { id: '1', value: mockUpdatedHero },
      };
      const expectedState = {
        collection: {
          '1': mockUpdatedHero,
          '2': mockedHero(2),
        },
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });
    it('Should create unexisting hero', () => {
      const state = {
        collection: {
          '1': mockedHero(1),
          '2': mockedHero(2),
        },
      };
      const mockUpdatedHero = mockedHero(3);

      const action = {
        type: 'SET_HERO',
        payload: { id: '3', value: mockUpdatedHero },
      };
      const expectedState = {
        collection: {
          '1': mockedHero(1),
          '2': mockedHero(2),
          '3': mockedHero(3),
        },
      };
      expect(reducer(state, action)).toEqual(expectedState);
    });
  });

  describe('RESET_HEROES', () => {
    it('Should handle action', () => {
      const state = {
        collection: {
          '1': mockedHero(1),
          '2': mockedHero(2),
        },
      };
      const action = { type: 'RESET_HEROES', payload: null as unknown as HeroesPayload };
      expect(reducer(state, action)).toEqual(initialState);
    });
  });
});
