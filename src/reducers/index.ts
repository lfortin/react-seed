import { configureStore, Reducer, ReducersMapObject } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import heroes, { HeroesState } from './heroes';
import tooltips, { TooltipsState } from './tooltips';
import { HERO, TOOLTIPS } from '../constants/reduxTypes';
import { Action } from '../actionTypes';

export const organizePerKey = <T>(arr: T[], key: string): Record<string, T> =>
  arr.reduce((res, el) => {
    const keyAttr = el[key as keyof T] as string;

    if (keyAttr) {
      return { ...res, [keyAttr]: el };
    }

    return res;
  }, {});

export interface StateInterface {
  [HERO]: HeroesState;
  [TOOLTIPS]: TooltipsState;
}

export interface StoreInterface {
  [HERO]: (state: HeroesState, action: Action) => HeroesState;
  [TOOLTIPS]: (state: TooltipsState, action: Action) => TooltipsState;
}

const reducers: ReducersMapObject<StateInterface, Action> = {
  [HERO]: heroes,
  [TOOLTIPS]: tooltips,
};

const rootReducer: Reducer<StateInterface, Action> = combineReducers(reducers);

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
  devTools: !!import.meta.env.DEV,
});

export default rootReducer;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
