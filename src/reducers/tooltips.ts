import { TOOLTIP_SET, TOOLTIP_SHOW, TOOLTIP_HIDE, Action } from '../actionTypes';
import { TooltipPayload } from '../actions/tooltips';
import { TooltipType } from '../models/tooltips';

export interface TooltipsReducer {
  state: TooltipsState;
  action: Action;
}

export type TooltipsCollection = Record<string, TooltipType>;

export type TooltipsState = Record<string, TooltipType>;

const initialTooltipsState = {};

const initialState: TooltipsState = {
  ...initialTooltipsState,
};

const reducer = (state = initialState, action: Action): TooltipsState => {
  switch (action.type) {
    case TOOLTIP_SET: {
      const { id, text, dimensions, direction, displayTooltip, values, isText } =
        action.payload as TooltipType;
      return {
        ...state,
        [id]: {
          id,
          text,
          values,
          dimensions,
          direction,
          displayTooltip,
          visible: false,
          isText,
        },
      };
    }
    case TOOLTIP_SHOW: {
      const { id }: { id: string } = action.payload as TooltipPayload;
      const hideAllExcept = (id: string) =>
        Object.values(state).reduce(
          (res, t) => ({
            ...res,
            [t.id]: { ...t, visible: t.id === id },
          }),
          {}
        );
      return {
        ...state,
        ...hideAllExcept(id),
      };
    }
    case TOOLTIP_HIDE: {
      const { id }: { id: string } = action.payload as TooltipPayload;
      return {
        ...state,
        [id]: {
          ...state[id],
          visible: false,
        },
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
