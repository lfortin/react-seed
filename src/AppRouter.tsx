import React, { FC, useEffect, lazy, ReactNode } from 'react';
import { Navigate, Routes, Route, useLocation, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { IntlProvider } from 'react-intl';
import './App.css';
// store
import { useLocaleStateStore } from './store/Locale';
// components
const Form = lazy(() => import('./tsx/Views/Form'));
const HeroContainer = lazy(() => import('./tsx/Views/Hero'));
const Home = lazy(() => import('./tsx/Views/Home'));
const System = lazy(() => import('./tsx/Views/System'));
const Notification = lazy(() => import('./tsx/Views/Notification'));
const ReactApp = lazy(() => import('./tsx/Views/ReactApp'));
const TabbedContainer = lazy(() => import('./tsx/Views/Tabbed'));

import AppLayout from './tsx/Components/AppLayout/AppLayout';
// libs
import { defaultTheme, changeTheme } from './libs/colors';
import dispatcher from './libs/utils/dispatcher';
import { history } from './libs/utils/navigation';
import { intl, switchLocale } from './libs/i18n/initReactIntl';

const fbStyle = {
  height: '100%',
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
};
const fallback = (
  <div style={fbStyle}>
    <div>...</div>
  </div>
);

const Element: FC<ElementProps> = ({ children }) => (
  <React.Suspense fallback={fallback}>{children}</React.Suspense>
);

interface ElementProps {
  children: ReactNode;
}

const AppRouter: FC = () => {
  const location = useLocation();
  const { locale } = useLocaleStateStore();

  history.navigate = useNavigate();
  history.location = location;

  const dispatch = useDispatch();
  if (!dispatcher.dispatch) {
    dispatcher.dispatch = dispatch;
  }

  useEffect(() => {
    changeTheme(defaultTheme);
  }, []);

  useEffect(() => {
    switchLocale(locale);
  }, [locale]);

  return (
    <div className="app-wrapper">
      <IntlProvider locale={locale} messages={intl.messages}>
        <Routes>
          <Route element={<AppLayout location={location} />}>
            <Route
              path="/"
              element={
                <Element>
                  <Navigate to="/home" />
                </Element>
              }
            />
            <Route
              path="/home"
              element={
                <Element>
                  <Home />
                </Element>
              }
            />
            <Route
              path="/hero/:id"
              element={
                <Element>
                  <HeroContainer location={location} />
                </Element>
              }
            />
            <Route
              path="/tab-alpha"
              element={
                <Element>
                  <TabbedContainer location={location} />
                </Element>
              }
            />
            <Route
              path="/tab-beta"
              element={
                <Element>
                  <TabbedContainer location={location} />
                </Element>
              }
            />
            <Route
              path="/tab-omega"
              element={
                <Element>
                  <TabbedContainer location={location} />
                </Element>
              }
            />
            <Route
              path="/form"
              element={
                <Element>
                  <Form />
                </Element>
              }
            />
            <Route
              path="/notifications"
              element={
                <Element>
                  <Notification />
                </Element>
              }
            />
            <Route
              path="/system"
              element={
                <Element>
                  <System />
                </Element>
              }
            />
            <Route
              path="/thank-you"
              element={
                <Element>
                  <ReactApp />
                </Element>
              }
            />
            <Route
              path="*"
              element={
                <Element>
                  <Navigate to="/home" />
                </Element>
              }
            />
          </Route>
        </Routes>
      </IntlProvider>
    </div>
  );
};

export default AppRouter;
