export const HERO = 'heroes';
export const TOOLTIPS = 'tooltips';

export const storeKeys = {
  [HERO]: 'id',
  [TOOLTIPS]: 'id',
};
