import { create } from 'zustand';

interface TitleState {
  title: string;
  setTitle: (title: string) => void;
}

export const useTitleStateStore = create<TitleState>(
  (set): TitleState => ({
    title: '',
    setTitle: (title: string) => set({ title }),
  })
);
