import { create } from 'zustand';
import { defaultLocale, LocaleType } from '../libs/i18n/initReactIntl';

interface LocaleState {
  locale: LocaleType;
  setLocale: (newLocale: LocaleType) => void;
}

export const useLocaleStateStore = create<LocaleState>(
  (set): LocaleState => ({
    locale: defaultLocale,
    setLocale: (locale: LocaleType) => set({ locale }),
  })
);
