import { create } from 'zustand';

interface MenuOpenState {
  isOpen: boolean;
  setMenuOpen: (newMenuOpen: boolean) => void;
}

export const useMenuOpenStateStore = create<MenuOpenState>(
  (set): MenuOpenState => ({
    isOpen: true,
    setMenuOpen: (isOpen: boolean) => set({ isOpen }),
  })
);
