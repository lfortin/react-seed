import { create } from 'zustand';
import { defaultTheme } from '../libs/colors';

interface ThemeState {
  theme: string;
  setTheme: (newTheme: string) => void;
}

export const useThemeStateStore = create<ThemeState>(
  (set): ThemeState => ({
    theme: defaultTheme,
    setTheme: (newTheme: string) => set({ theme: newTheme }),
  })
);
