import { describe, it, vi } from 'vitest';
import { MemoryRouter } from 'react-router-dom';
import { screen, waitFor } from '@testing-library/react';
import App from './AppRouter';
import { renderWithProviders } from './test-utils';

vi.mock('./api/heroes', () => ({
  fetchHeroes: vi.fn(() => Promise.resolve()),
}));

describe('Router Component', () => {
  it('Default view is Home view', async () => {
    renderWithProviders(
      <MemoryRouter>
        <App />
      </MemoryRouter>
    );
    // Wait for loading to finish
    await screen.findByText(/^Here are your (\d+) heroes$/);
    // View is lazy-loaded, should wait for it
    await waitFor(() => expect(screen.getByTestId('home-view')).toBeDefined());
  });

  it('Unknown URL redirects to Home view', async () => {
    const badRoute = '/some/bad/route';
    // use <MemoryRouter> when you want to manually control the history
    renderWithProviders(
      <MemoryRouter initialEntries={[badRoute]}>
        <App />
      </MemoryRouter>
    );
    await screen.findByText(/^Here are your (\d+) heroes$/);
    const homeView = screen.getByTestId('home-view');
    expect(homeView).toBeDefined();
  });

  it('Empty URL redirects to Home view', async () => {
    const emptyRoute = '/';
    renderWithProviders(
      <MemoryRouter initialEntries={[emptyRoute]}>
        <App />
      </MemoryRouter>
    );
    await screen.findByText(/^Here are your (\d+) heroes$/);
    const homeView = screen.getByTestId('home-view');
    expect(homeView).toBeDefined();
  });

  it('Check tab-alpha URL', async () => {
    const route = '/tab-alpha';
    renderWithProviders(
      <MemoryRouter initialEntries={[route]}>
        <App />
      </MemoryRouter>
    );
    // View is lazy-loaded, should wait for it
    await waitFor(() => expect(screen.getByTestId('tab-view')).toBeDefined());
  });

  it('Check tab-beta URL', async () => {
    const route = '/tab-beta';
    renderWithProviders(
      <MemoryRouter initialEntries={[route]}>
        <App />
      </MemoryRouter>
    );
    // View is lazy-loaded, should wait for it
    await waitFor(() => expect(screen.getByTestId('tab-view')).toBeDefined());
  });

  it('Check form URL', async () => {
    const route = '/form';
    renderWithProviders(
      <MemoryRouter initialEntries={[route]}>
        <App />
      </MemoryRouter>
    );
    // View is lazy-loaded, should wait for it
    await waitFor(() => expect(screen.getByTestId('form-view')).toBeDefined());
  });

  it('Check notification URL', async () => {
    const route = '/notifications';
    renderWithProviders(
      <MemoryRouter initialEntries={[route]}>
        <App />
      </MemoryRouter>
    );
    // View is lazy-loaded, should wait for it
    await waitFor(() => expect(screen.getByTestId('notification-view')).toBeDefined());
  });

  it('Check system URL', async () => {
    const route = '/system';
    renderWithProviders(
      <MemoryRouter initialEntries={[route]}>
        <App />
      </MemoryRouter>
    );
    // View is lazy-loaded, should wait for it
    await waitFor(() => expect(screen.getByTestId('system-view')).toBeDefined());
  });

  it('Check tip URL', async () => {
    const route = '/thank-you';
    renderWithProviders(
      <MemoryRouter initialEntries={[route]}>
        <App />
      </MemoryRouter>
    );
    // View is lazy-loaded, should wait for it
    await waitFor(() => expect(screen.getByTestId('tip-view')).toBeDefined());
  });
});
