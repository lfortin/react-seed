import { Hero } from './heroes';

const mockedMedia = {
  available: 0,
  collectionURI: 'http://mocked/collection/uri',
  items: [
    {
      ressourceURI: 'http://mocked/ressource/uri',
      name: 'mocked-name',
    },
  ],
  returned: 0,
};
const mockedUrl = { type: 'wiki', url: 'http:/wiki/mocked/url' };

export const mockedHero = (id: number): Hero => ({
  id,
  name: `Mocked Hero ${id}`,
  description: `Mocked Hero description ${id}`,
  modified: '',
  thumbnail: {
    path: 'https://images.unsplash.com/photo-1712743072516-13b19bc38840?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    extension: 'jpg',
  },
  ressourceURI: `http://mocked/uri/${id}`,
  comics: [mockedMedia],
  series: [mockedMedia],
  stories: [mockedMedia],
  events: [mockedMedia],
  urls: [mockedUrl],
});

describe('Hero Mock Function', () => {
  it('Should return a fully formatted Hero object', () => {
    const hero = mockedHero(1);
    expect(hero.id).toBe(1);
    expect(hero.name).toBe(`Mocked Hero 1`);
    expect(hero.description).toBe(`Mocked Hero description 1`);
    expect(hero.ressourceURI).toBe(`http://mocked/uri/1`);
    expect(hero.comics).toHaveLength(1);
    expect(hero.comics[0]).toBe(mockedMedia);
    expect(hero.series).toHaveLength(1);
    expect(hero.series[0]).toBe(mockedMedia);
    expect(hero.stories).toHaveLength(1);
    expect(hero.stories[0]).toBe(mockedMedia);
    expect(hero.events).toHaveLength(1);
    expect(hero.events[0]).toBe(mockedMedia);
    expect(hero.urls).toHaveLength(1);
    expect(hero.urls[0]).toBe(mockedUrl);
  });
});
