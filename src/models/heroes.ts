export interface HeroPicture {
  path: string;
  extension: string;
}

interface MediaItem {
  ressourceURI: string;
  name: string;
}

interface Media {
  available: number;
  collectionURI: string;
  items: MediaItem[];
  returned: number;
}

interface HeroUrls {
  type: string;
  url: string;
}

export interface Hero {
  id: number;
  name: string;
  description: string;
  modified: string;
  thumbnail: HeroPicture;
  ressourceURI: string;
  comics: Media[];
  series: Media[];
  stories: Media[];
  events: Media[];
  urls: HeroUrls[];
}
