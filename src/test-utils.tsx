import { FC, ReactNode } from 'react';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { render, RenderOptions } from '@testing-library/react';
import {
  AnyAction,
  MiddlewareArray,
  Reducer,
  ReducersMapObject,
  ThunkMiddleware,
  combineReducers,
  configureStore,
} from '@reduxjs/toolkit';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
// redux
import { Action } from './actionTypes';
import { StateInterface } from './reducers';
import heroes from './reducers/heroes';
import tooltips from './reducers/tooltips';
import { HERO, TOOLTIPS } from './constants/reduxTypes';
// intl
import en from './libs/i18n/en.json' assert { type: 'json' };

const reducers: ReducersMapObject<StateInterface, Action> = {
  [HERO]: heroes,
  [TOOLTIPS]: tooltips,
};

const rootReducer: Reducer<StateInterface, Action> = combineReducers(reducers);

interface RenderWithProvidersOptions {
  preloadedState?: StateInterface;
  store?: ToolkitStore<
    StateInterface,
    Action,
    MiddlewareArray<[ThunkMiddleware<StateInterface, AnyAction>]>
  >;
  renderOptions?: RenderOptions;
}

interface WrapperProps {
  children: ReactNode;
}

export function renderWithProviders(
  ui: React.ReactElement,
  {
    preloadedState,
    // Automatically create a store instance if no store was passed in
    store = configureStore({
      reducer: rootReducer,
      preloadedState,
      middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
          serializableCheck: false,
        }),
    }),
    ...renderOptions
  }: RenderWithProvidersOptions = {}
) {
  const Wrapper: FC<WrapperProps> = ({ children }) => {
    return (
      <Provider store={store}>
        <IntlProvider locale="en" messages={en}>
          {children}
        </IntlProvider>
      </Provider>
    );
  };

  // Return an object with the store and all of RTL's query functions
  return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}
