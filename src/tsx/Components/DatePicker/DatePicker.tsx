/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect, useState } from 'react';
import ReactDatePicker, { DatePickerProps } from 'react-date-picker';
import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';
import './DatePicker.css';
// store
import { useLocaleStateStore } from '../../../store/Locale';
// libs
import { EN, FR } from '../../../libs/i18n/initReactIntl';

type ValuePiece = Date | null;
type Value = ValuePiece | [ValuePiece, ValuePiece];

const DatePicker: FC<Props> = ({
  disabled = false,
  isOpen = false,
  selectedDate = new Date(),
  maxDate,
  minDate,
  showCalendarIcon = true,
  onChange,
  setOpen,
}) => {
  const { locale } = useLocaleStateStore();
  const [startDate, setStartDate] = useState(selectedDate);

  useEffect(() => {
    if (new Date(startDate) !== new Date(selectedDate)) {
      setStartDate(selectedDate);
    }
  }, [selectedDate]);

  const onDateChange = (date: Value) => {
    const newDate = (date as Date) || new Date();
    onChange(newDate);
  };

  const defaultFormat = 'MM/dd/yyyy';
  let format = defaultFormat;

  switch (locale) {
    case FR:
      format = 'dd/MM/yyyy';
      break;
    case EN:
      format = 'MM/dd/yyyy';
      break;
    default:
      format = defaultFormat;
      break;
  }

  const onClose = () => {
    setTimeout(() => {
      setOpen(false);
    }, 100);
  };

  const datePickerOptionalProps: DatePickerProps = {};

  if (!showCalendarIcon) {
    datePickerOptionalProps.calendarIcon = null;
  }

  return (
    <ReactDatePicker
      className="app-datePicker"
      disabled={disabled}
      format={format}
      isOpen={isOpen}
      locale={locale?.toLocaleLowerCase()}
      maxDate={maxDate}
      minDate={minDate}
      onCalendarClose={onClose}
      onChange={(date: Value) => {
        onDateChange(date);
      }}
      value={startDate}
      {...datePickerOptionalProps}
    />
  );
};

interface Props {
  disabled?: boolean;
  isOpen?: boolean;
  maxDate?: Date;
  minDate?: Date;
  selectedDate?: Date;
  showCalendarIcon?: boolean;
  onChange: (value: Date) => void;
  setOpen: (open: boolean) => void;
}

export default DatePicker;
