import { FC } from 'react';
import { useIntl } from 'react-intl';
import './LocaleSelector.css';
import { useLocaleStateStore } from '../../../store/Locale';
import Dropdown from '../generic/Dropdown';
import { DropdownOption, SelectedOption } from '../generic/Dropdown/Dropdown';
import { EN, FR, LocaleType, defaultLocale, switchLocale } from '../../../libs/i18n/initReactIntl';

const LocaleSelector: FC<Props> = ({ isOpen }) => {
  const intl = useIntl();
  const { locale, setLocale } = useLocaleStateStore();

  const changeLocale = (value: SelectedOption) => {
    setLocale(value as LocaleType); // zustand
    switchLocale(value as LocaleType); // i18n manager
  };

  const enLabel = intl.formatMessage({
    id: `locale.en${isOpen ? '' : '_short'}`,
  });
  const frLabel = intl.formatMessage({
    id: `locale.fr${isOpen ? '' : '_short'}`,
  });

  const localeOptions: DropdownOption[] = [
    { value: EN, label: enLabel },
    { value: FR, label: frLabel },
  ];

  return (
    <Dropdown
      className="locale-selector"
      label="locale.selector.label"
      options={localeOptions}
      selectedOption={locale || defaultLocale}
      onChange={changeLocale}
      width={isOpen ? 8 : 4}
    />
  );
};

interface Props {
  isOpen?: boolean;
}

export default LocaleSelector;
