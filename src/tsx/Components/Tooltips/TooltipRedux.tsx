import { FC, memo, useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import './Tooltip.css';
import { getVisibleTooltip } from '../../../selectors/tooltips';
import { StateInterface } from '../../../reducers';
import { BOTTOM, DimensionsType, LEFT, RIGHT, TooltipType } from '../../../models/tooltips';

interface ElementProperties {
  height: number;
  width: number;
}

const TooltipRedux: FC<Props> = ({ tooltip }) => {
  const [elementProperties, setElementProperties] = useState<ElementProperties>({
    height: 0,
    width: 0,
  });

  const elt = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (elt.current) {
      setElementProperties(elt.current.getBoundingClientRect());
    }
  }, [elt, tooltip]);

  const { dimensions, text, values, isText, direction, displayTooltip } = tooltip ?? {};
  const dim = dimensions ?? ({} as DimensionsType);
  const middleHeightTooltip = elementProperties.height / 2;
  const middleWidthTooltip = elementProperties.width / 2;

  let tooltipPositionX = 0;
  let tooltipPositionY = 0;

  // Kept historical code, removed fixed the unit tests
  // if (tooltip && middleWidthTooltip !== 0) {
  if (tooltip) {
    switch (direction) {
      case RIGHT:
        tooltipPositionX = dim.right + 10;
        tooltipPositionY = dim.top + dim.height / 2 - middleHeightTooltip;
        break;
      case LEFT:
        tooltipPositionX = dim.left - elementProperties.width - 10;
        tooltipPositionY = dim.top + dim.height / 2 - middleHeightTooltip;
        break;
      case BOTTOM:
        tooltipPositionX = dim.left + dim.width / 2 - middleWidthTooltip;
        tooltipPositionY = dim.bottom + 10;
        break;
      default:
        tooltipPositionX = dim.left + dim.width / 2 - middleWidthTooltip;
        tooltipPositionY = dim.top - elementProperties.height - 10;
        break;
    }
  }

  const toolText = isText ? text : text ? <FormattedMessage id={text} values={values} /> : '';

  const className = `ToolTip ${displayTooltip ? `display ${direction?.toLocaleLowerCase()}` : ''}`;
  const style = { top: `${tooltipPositionY}px`, left: `${tooltipPositionX}px` };

  return ReactDOM.createPortal(
    <div data-testid="tooltip-component" className={className} style={style} ref={elt}>
      {toolText}
    </div>,
    document.body
  );
};

interface Props {
  tooltip?: TooltipType;
}

const mapStateToProps = (state: StateInterface) => ({
  tooltip: getVisibleTooltip(state),
});

const areEquals = (prevProps: Props, nextProps: Props) =>
  JSON.stringify(prevProps) === JSON.stringify(nextProps);

export default connect(mapStateToProps)(memo(TooltipRedux, areEquals));
