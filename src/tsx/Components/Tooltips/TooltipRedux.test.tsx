import { describe, it, vi } from 'vitest';
import { screen } from '@testing-library/react';
import TooltipRedux from './TooltipRedux';
import { renderWithProviders } from '../../../test-utils';
import { getVisibleTooltip } from '../../../selectors/tooltips';
import { TooltipType } from '../../../models/tooltips';

const tooltipMock: TooltipType = {
  id: 'tooltip-mocked',
  dimensions: {
    top: 100,
    left: 200,
    width: 50,
    height: 20,
    bottom: 120,
    right: 250,
    x: 0,
    y: 0,
  },
  text: 'Tooltip Text',
  values: {},
  isText: true,
  direction: 'BOTTOM',
  displayTooltip: true,
};

vi.mock('../../../selectors/tooltips');

describe('TooltipRedux Component', () => {
  beforeEach(() => {
    vi.mocked(getVisibleTooltip).mockReturnValue(tooltipMock);
    renderWithProviders(<TooltipRedux />);
  });

  it('Renders Tooltip', () => {
    const tooltipComponent = screen.getByTestId('tooltip-component');
    expect(tooltipComponent).toBeDefined();
    const className = tooltipComponent?.getAttribute('class');
    expect(className).toMatch(/display/);
  });

  it('Renders tooltip text correctly', () => {
    const tooltipText = screen.getByText('Tooltip Text');
    expect(tooltipText).toBeDefined();
  });

  it('Renders tooltip with correct position', () => {
    const tooltipComponent = screen.getByTestId('tooltip-component');
    expect(tooltipComponent.style.top).toBe('130px');
    expect(tooltipComponent.style.left).toBe('225px');
  });
});

describe('Translated Tooltip', () => {
  const tooltipTranslated = { ...tooltipMock, text: 'tip.buymeacoffee', isText: false };
  beforeEach(() => {
    vi.mocked(getVisibleTooltip).mockReturnValue(tooltipTranslated);
    renderWithProviders(<TooltipRedux />);
  });

  it('Renders translated tooltip text correctly', () => {
    const tooltipText = screen.getByText('Buy Me A Coffe');
    expect(tooltipText).toBeDefined();
  });
});

describe('Not displayed Tooltip', () => {
  const tooltipMockWithoutDisplay = { ...tooltipMock, displayTooltip: false };
  beforeEach(() => {
    renderWithProviders(<TooltipRedux />);
  });

  it('Does not render tooltip when displayTooltip is false', () => {
    vi.mocked(getVisibleTooltip).mockReturnValue(tooltipMockWithoutDisplay);
    const tooltipComponent = screen.queryByTestId('tooltip-component');
    const className = tooltipComponent?.getAttribute('class');
    expect(className).not.toContain(/display/);
  });
});

describe('Empty Tooltip', () => {
  const noVisibleTooltip = undefined;
  beforeEach(() => {
    renderWithProviders(<TooltipRedux />);
  });

  it('Does not render tooltip when selector returns null', () => {
    vi.mocked(getVisibleTooltip).mockReturnValue(noVisibleTooltip);
    const tooltipComponent = screen.queryByTestId('tooltip-component');
    const className = tooltipComponent?.getAttribute('class');
    expect(className).not.toContain(/display/);
  });
});
