/* eslint-disable @typescript-eslint/no-unused-vars */
import { describe, it, vi } from 'vitest';
import { Location } from 'react-router-dom';
import { screen } from '@testing-library/react';
import Menu from './Menu';
import { IconType } from '../Icon/Icon';
import { useMenuOpenStateStore } from '../../../store/MenuOpen';
import { renderWithProviders } from '../../../test-utils';

vi.mock('../../../store/MenuOpen');

const mockLocation: Location = {
  pathname: '/',
  state: {},
  key: '',
  search: '',
  hash: '',
};

vi.mock('./LinkTo', () => ({
  default: ({ displayMenuTitle, messageId, to }: LinkToProps) => {
    return (
      <a data-testid={`to-${messageId}`} href={to}>
        <div className="item">
          {displayMenuTitle && (
            <div className="menu-label" data-testid="menu-label">
              <span>{messageId}</span>
            </div>
          )}
        </div>
      </a>
    );
  },
}));

interface LinkToProps {
  className?: string;
  displayMenuTitle: boolean;
  isActive?: boolean;
  menuIcon: IconType;
  messageId: string;
  newTarget?: boolean;
  onClick?: void;
  testId?: string;
  to: string;
}

describe('Menu Component', () => {
  const setMenuOpenMockFunc = vi.fn((_val: boolean) => ({}));
  beforeEach(() => {
    vi.mocked(useMenuOpenStateStore).mockReturnValue({
      isOpen: true,
      setMenuOpen: setMenuOpenMockFunc,
    });
    renderWithProviders(<Menu location={mockLocation} />);
  });

  it('Renders Menu', () => {
    const menuComponent = screen.getByTestId('menu');
    expect(menuComponent).toBeDefined();
    const className = menuComponent.getAttribute('class');
    expect(className).toMatch(/open/);
  });

  it('Renders Menu links correctly', () => {
    const menuLinks = screen.getAllByTestId(/to-/);
    expect(menuLinks).toHaveLength(6); // Nombre total de liens dans le Menu
  });

  it('Toggles Menu on click', () => {
    const menuToggleIcon = screen.getByTestId('menu-toggle-icon');
    menuToggleIcon.click();
    expect(setMenuOpenMockFunc).toHaveBeenCalledTimes(1);
    expect(setMenuOpenMockFunc).toHaveBeenCalledWith(false);
  });

  it('Renders Menu title correctly', () => {
    const menuTitle = screen.getByText(/Menu/i);
    expect(menuTitle).toBeDefined();
  });
});

describe('Collapsed Menu Component', () => {
  const setMenuOpenMockFunc = vi.fn((_val: boolean) => ({}));
  beforeEach(() => {
    vi.mocked(useMenuOpenStateStore).mockReturnValue({
      isOpen: false,
      setMenuOpen: setMenuOpenMockFunc,
    });
    renderWithProviders(<Menu location={mockLocation} />);
  });

  it('Renders small Menu', () => {
    const menuComponent = screen.getByTestId('menu');
    const className = menuComponent.getAttribute('class');
    expect(className).not.toMatch(/open/);
  });

  it('Opens Menu on click', () => {
    const menuToggleIcon = screen.getByTestId('menu-toggle-icon');
    menuToggleIcon.click();
    expect(setMenuOpenMockFunc).toHaveBeenCalledTimes(1);
    expect(setMenuOpenMockFunc).toHaveBeenCalledWith(true);
  });
});
