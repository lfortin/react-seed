import { FC } from 'react';
import { Link } from 'react-router-dom';
import { useIntl } from 'react-intl';
import './Menu.css';
import Icon from '../Icon';
import { IconType } from '../Icon/Icon';

const MenuTitle: FC<MenutitleProps> = ({ id }) => {
  const intl = useIntl();
  return (
    <div className="menu-label" data-testid="menu-label">
      <span>{intl.formatMessage({ id })}</span>
    </div>
  );
};

interface MenutitleProps {
  id: string;
}

const LinkTo: FC<Props> = (props) => {
  const {
    className,
    displayMenuTitle = true,
    isActive = false,
    menuIcon,
    messageId,
    newTarget = false,
    onClick,
    testId,
    to,
  } = props;

  return (
    <Link
      data-testid={testId}
      to={to}
      {...(newTarget ? { target: '_blank' } : {})}
      className={`link-menu flex ${isActive ? 'active' : ''} ${className}`}
      onClick={onClick ?? undefined}
    >
      <div className="item">
        <Icon icon={menuIcon} size={36} />
        {displayMenuTitle && <MenuTitle id={messageId} />}
      </div>
    </Link>
  );
};

interface Props {
  className?: string;
  displayMenuTitle: boolean;
  isActive?: boolean;
  menuIcon: IconType;
  messageId: string;
  newTarget?: boolean;
  onClick?: void;
  testId?: string;
  to: string;
}

export default LinkTo;
