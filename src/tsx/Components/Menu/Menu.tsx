/* eslint-disable react-hooks/exhaustive-deps */
import { FC, ReactNode, memo, useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { Location } from 'react-router-dom';
import './Menu.css';
// redux
import { useMenuOpenStateStore } from '../../../store/MenuOpen';
import { RIGHT } from '../../../models/tooltips';
// components
import LinkTo from './LinkTo';
import LocaleSelector from '../LocaleSelector';
import ThemeSelector from '../ThemeSelector';
import Icon from '../Icon';
import { IconType, ICON_TYPE } from '../Icon/Icon';
// libs
import useTooltip from '../../../libs/utils/tooltips';

const OPEN_MENU_MIN_WIDTH = 750;

const LinkBtn: FC<LinkBtnProps> = ({ tooltipId, label, children, navbarMinimized }) => (
  <div
    ref={useTooltip(tooltipId, {
      text: label,
      displayTooltip: navbarMinimized,
      dir: RIGHT,
    })}
  >
    {children}
  </div>
);

interface LinkBtnProps {
  tooltipId: string;
  label: string;
  children: ReactNode;
  navbarMinimized: boolean;
}

const Menu: FC<Props> = ({ location }) => {
  const { isOpen, setMenuOpen } = useMenuOpenStateStore();
  const [isWideEnough, setWideEnough] = useState(window.innerWidth >= OPEN_MENU_MIN_WIDTH);
  const intl = useIntl();

  useEffect(() => {
    handleResize();
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [isWideEnough]);

  const { pathname } = location;

  const onChangeMenu = () => {
    setMenuOpen(!isOpen);
  };

  const handleResize = () => {
    const wideEnough = window.innerWidth >= OPEN_MENU_MIN_WIDTH;
    if (wideEnough !== isWideEnough || wideEnough !== isOpen) {
      setWideEnough(wideEnough);
    }
  };

  interface MenuElement {
    linkTo: string;
    className: string;
    displayMenuTitle: boolean;
    isActive: boolean;
    menuIcon: IconType;
    text: string;
    testId: string;
  }

  const menuElements: MenuElement[] = [
    {
      linkTo: '/home',
      className: '',
      displayMenuTitle: !!isOpen,
      isActive: pathname === '/home' || pathname.includes('/hero/'),
      menuIcon: ICON_TYPE.LIST,
      text: 'virtualList',
      testId: 'to-vl',
    },
    {
      linkTo: '/tab-alpha',
      className: '',
      displayMenuTitle: !!isOpen,
      isActive: ['/tab-alpha', '/tab-beta'].includes(pathname),
      menuIcon: ICON_TYPE.TAB,
      text: 'tabbed',
      testId: 'to-tab',
    },
    {
      linkTo: '/form',
      className: '',
      displayMenuTitle: !!isOpen,
      isActive: pathname === '/form',
      menuIcon: ICON_TYPE.FORM,
      text: 'form',
      testId: 'to-form',
    },
    {
      linkTo: '/notifications',
      className: '',
      displayMenuTitle: !!isOpen,
      isActive: pathname === '/notifications',
      menuIcon: ICON_TYPE.NOTIF,
      text: 'notification',
      testId: 'to-notif',
    },
    {
      linkTo: '/system',
      className: '',
      displayMenuTitle: !!isOpen,
      isActive: pathname === '/system',
      menuIcon: ICON_TYPE.STORE,
      text: 'system',
      testId: 'to-system',
    },
    {
      linkTo: '/thank-you',
      className: '',
      displayMenuTitle: !!isOpen,
      isActive: pathname === '/thank-you',
      menuIcon: ICON_TYPE.BEER,
      text: 'tip',
      testId: 'to-tips',
    },
  ];

  const openMenu = isOpen && isWideEnough;
  const menuIcon: IconType = openMenu ? ICON_TYPE.FOLD_MENU : ICON_TYPE.MENU;
  const msg: string = intl.formatMessage({
    id: `app.menu${openMenu ? '' : '_small'}`,
  });

  return (
    <div data-testid="menu" className={`menu ${openMenu ? 'open' : ''}`}>
      <div className="title">
        <span>{msg}</span>
        <div data-testid="menu-toggle-icon" className="icon" onClick={onChangeMenu}>
          <Icon icon={menuIcon} size={48} />
        </div>
      </div>
      <div className="menu-links grow">
        {menuElements.map((e: MenuElement) => (
          <LinkBtn
            key={e.text}
            tooltipId={`nav-${e.text}`}
            label={`pages.${e.text}`}
            navbarMinimized={!openMenu}
          >
            <LinkTo
              to={e.linkTo}
              className={e.className}
              displayMenuTitle={!!openMenu}
              isActive={e.isActive}
              menuIcon={e.menuIcon}
              messageId={`pages.${e.text}`}
              testId={e.testId}
            ></LinkTo>
          </LinkBtn>
        ))}
      </div>
      <div className="menu-bottom">
        <LocaleSelector isOpen={openMenu} />
        <ThemeSelector />
      </div>
    </div>
  );
};

interface Props {
  location: Location;
}

const areEquals = (props: Props, nextProps: Props) =>
  !!(props.location.pathname === nextProps.location.pathname);

export default memo(Menu, areEquals);
