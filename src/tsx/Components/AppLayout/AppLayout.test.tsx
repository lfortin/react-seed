import { describe, it, vi } from 'vitest';
import { FC } from 'react';
import { BrowserRouter, Location, Route, Routes } from 'react-router-dom';
import { screen } from '@testing-library/react';
import AppLayout from './AppLayout';
import { renderWithProviders } from '../../../test-utils';

const mockLocation: Location = {
  pathname: '/',
  state: {},
  key: '',
  search: '',
  hash: '',
};

const MockChildComponent: FC = () => <div>Mocked child component</div>;

vi.mock('../../../store/Title', () => ({
  useTitleStateStore: vi.fn(() => ({ title: 'title.home' })),
}));

describe('AppLayout Tests', () => {
  beforeEach(() => {
    renderWithProviders(
      <BrowserRouter>
        <Routes>
          <Route element={<AppLayout location={mockLocation} />}>
            <Route path="/" element={<MockChildComponent />} />
          </Route>
        </Routes>
      </BrowserRouter>
    );
  });

  describe('AppLayout Content', () => {
    it('Renders global App Layout', () => {
      const layout = screen.getByTestId('app-layout');
      expect(layout).toBeDefined();
    });

    it('Includes menu', () => {
      const menu = screen.getByTestId('menu');
      expect(menu).toBeDefined();
    });

    it('Includes content', () => {
      const content = screen.getByTestId('app-content');
      expect(content).toBeDefined();
    });

    it('Includes tooltips component', () => {
      const ttComponent = screen.getByTestId('tooltip-component');
      expect(ttComponent).toBeDefined();
    });
  });

  describe('AppLayout Behavior', () => {
    it('Sets document title when title prop is provided', () => {
      const mockTitle = 'Here we go again';
      expect(document.title).toEqual(mockTitle);
    });
  });
});
