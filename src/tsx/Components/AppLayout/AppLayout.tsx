import { FC } from 'react';
import { Location, Outlet } from 'react-router-dom';
import { useIntl } from 'react-intl';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './AppLayout.css';
// store
import { useTitleStateStore } from '../../../store/Title';
// Components
import Menu from '../Menu';
import TooltipRedux from '../Tooltips';

const AppLayout: FC<Props> = ({ location, ...rest }) => {
  const intl = useIntl();

  const { title } = useTitleStateStore();
  if (title) document.title = intl.formatMessage({ id: title });

  return (
    <div data-testid="app-layout" className="app flex">
      <Menu location={location} />
      <div data-testid="app-content" className="grow flex app-content">
        <Outlet context={{ ...rest }} />
      </div>
      <TooltipRedux />
      <ToastContainer />
    </div>
  );
};

interface Props {
  location: Location;
}

export default AppLayout;
