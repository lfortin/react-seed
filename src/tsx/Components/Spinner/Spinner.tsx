import { FC } from 'react';
import './Spinner.css';

const Spinner: FC<Props> = ({ size = 5 }) => {
  const spinnerStyle = {
    width: `${size * 20}px`,
    height: `${size * 20}px`,
    borderWidth: `${size * 2}px`, // Adjust border thickness based on size
  };

  return <div className="spinner" data-testid="spinner" style={spinnerStyle}></div>;
};

interface Props {
  size?: number;
}

export default Spinner;
