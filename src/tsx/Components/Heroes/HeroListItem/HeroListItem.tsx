import { FC } from 'react';
import { Hero } from '../../../../models/heroes';

const HeroListItem: FC<Props> = ({ hero }) => {
  return <div data-testid={`hero-list-item-${hero.id}`}>{hero.name}</div>;
};

interface Props {
  hero: Hero;
}

export default HeroListItem;
