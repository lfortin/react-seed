import { FC, memo } from 'react';
import './Hero.css';
import { Hero, HeroPicture } from '../../../../models/heroes';

const HeroComponent: FC<Props> = ({ hero }) => {
  if (!hero) return null;

  const getHeroImageUrl = (thumbnail: HeroPicture): string => `
	${thumbnail.path}.${thumbnail.extension}
`;

  return (
    <div data-testid="hero-card" className="hero-card">
      <h3 className="title">{hero.name}</h3>
      <p className="description">{hero.description}</p>
      <div className="flex-cc">
        <img
          alt={hero.name + '_picture'}
          className="picture"
          src={getHeroImageUrl(hero.thumbnail)}
        />
      </div>
    </div>
  );
};

interface Props {
  hero: Hero;
}

/**
 * @param {*} props
 * @param {*} nextProps
 * @returns true if component does not need to re-render
 */
const equalProps = (props: Props, nextProps: Props): boolean =>
  !!(props.hero && nextProps.hero && props.hero.id === nextProps.hero.id);

export default memo(HeroComponent, equalProps);
