import { describe, it } from 'vitest';
import { screen } from '@testing-library/react';
import Hero from './Hero';
import { renderWithProviders } from '../../../../test-utils';

const mockedMedia = {
  available: 0,
  collectionURI: 'http://mocked/collection/uri',
  items: [
    {
      ressourceURI: 'http://mocked/ressource/uri',
      name: 'mocked-name',
    },
  ],
  returned: 0,
};
const mockedUrl = { type: 'wiki', url: 'http:/wiki/mocked/url/1' };

const mockHero = {
  id: 1,
  name: 'Hero 1',
  description: 'Mocked Hero',
  modified: '',
  thumbnail: {
    path: 'https://images.unsplash.com/photo-1712743072516-13b19bc38840?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    extension: 'jpg',
  },
  ressourceURI: 'http://mocked/uri/1',
  comics: [mockedMedia],
  series: [mockedMedia],
  stories: [mockedMedia],
  events: [mockedMedia],
  urls: [mockedUrl],
};

describe('HeroContainer Component', () => {
  beforeEach(() => {
    renderWithProviders(<Hero hero={mockHero} />);
  });

  it('Renders Hero component', () => {
    const card = screen.getByTestId('hero-card');
    expect(card).toBeDefined();
  });

  it('Contains Hero name', () => {
    const name = screen.getByText(mockHero.name);
    expect(name).toBeDefined();
  });

  it('Contains Hero description', () => {
    const description = screen.getByText(mockHero.description);
    expect(description).toBeDefined();
  });

  it('Contains Hero image', () => {
    const image: HTMLImageElement = screen.getByAltText(`${mockHero.name}_picture`);
    expect(image.src).toContain(mockHero.thumbnail.path);
  });
});
