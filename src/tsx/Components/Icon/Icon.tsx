import { FC } from 'react';
import Ic from '@mdi/react';
import './Icon.css';

// import from https://pictogrammers.com/library/mdi/
import {
  mdiBeerOutline,
  mdiBellOutline,
  mdiCalendarMonthOutline,
  mdiCheckBold,
  mdiChevronLeftCircle,
  mdiClose,
  mdiCoffeeOutline,
  mdiEyeOutline,
  mdiEyeOffOutline,
  mdiFileMultipleOutline,
  mdiFileUploadOutline,
  mdiFormTextboxPassword,
  mdiListBoxOutline,
  mdiMagnify,
  mdiMenu,
  mdiMenuLeft,
  mdiSend,
  mdiServerOutline,
  mdiTable,
  mdiWeatherNight,
  mdiWhiteBalanceSunny,
} from '@mdi/js';

export const ICON_TYPE = {
  BACK: mdiChevronLeftCircle,
  BEER: mdiBeerOutline,
  CALENDAR: mdiCalendarMonthOutline,
  CANCEL: mdiClose,
  CHECKED: mdiCheckBold,
  COFFEE: mdiCoffeeOutline,
  DARK_MODE: mdiWeatherNight,
  DROP_ZONE: mdiFileMultipleOutline,
  EYE: mdiEyeOutline,
  EYE_OFF: mdiEyeOffOutline,
  FILE_UPLOAD: mdiFileUploadOutline,
  FOLD_MENU: mdiMenuLeft,
  FORM: mdiFormTextboxPassword,
  LIGHT_MODE: mdiWhiteBalanceSunny,
  LIST: mdiListBoxOutline,
  MENU: mdiMenu,
  NOTIF: mdiBellOutline,
  SEARCH: mdiMagnify,
  SEND: mdiSend,
  STORE: mdiServerOutline,
  TAB: mdiTable,
};

const DEFAUT_SIZE = 24;

export type IconType = (typeof ICON_TYPE)[keyof typeof ICON_TYPE];

const Icon: FC<Props> = ({ icon, iconClassName, size = DEFAUT_SIZE, color, title }) => {
  return (
    <div className={`app-icon ${iconClassName}`}>
      <Ic
        path={icon}
        title={title}
        color={color}
        style={{ height: `${size}px`, width: `${size}px` }}
      />
    </div>
  );
};

interface Props {
  icon: IconType;
  iconClassName?: string;
  size?: number;
  color?: string;
  title?: string;
}

export default Icon;
