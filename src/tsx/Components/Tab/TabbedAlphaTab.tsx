/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { useTitleStateStore } from '../../../store/Title';

const TabbedAlphaTab: FC<Props> = (props) => {
  const { message } = props;
  const { setTitle } = useTitleStateStore();
  const intl = useIntl();
  useEffect(() => {
    initTab();
    setTitle('title.tabbed.alpha');
  }, []);

  const initTab = () => console.log('init alpha tab data : ', props);

  return (
    <div>
      <p>Alpha TAB</p>
      <p className="message">
        {intl.formatMessage({ id: 'tabbed.message.from_parent' }, { message })}
      </p>
    </div>
  );
};

interface Props {
  message: string;
}

export default TabbedAlphaTab;
