import { TabsType } from '../generic/Tabs/constants';
import AlphaTab from './TabbedAlphaTab';
import BetaTab from './TabbedBetaTab';

export const TAB_ALPHA = 'TAB_ALPHA';
export const TAB_BETA = 'TAB_BETA';
export const TAB_OMEGA = 'TAB_OMEGA';

export const TABS: TabsType = {
  [TAB_ALPHA]: {
    key: 1,
    name: TAB_ALPHA,
    url: '/tab-alpha',
    title: 'alpha',
    Component: AlphaTab,
  },
  [TAB_BETA]: {
    key: 2,
    name: TAB_BETA,
    url: '/tab-beta',
    title: 'beta',
    Component: BetaTab,
  },
  [TAB_OMEGA]: {
    key: 3,
    name: TAB_OMEGA,
    url: '/tab-omega',
    title: 'omega',
    Component: BetaTab,
  },
};
