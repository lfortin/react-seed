/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { useTitleStateStore } from '../../../store/Title';

const TabbedBetaTab: FC<Props> = (props) => {
  const { message } = props;
  const { setTitle } = useTitleStateStore();
  const intl = useIntl();
  useEffect(() => {
    initTab();
    setTitle('title.tabbed.beta');
  }, []);

  const initTab = () => console.log('init beta tab data : ', props);

  return (
    <div>
      <p>Beta TAB</p>
      <p className="message">
        {intl.formatMessage({ id: 'tabbed.message.from_parent' }, { message })}
      </p>
    </div>
  );
};

interface Props {
  message: string;
}

export default TabbedBetaTab;
