/* eslint-disable @typescript-eslint/no-unused-vars */
import { describe, it, vi } from 'vitest';
import { fireEvent, screen } from '@testing-library/react';
import Input from './Input';
import { renderWithProviders } from '../../../../test-utils';

const onChangeFuncMocked = vi.fn((_val: string) => ({}));

describe('Input Component', () => {
  beforeEach(() => {
    renderWithProviders(
      <Input
        type="text"
        onChange={onChangeFuncMocked}
        testId="mocked-input"
        placeholder="mocked-placeholder"
        translatePh={false}
      />
    );
  });

  it('Renders input', () => {
    const element = screen.getByTestId('mocked-input');
    expect(element).toBeDefined();
    const className = element.getAttribute('class');
    expect(className).not.toMatch(/disabled/);
    const input = screen.getByPlaceholderText('mocked-placeholder');
    expect(input).toHaveProperty('disabled', false);
  });

  it('Renders disabled input', () => {
    renderWithProviders(
      <Input
        type="text"
        onChange={onChangeFuncMocked}
        testId="mocked-input-disabled"
        placeholder="mocked-placeholder-disabled"
        translatePh={false}
        disabled
      />
    );
    const element = screen.getByTestId('mocked-input-disabled');
    const className = element.getAttribute('class');
    expect(className).toMatch(/disabled/);

    const input = screen.getByPlaceholderText('mocked-placeholder-disabled');
    expect(input).toHaveProperty('disabled', true);
  });

  it('Renders with placeholder', () => {
    const input = screen.getByPlaceholderText('mocked-placeholder');
    expect(input).toBeDefined();
  });

  it('Renders with translated placeholder', () => {
    renderWithProviders(
      <Input type="text" onChange={onChangeFuncMocked} placeholder="form.placeholder.email" />
    );
    const input = screen.getByPlaceholderText('Email');
    expect(input).toBeDefined();
  });

  it('Calls onChange handler when input value changes', () => {
    const newValue = 'New Value';
    const input = screen.getByPlaceholderText('mocked-placeholder');
    fireEvent.change(input, { target: { value: newValue } });
    expect(onChangeFuncMocked).toHaveBeenCalledWith(newValue);
    expect(onChangeFuncMocked).toHaveBeenCalledTimes(1);
  });
});
