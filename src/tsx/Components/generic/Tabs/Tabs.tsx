import { FC } from 'react';
import './Tabs.css';
import Tab from './Tab';
import { TabState, TabType, TabsType } from './constants';

const Tabs: FC<Props> = ({ disabledTabs, nameComplement, state, view, TABS, onToggleTab }) => {
  const tabClassname = (tab: TabType): string => {
    let className = tab.name.toLowerCase();
    if (disabledTabs.includes(tab.key)) className += ' disabled-tab';
    return className;
  };

  const tabName = (t: TabType): string =>
    `tabs.${view}.${t.name.toLowerCase()}${
      nameComplement?.[t.key] ? `.${nameComplement[t.key]}` : ''
    }`;

  return (
    <div className="all-tabs">
      <div className="tabs">
        {Object.values(TABS).map((t: TabType) => (
          <Tab
            key={t.key}
            title={tabName(t)}
            active={state.currentTab.key === t.key}
            className={tabClassname(t)}
            eventKey={t.key}
            onToggleTab={onToggleTab}
          />
        ))}
      </div>
    </div>
  );
};

interface Props {
  disabledTabs: number[];
  nameComplement?: string;
  state: TabState;
  view: string;
  TABS: TabsType;
  onToggleTab: (key: number) => void;
}

export default Tabs;
