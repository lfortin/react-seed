import { FC, ReactNode } from 'react';
import { FormattedMessage } from 'react-intl';
import './Tabs.css';

const Tab: FC<Props> = ({ active, className, eventKey, title, testId, onToggleTab, children }) => {
  return (
    <div
      className={`tab ${className} ${active && 'active'}`}
      data-testid={testId}
      onClick={() => onToggleTab(eventKey)}
    >
      <FormattedMessage id={title} />
      {children}
    </div>
  );
};

interface Props {
  active: boolean;
  className: string;
  eventKey: number;
  testId?: string;
  title: string;
  onToggleTab: (key: number) => void;
  children?: ReactNode;
}
export default Tab;
