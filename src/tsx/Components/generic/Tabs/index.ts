import TabView from './TabView';
import Tabs from './Tabs';
import Tab from './Tab';

export { TabView, Tabs, Tab };
