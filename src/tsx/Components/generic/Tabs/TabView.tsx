/* eslint-disable react-hooks/exhaustive-deps */
import { ComponentType, FC, useEffect, useState } from 'react';
import { Location, useNavigate } from 'react-router-dom';
import './Tabs.css';
import Tabs from './Tabs';
import { TabType, TabsType } from './constants';

const TabView: FC<Props> = (props) => {
  const { children, className, defaultTab, disabledTabs = [] } = props;
  const { location, parentTab, tabsProps, tabViewProps, view, TABS } = props;
  const { getTabFunc, setTitle = () => null } = props;

  const tabName = Object.values(TABS).find((t) => t.url === location.pathname)?.name;

  const [currentTab, setCurrentTab] = useState(TABS[tabName ?? defaultTab]);

  useEffect(() => {
    setTitle(currentTab.title);
  }, []);

  useEffect(() => {
    const tab = getTabFunc
      ? getTabFunc(location, TABS)
      : Object.values(TABS).find((t) => t.url === location.pathname);
    if (tab) {
      setCurrentTab(tab);
      setTitle(tab.title);
    }
  }, [location]);

  const onToggleTab = (eventKey: number) => {
    if (disabledTabs.includes(eventKey)) return;
    const tab = Object.values(TABS).find((tab) => tab.key === eventKey);
    if (!tab) return;
    goToTab(tab);
  };

  const navigate = useNavigate();

  const goToTab = (tab: TabType) => {
    if (tab.url) navigate(tab.url);
    else {
      // will be done in useEffet
      setCurrentTab(tab);
      setTitle(tab.title);
    }
    if (parentTab) parentTab(tab);
  };

  const CurrentTabView = currentTab.Component;

  return (
    <>
      <Tabs
        disabledTabs={disabledTabs}
        onToggleTab={onToggleTab}
        state={{ currentTab }}
        view={view}
        TABS={TABS}
        {...tabsProps}
      />
      <div className={`${className} ${currentTab.name.toLowerCase()}`}>
        <CurrentTabView {...tabsProps} {...tabViewProps} />
      </div>
      {children}
    </>
  );
};

interface Props {
  children?: ComponentType;
  className: string;
  defaultTab: string;
  disabledTabs?: number[];
  getTabFunc: (location: Location, TABS: TabsType) => TabType;
  location: Location;
  parentTab?: (tab: TabType) => void;
  setTitle?: (title?: string) => void;
  tabsProps: object;
  tabViewProps?: object;
  view: string;
  TABS: TabsType;
}

export default TabView;
