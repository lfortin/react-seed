/* eslint-disable @typescript-eslint/no-explicit-any */
export interface TabType {
  key: number;
  name: string;
  url: string;
  title?: string;
  Component: React.FC<any>;
}

export type TabsType = Record<string, TabType>;

export interface TabState {
  currentTab: TabType;
}
