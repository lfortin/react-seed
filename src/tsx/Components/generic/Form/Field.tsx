import { Dispatch, ElementType, ReactNode, SetStateAction } from 'react';
import {
  Control,
  Controller,
  ControllerRenderProps,
  FieldErrors,
  FieldValues,
  Path,
  PathValue,
  RegisterOptions,
  UseFormSetValue,
  UseFormTrigger,
} from 'react-hook-form';
import { useIntl } from 'react-intl';
import './Form.css';
import { DropdownOption, SelectedOption } from '../Dropdown/Dropdown';

const FormField = <T extends FieldValues>(props: Props<T>) => {
  const { name, className = '', type, control, registerOptions } = props;
  const { errors, setValue, trigger } = props;
  const { Component, componentProps, getComponentProps, iconComponent } = props;

  const intl = useIntl();

  const label = intl.formatMessage({ id: `form.field.${name}` });
  const errorMessage: string | undefined = errors?.[name]?.message as string;

  const required = !!registerOptions[name]?.required;

  const onChangeFunc = (val: PathValue<T, Path<T>>): void => {
    const options: DropdownOption[] = componentProps?.options ?? [];
    let selectedOption = options[0];
    switch (type) {
      case 'boolean':
      case 'text':
        setValue(name, val);
        trigger && void trigger(name);
        break;
      case 'files':
      case 'date':
        setValue(name, val);
        break;
      case 'dropdown':
        selectedOption = options.find((o) => o.value === val) ?? options[0];
        setValue(name, selectedOption as PathValue<T, Path<T>>);
        break;
      default:
        break;
    }
  };

  const formAttrs = (field: ControllerRenderProps<T, Path<T>>) =>
    getComponentProps ? getComponentProps(field) : {};

  return (
    <div className={`${className} ${required ? 'required' : ''}`}>
      <span className="label">{label}</span>
      <div className="field">
        <div className="field-value">
          <Controller
            name={name}
            control={control}
            rules={registerOptions[name]}
            render={({ field }) => (
              <Component
                {...field}
                {...componentProps}
                {...formAttrs(field)}
                onChange={onChangeFunc}
                ref={null}
              />
            )}
          />
          <small className={`text-danger ${errorMessage ? 'show-error' : ''}`}>
            {errorMessage}
          </small>
        </div>
        <div className="field-icon">{iconComponent}</div>
      </div>
    </div>
  );
};

interface IComponentProps {
  // general
  placeholder?: string;
  type?: string;
  // dropdown
  options?: DropdownOption[];
  // date picker
  maxDate?: Date;
  minDate?: Date;
  showCalendarIcon?: false;
  isOpen?: boolean;
  setOpen?: Dispatch<SetStateAction<boolean>>;
}

interface IGetComponentPropsResponse {
  value?: unknown; // string, number
  checked?: boolean; // switch, checkbox
  validFiles?: File[]; // File input
  selectedDate?: Date; // date picker
  selectedOption?: SelectedOption; // dropdown
}

interface Props<T extends FieldValues> {
  name: Path<T>;
  className?: string;
  type: string;
  control: Control<T>;
  registerOptions: Record<keyof T, RegisterOptions>;
  errors: FieldErrors<T>;
  setValue: UseFormSetValue<T>;
  trigger?: UseFormTrigger<T>;
  Component: ElementType;
  componentProps?: IComponentProps;
  getComponentProps?: (field: ControllerRenderProps<T, Path<T>>) => IGetComponentPropsResponse;
  iconComponent?: ReactNode;
}

export default FormField;
