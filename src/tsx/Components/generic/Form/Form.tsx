/* eslint-disable @typescript-eslint/no-misused-promises */
import { ReactNode } from 'react';
import { FieldValues, UseFormReset } from 'react-hook-form';
import { useIntl } from 'react-intl';
import './Form.css';
// components
import Button from '../Button';

const Form = <T extends FieldValues>(props: Props<T>) => {
  const { children, className = '', onSubmit, reset } = props;
  const intl = useIntl();
  const onReset = () =>
    reset && reset(undefined, { keepErrors: false, keepDirty: false, keepTouched: false });

  return (
    <div className={className}>
      <form onSubmit={onSubmit}>
        {children}
        {reset && (
          <Button className="reverse reset-form-btn" onClick={onReset}>
            <span>{intl.formatMessage({ id: 'form.reset' })}</span>
          </Button>
        )}
      </form>
    </div>
  );
};

interface Props<T extends FieldValues> {
  children: ReactNode;
  className?: string;
  onSubmit: (e?: React.BaseSyntheticEvent<object, unknown, unknown> | undefined) => Promise<void>;
  reset?: UseFormReset<T>;
}

export default Form;
