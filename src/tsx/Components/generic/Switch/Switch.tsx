import { FC } from 'react';
import './Switch.css';

const Switch: FC<Props> = ({ checked, className, disabled, size = 25, testId, onChange }) => {
  interface BtnStyle {
    height?: string;
    width?: string;
  }
  const style: BtnStyle = {
    height: `${size}px`,
    width: `${size * 2}px`,
  };
  return (
    <div className={`switch-container ${className}`}>
      <label className={disabled ? 'disabled' : ''}>
        <div data-testid={testId}>
          <input
            id="switch"
            data-testid={`${testId}-input`}
            checked={checked}
            className={`tgl ${checked ? 'checked' : ''}`}
            type="checkbox"
            disabled={disabled}
            onChange={() => !disabled && onChange(!checked)}
          />
          <label className="tgl-btn" htmlFor="switch" style={style}></label>
        </div>
      </label>
    </div>
  );
};

interface Props {
  checked?: boolean;
  className?: string;
  disabled?: boolean;
  size?: number;
  testId?: string;
  onChange: (val: boolean) => void;
}

export default Switch;
