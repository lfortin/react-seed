/* eslint-disable @typescript-eslint/no-unused-vars */
import { describe, it, vi } from 'vitest';
import { fireEvent, screen } from '@testing-library/react';
import Switch from './Switch';
import { renderWithProviders } from '../../../../test-utils';

const onChangeFuncMocked = vi.fn((_val: boolean) => ({}));

describe('Switch Component', () => {
  it('Renders Switch', () => {
    renderWithProviders(<Switch onChange={onChangeFuncMocked} testId="mocked-switch" />);
    const element = screen.getByTestId('mocked-switch');
    expect(element).toBeDefined();

    const input = screen.getByTestId('mocked-switch-input');
    expect(input).toHaveProperty('disabled', false);

    const className = input.getAttribute('class');
    expect(className).toMatch(/tgl/);
    expect(className).not.toMatch(/checked/);
  });

  it('Renders disabled Switch', () => {
    renderWithProviders(<Switch onChange={onChangeFuncMocked} testId="mocked-switch" disabled />);
    const input = screen.getByTestId('mocked-switch-input');
    expect(input).toHaveProperty('disabled', true);
  });

  it('Renders checked Switch', () => {
    renderWithProviders(<Switch onChange={onChangeFuncMocked} testId="mocked-switch" checked />);
    const input = screen.getByTestId('mocked-switch-input');
    expect(input).toHaveProperty('checked', true);
    const className = input.getAttribute('class');
    expect(className).toMatch(/checked/);
  });

  it('Calls onChange handler when clicked with value true', () => {
    const onChangeFuncTestMocked = vi.fn((_val: boolean) => ({}));
    renderWithProviders(<Switch onChange={onChangeFuncTestMocked} testId="mocked-switch" />);
    const input = screen.getByTestId('mocked-switch-input');
    fireEvent.click(input);
    expect(onChangeFuncTestMocked).toHaveBeenCalledTimes(1);
    expect(onChangeFuncTestMocked).toHaveBeenCalledWith(true);
  });

  it('Calls onChange handler when clicked with value true', () => {
    const onChangeFuncTestMocked = vi.fn((_val: boolean) => ({}));
    renderWithProviders(
      <Switch onChange={onChangeFuncTestMocked} testId="mocked-switch" checked />
    );
    const input = screen.getByTestId('mocked-switch-input');
    fireEvent.click(input);
    expect(onChangeFuncTestMocked).toHaveBeenCalledTimes(1);
    expect(onChangeFuncTestMocked).toHaveBeenCalledWith(false);
  });

  it('Do not calls onChange handler when clicked and disabled', () => {
    const onChangeFuncTestMocked = vi.fn((_val: boolean) => ({}));
    renderWithProviders(
      <Switch onChange={onChangeFuncTestMocked} testId="mocked-switch" disabled />
    );
    const input = screen.getByTestId('mocked-switch-input');
    fireEvent.click(input);
    expect(onChangeFuncTestMocked).toHaveBeenCalledTimes(0);
  });
});
