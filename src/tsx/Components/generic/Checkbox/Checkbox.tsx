import { FC } from 'react';
import './Checkbox.css';
import Icon from '../../Icon';
import { ICON_TYPE } from '../../Icon/Icon';

const Checkbox: FC<Props> = ({ checked, className, disabled, onChange, size = 16, testId }) => {
  interface BoxStyle {
    height?: string;
    width?: string;
  }
  const style: BoxStyle = {
    height: `${size}px`,
    width: `${size}px`,
  };

  return (
    <div className={`checkbox-container ${className}`}>
      <label className={disabled ? 'disabled' : ''}>
        <div
          data-testid={testId}
          className={`${checked ? 'checked' : ''} ${disabled ? 'disabled' : ''}`}
          style={style}
        >
          <input
            data-testid={`${testId}-input`}
            checked={checked ?? false}
            className="display-none"
            disabled={disabled}
            onChange={() => !disabled && onChange(!checked)}
            type="checkbox"
          />
          {checked ? <Icon icon={ICON_TYPE.CHECKED} size={size} /> : <></>}
        </div>
      </label>
    </div>
  );
};

interface Props {
  checked?: boolean;
  className?: string;
  disabled?: boolean;
  size?: number;
  testId?: string;
  onChange: (val: boolean) => void;
}

export default Checkbox;
