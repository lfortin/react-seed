/* eslint-disable @typescript-eslint/no-unused-vars */
import { describe, it, vi } from 'vitest';
import { fireEvent, screen } from '@testing-library/react';
import Checkbox from './Checkbox';
import { renderWithProviders } from '../../../../test-utils';

describe('Checkbox Component', () => {
  it('Renders checkbox', () => {
    const onChangeFuncMocked = vi.fn((_val: boolean) => ({}));

    renderWithProviders(
      <Checkbox testId="mocked-checkbox" checked={false} onChange={onChangeFuncMocked} />
    );
    const checkbox = screen.getByTestId('mocked-checkbox');
    expect(checkbox).toBeDefined();

    const checkboxInput = screen.getByTestId('mocked-checkbox-input');
    expect(checkboxInput).toBeDefined();

    const className = checkboxInput.getAttribute('class');
    expect(className).toMatch(/display-none/);
  });

  it('Renders checked checkbox', () => {
    renderWithProviders(
      <Checkbox testId="mocked-checked-checkbox" checked={true} onChange={() => vi.fn()} />
    );
    const checkbox = screen.getByTestId('mocked-checked-checkbox');
    const className = checkbox.getAttribute('class');
    expect(className).toMatch(/checked/);
  });

  it('Renders unchecked checkbox', () => {
    const onChangeFuncMocked = vi.fn((_val: boolean) => ({}));
    renderWithProviders(
      <Checkbox testId="mocked-checkbox" checked={false} onChange={onChangeFuncMocked} />
    );
    const checkbox = screen.getByTestId('mocked-checkbox');
    const className = checkbox.getAttribute('class');
    expect(className).not.toMatch(/checked/);
  });

  it('Renders disabled checkbox', () => {
    renderWithProviders(
      <Checkbox testId="mocked-disabled-checkbox" onChange={() => vi.fn()} disabled />
    );
    const checkbox = screen.getByTestId('mocked-disabled-checkbox');
    const className = checkbox.getAttribute('class');
    expect(className).toMatch(/disabled/);
  });

  it('Calls onChange handler when clicked with value true', () => {
    const onChangeFuncMocked = vi.fn((_val: boolean) => ({}));
    renderWithProviders(
      <Checkbox testId="mocked-checkbox" checked={false} onChange={onChangeFuncMocked} />
    );
    const checkboxInput = screen.getByTestId('mocked-checkbox-input');
    fireEvent.click(checkboxInput);

    expect(onChangeFuncMocked).toHaveBeenCalledTimes(1);
    expect(onChangeFuncMocked).toHaveBeenCalledWith(true);
  });

  it('Calls onChange handler when clicked with value false', () => {
    const onChangeFuncMocked = vi.fn((_val: boolean) => ({}));
    renderWithProviders(
      <Checkbox testId="mocked-checked-checkbox" checked={true} onChange={onChangeFuncMocked} />
    );
    const checkboxInput = screen.getByTestId('mocked-checked-checkbox-input');
    fireEvent.click(checkboxInput);
    expect(onChangeFuncMocked).toHaveBeenCalledTimes(1);
    expect(onChangeFuncMocked).toHaveBeenCalledWith(false);
  });

  it('Do not calls onChange handler when clicked and disabled', () => {
    const onChangeFuncTestMocked = vi.fn((_val: boolean) => ({}));
    renderWithProviders(
      <Checkbox testId="mocked-disabled-checkbox" onChange={onChangeFuncTestMocked} disabled />
    );
    const input = screen.getByTestId('mocked-disabled-checkbox-input');
    fireEvent.click(input);
    expect(onChangeFuncTestMocked).toHaveBeenCalledTimes(0);
  });
});
