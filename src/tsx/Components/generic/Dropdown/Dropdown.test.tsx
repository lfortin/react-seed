/* eslint-disable @typescript-eslint/no-unused-vars */
import { describe, it, vi } from 'vitest';
import { fireEvent, screen } from '@testing-library/react';
import Dropdown, { DropdownOption, SelectedOption } from './Dropdown';
import { renderWithProviders } from '../../../../test-utils';

const options: DropdownOption[] = [
  { label: 'Mocked 1', value: 1 },
  { label: 'Mocked 2', value: 2 },
  { label: 'Mocked 3', value: 3 },
];
const onChangeFuncMocked = vi.fn((_val: SelectedOption) => ({}));

describe('Dropdown Component', () => {
  beforeEach(() => {
    renderWithProviders(
      <Dropdown
        options={options}
        onChange={onChangeFuncMocked}
        testId="mocked-dropdown"
        className="mocked-className"
      />
    );
  });

  it('Renders dropdown', () => {
    const dropdown = screen.getByTestId('mocked-dropdown');
    expect(dropdown).toBeDefined();

    const className = dropdown.getAttribute('class');
    expect(className).toMatch(/mocked-className/);
    expect(className).toMatch(/select/);
  });

  it('Renders options correctly', () => {
    options.forEach((option) => {
      expect(screen.getByText(option.label)).toBeDefined();
    });
  });

  it('Renders options correctly', () => {
    options.forEach((option) => {
      expect(screen.getByText(option.label)).toBeDefined();
    });
  });

  it('Calls onChange handler when an option is selected', () => {
    const optionToSelect: SelectedOption = '2';
    const dropdown = screen.getByTestId('mocked-dropdown');
    fireEvent.change(dropdown, { target: { value: optionToSelect } });
    expect(onChangeFuncMocked).toHaveBeenCalledWith(optionToSelect);
    expect(onChangeFuncMocked).toHaveBeenCalledTimes(1);
  });
});
