import { FC, MouseEventHandler, ReactNode } from 'react';
import './Button.css';

const Button: FC<Props> = ({
  className = '',
  disabled = false,
  onClick,
  type = 'button',
  children,
}) => (
  <button
    disabled={disabled}
    className={`app-button ${className} ${disabled ? 'disabled' : ''}`}
    type={type}
    onClick={onClick}
  >
    {children}
  </button>
);

interface Props {
  children: ReactNode;
  className?: string;
  disabled?: boolean;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  type?: 'button' | 'submit' | 'reset' | undefined;
}

export default Button;
