import { describe, it, vi } from 'vitest';
import { fireEvent, screen } from '@testing-library/react';
import Button from './Button';
import { renderWithProviders } from '../../../../test-utils';

describe('Button Component', () => {
  beforeEach(() => {
    renderWithProviders(<Button onClick={() => vi.fn()}>Text</Button>);
  });

  it('Should render label correctly', () => {
    const buttonLabel = screen.getByText(/^Text$/);
    expect(buttonLabel).toBeDefined();
  });

  it('Should call onClick successfully', () => {
    const spy = vi.spyOn(vi, 'fn');
    fireEvent.click(screen.getByText(/^Text$/));
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
