import { describe, it, vi } from 'vitest';
import { screen } from '@testing-library/react';
import VirtualList from './VirtualList';
import { renderWithProviders } from '../../../../test-utils';
import { ReactNode } from 'react';

const instances = [
  { id: 1, name: 'Item 1' },
  { id: 2, name: 'Item 2' },
  { id: 3, name: 'Item 3' },
];
const RowComponent = (idx: number) => <div key={instances[idx].id}>{instances[idx].name}</div>;

const EmptyComponent = () => <div>Empty mocked list</div>;

vi.mock('react-virtuoso', () => ({
  Virtuoso: ({ itemContent, totalCount, style }: VirtuosoProps) => {
    const array = Array.from({ length: totalCount }, (_, index) => index);
    return (
      <div style={style}>
        {array.map((i) => (
          <div key={i}>{itemContent(i)}</div>
        ))}
      </div>
    );
  },
}));

interface VirtuosoProps {
  itemContent: (i: number) => ReactNode;
  totalCount: number;
  style: object;
}

describe('VirtualList Component', () => {
  beforeEach(() => {
    renderWithProviders(
      <VirtualList instances={instances} RowComponent={RowComponent} testId="mocked-list" />
    );
  });

  it('Renders VirtualList', () => {
    const list = screen.getByTestId('mocked-list');
    expect(list).toBeDefined();
  });

  it('Renders RowComponent for each instance', () => {
    instances.forEach((instance) => {
      expect(screen.getByText(instance.name)).toBeDefined();
    });
  });

  test('Renders EmptyComponent when instances array is empty', () => {
    renderWithProviders(
      <VirtualList
        instances={[]}
        RowComponent={RowComponent}
        EmptyComponent={EmptyComponent}
        testId="mocked-empty-list"
      />
    );
    const list = screen.getByTestId('mocked-empty-list');
    expect(list).toBeDefined();

    const emptyComponent = screen.getByText('Empty mocked list');
    expect(emptyComponent).toBeDefined();
  });
});
