import { FC, ReactNode, useEffect } from 'react';
import { useDropzone } from 'react-dropzone';
import { FormattedMessage } from 'react-intl';
import { Dispatch } from 'redux';
import './FileUpload.css';
import Icon from '../../Icon';
import { ICON_TYPE } from '../../Icon/Icon';

const DefaultOverlay: FC = () => (
  <div className="dropzone-overlay" id="dropzone-overlay">
    <div className="overlay-content">
      <Icon icon={ICON_TYPE.DROP_ZONE} size={48} />
    </div>
  </div>
);

const DisableDropzone: FC<DisabledDZProps> = ({ children }) => (
  <div className="dropzone">
    <div className="dropzone-overlay" id="dropzone-overlay">
      <div className="overlay-content">
        <FormattedMessage id="file.dropzone.disabled" />
      </div>
    </div>
    {children}
  </div>
);

interface DisabledDZProps {
  children?: ReactNode;
}

const FileUploadDropzone: FC<Props> = (props) => {
  const {
    allowPasteFiles = false,
    children,
    className = '',
    disable,
    noClick = true,
    _onDrop,
  } = props;

  const onPaste = (evt: ClipboardEvent) => {
    if (allowPasteFiles) {
      // Get the data of clipboard
      const clipboardItems = evt.clipboardData?.items ?? [];
      if (clipboardItems.length === 0) {
        return;
      }
      const item = clipboardItems[0];

      let blob: File | null;

      switch (item.kind) {
        case 'file':
          // Get the blob of image
          blob = item.getAsFile();
          if (blob) {
            //  If it's a folder, we don't want to paste it
            if (blob.size === 4096 && !item.type) {
              return;
            }

            const tzoffset = new Date().getTimezoneOffset() * 60000; // offset in milliseconds
            const date = new Date(Date.now() - tzoffset)
              .toISOString()
              .replace(/[a-zA-Z]/g, '_')
              .replace(/:/g, '')
              .slice(0, -7);

            const fileName = blob.name === 'image.png' ? `Screenshot_${date}.jpeg` : blob.name;

            const file = new File([blob], fileName, {
              type: 'image/jpeg',
              lastModified: Date.now(),
            });
            if (file.size > 0) {
              void onDrop([file]);
            }
          }

          break;
        default:
          break;
      }
    }
  };

  useEffect(() => {
    document.addEventListener('paste', onPaste);

    return () => {
      document.removeEventListener('paste', onPaste);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /**
   * Creates a folder in the cloud, upload its files and creates sub-folders recursively
   *
   * @param {[File]} files : The files in a folder
   * @param {object} parent : The parent folder. Should be updated to create sub-folders and uploads in the right folder recursively
   * @param {string} path : the path relative to the parent folder
   */
  // const handleFolder = (files, parent, path = '') => {
  //   const getSplittedPath = (file) => file.path.substr(path.length + 1).split('/');
  //   const isDirectChild = (file) => getSplittedPath(file).length === 2;

  //   const findFolder = (name, parent, files = cloudFiles) =>
  //     Object.values(files)
  //       .filter((f) => f.isFolder && f.name === name && f.parentId === parent.id)
  //       .sort((a, b) => (a.createdAt > b.createdAt ? -1 : b.createdAt > a.createdAt ? 1 : 0))[0];

  //   const handleChildren = (files, others, name, parent) => {
  //     onDrop(files, { ...parent, type: Enum.file.parentType.FOLDER });
  //     if (others.length && path.length === 0) handleFolder(others, parent, path + '/' + name);
  //   };
  //   const createFolderWithChildren = (name, files, others) =>
  //     createFolder(parent.id, name).then((allFiles) => {
  //       const created = findFolder(name, parent, allFiles);
  //       handleChildren(files, others, name, created);
  //     });

  //   files = files.map((f) => Object.assign(f, { folder: getSplittedPath(f)[0] }));
  //   const byFolder = _groupBy(files, 'folder');
  //   Object.keys(byFolder).forEach((name) => {
  //     const directChildren = byFolder[name].filter(isDirectChild);
  //     const others = byFolder[name].filter((f) => !isDirectChild(f));
  //     const correctFiles = checkUploadLimitations(currentUser, directChildren);
  //     const existingFolder = findFolder(name, parent);
  //     if (existingFolder) {
  //       const onConfirm = () => {
  //         // create a new folder with same name
  //         createFolderWithChildren(name, correctFiles, others);
  //       };
  //       const onCancel = () => {
  //         // upload in existing folder
  //         handleChildren(correctFiles, others, name, { ...existingFolder });
  //       };
  //       dispatch(
  //         displayModal(ModalTypes.CONFIRMATION, {
  //           text: translator.t('cloud.upload.folder.choice', { name }),
  //           onConfirm,
  //           onCancel,
  //           validationId: 'cloud.upload.folder.choice.create',
  //           cancelId: 'cloud.upload.folder.choice.merge',
  //         })
  //       );
  //     } else {
  //       createFolderWithChildren(name, correctFiles, others);
  //     }
  //   });
  // };

  const onDrop = (acceptedFiles: File[]): void => {
    const isFolder = (file: File) => file.size % 4096 === 0 && file.type === '';
    // const isFolder = (file: any) => file.path && file.path !== file.name;
    /*
     * Folder not supported
     * Files with 0B not supported
     * => filter on file size before upload
     */
    const files = acceptedFiles.filter((file) => file.size > 0 && !isFolder(file));
    const nullFiles = acceptedFiles.filter((file) => file.size === 0 && !isFolder(file));
    const valid = files;

    if (valid.length < acceptedFiles.length) {
      const errorMsg = {
        file: false,
      };

      nullFiles.forEach((file) => {
        if (file.type && file.type !== '' && !errorMsg.file) {
          errorMsg.file = true;
          console.log('error.file_null_not_supported');
        }
      });
    }

    const correctFiles = valid;

    if (correctFiles.length > 0) {
      _onDrop(correctFiles);
    }
  };

  if (disable) return <DisableDropzone />;
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    noClick,
  });

  const rootClass = `${className} dropzone ${isDragActive ? 'active' : ''}`;

  return (
    <div {...getRootProps({ className: rootClass })}>
      <input {...getInputProps()} />
      <>
        {isDragActive && <DefaultOverlay />}
        {children}
      </>
    </div>
  );
};

interface Props {
  allowPasteFiles?: boolean;
  children: ReactNode;
  className?: string;
  disable?: boolean;
  dispatch?: Dispatch;
  noClick?: boolean;
  _onDrop: (files: File[]) => void;
}

export default FileUploadDropzone;
