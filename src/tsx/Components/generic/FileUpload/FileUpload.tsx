/* eslint-disable react-hooks/exhaustive-deps */
import { ChangeEvent, FC, useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import './FileUpload.css';
import { LEFT } from '../../../../models/tooltips';
import FileUploadDropzone from './FileUploadDropzone';
import Icon, { ICON_TYPE } from '../../Icon/Icon';
import useTooltip from '../../../../libs/utils/tooltips';

const FileUpload: FC<Props> = ({ validFiles = [], onChange }) => {
  const [files, setFiles] = useState<File[]>([]);
  const intl = useIntl();

  useEffect(() => {
    if (JSON.stringify(files) !== JSON.stringify(validFiles)) {
      setFiles(validFiles);
    }
  }, [validFiles]);

  const onAddFiles = (e: ChangeEvent<HTMLInputElement>): void => {
    const selectedFiles = e.target.files! ?? [];

    const inputFiles = Object.values(selectedFiles).map((f) => f);

    if (inputFiles.length === 0) return;

    setFilesValue(files.concat(inputFiles));
    e.target.value = '';
  };

  const onRemoveFile = (i: number) => {
    const newFiles = [...files];
    if (i < 0 || i >= newFiles.length) {
      console.error('Élément inexistant');
      setFilesValue(newFiles);
    } else {
      newFiles.splice(i, 1);
      setFilesValue(newFiles);
    }
  };

  const onClear = () => {
    setFilesValue([]);
  };

  const setFilesValue = (files: File[]) => {
    onChange && onChange(files);
  };

  const onDrop = (droppedFiles: File[]) => {
    setFilesValue(files.concat(droppedFiles));
  };

  return (
    <div className="file-upload-container">
      <FileUploadDropzone allowPasteFiles className="" _onDrop={onDrop}>
        <div className="files-list">
          {files.length > 0 ? (
            <>
              {files.map((f: File, i: number) => (
                <div className="selected-file" key={`file-${i}`}>
                  <span className="ellipsis">{f.name}</span>
                  <div
                    className="ml5"
                    onClick={() => {
                      onRemoveFile(i);
                    }}
                  >
                    <Icon icon={ICON_TYPE.CANCEL} />
                  </div>
                </div>
              ))}
            </>
          ) : (
            <div className="no-files-msg">
              <div className="msg-row">{intl.formatMessage({ id: 'files-input.no_file' })}</div>
              <div className="msg-row">{intl.formatMessage({ id: 'files-input.drag_n_drop' })}</div>
              <div className="msg-row">{intl.formatMessage({ id: 'files-input.paste' })}</div>
            </div>
          )}
        </div>
      </FileUploadDropzone>
      <input
        data-testid="file-upload-input"
        id="file"
        className="files-input"
        type="file"
        onChange={onAddFiles}
        multiple
        accept="image/png, image/gif, image/jpeg"
      />
      <div className="file-upload-actions">
        <div
          ref={useTooltip('upload-files-btn', {
            text: 'files-input.upload_files',
            dir: LEFT,
          })}
        >
          <label htmlFor="file" data-testid="select-file-upload">
            <Icon icon={ICON_TYPE.FILE_UPLOAD} />
          </label>
        </div>
        <div
          data-testid="clear-file-upload"
          onClick={onClear}
          ref={useTooltip('clear-files-btn', {
            text: 'files-input.clear_files',
            dir: LEFT,
          })}
        >
          <Icon icon={ICON_TYPE.CANCEL} />
        </div>
      </div>
    </div>
  );
};

interface Props {
  validFiles?: File[];
  onChange?: (value: File[]) => void;
}

export default FileUpload;
