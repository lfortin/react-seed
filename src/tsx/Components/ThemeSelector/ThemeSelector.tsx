import { FC } from 'react';
import './ThemeSelector.css';
import { useThemeStateStore } from '../../../store/Theme';
import Icon from '../Icon';
import { ICON_TYPE, IconType } from '../Icon/Icon';
import { DARK, LIGHT, Theme, changeTheme } from '../../../libs/colors';

const ThemeSelector: FC = () => {
  const { theme, setTheme } = useThemeStateStore();

  const switchTheme = (): void => {
    const newTheme: Theme = theme === LIGHT ? DARK : LIGHT;
    setTheme(newTheme); // zustand
    changeTheme(newTheme); // colors library
  };

  const isLight = theme === LIGHT;
  const icon: IconType = isLight ? ICON_TYPE.LIGHT_MODE : ICON_TYPE.DARK_MODE;

  return (
    <div className="box theme-selector" onClick={switchTheme}>
      <div className={`switch ${isLight ? 'active' : ''}`}>
        <div className="slider">
          <Icon icon={icon} iconClassName="flex" />
        </div>
      </div>
    </div>
  );
};

export default ThemeSelector;
