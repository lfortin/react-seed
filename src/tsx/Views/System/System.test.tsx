import { describe, it } from 'vitest';
import { screen } from '@testing-library/react';
import System from './System';
import { renderWithProviders } from '../../../test-utils';

describe('System Component', () => {
  beforeEach(() => {
    renderWithProviders(<System />);
  });

  it('Renders system view', () => {
    const view = screen.getByTestId('system-view');
    expect(view).toBeDefined();
  });
});
