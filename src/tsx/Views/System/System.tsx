/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import './System.css';
import { useTitleStateStore } from '../../../store/Title';
import Input from '../../Components/generic/Input';
import Button from '../../Components/generic/Button';
import { KSESSION, get, set, remove, clear } from '../../../libs/localstorage';
import {
  UNKNWON,
  detectBrowser,
  detectBrowserVersion,
  detectOperatingSystem,
} from '../../../libs/system';

const System: FC = () => {
  const [value, setValue] = useState('');
  const [browser, setBrowser] = useState(UNKNWON);
  const [browserVersion, setBrowserVersion] = useState<number | undefined>(undefined);
  const [os, setOs] = useState(UNKNWON);
  const [storedValue, setStoredValue] = useState('');
  const { setTitle } = useTitleStateStore();

  useEffect(() => {
    setTitle('title.home');
    setBrowser(detectBrowser());
    setBrowserVersion(detectBrowserVersion());
    setOs(detectOperatingSystem());
  }, []);

  useEffect(() => {
    setStoredValue(get(KSESSION) ?? '');
  }, []);

  const setLocal = () => {
    set(KSESSION, value);
    setValue('');
    setStoredValue(get(KSESSION) ?? '');
  };

  const removeLocal = () => {
    remove(KSESSION);
    setValue('');
    setStoredValue(get(KSESSION) ?? '');
  };

  const setLocalClear = () => {
    clear();
    setValue('');
    setStoredValue(get(KSESSION) ?? '');
  };

  return (
    <div data-testid="system-view" className="sys-container">
      <h2>
        <span>
          <FormattedMessage id="system.ls" />
        </span>
      </h2>

      <div className="fields-container">
        <div className="field">
          <span className="header">
            <FormattedMessage id="system.stored" />
          </span>
          <span className="content">{storedValue}</span>
        </div>

        <div className="field">
          <span className="header">
            <FormattedMessage id="system.input" />
          </span>
          <Input
            className="content"
            placeholder={KSESSION}
            translatePh={false}
            value={value}
            onChange={setValue}
          />
        </div>

        <div className="field">
          <span className="header">
            <FormattedMessage id="system.actions" />
          </span>
          <div className="content actions">
            <Button className="reverse" onClick={removeLocal}>
              <span>
                <FormattedMessage id="system.clear" />
              </span>
            </Button>
            <Button onClick={setLocal}>
              <span>
                <FormattedMessage id="system.set" />
              </span>
            </Button>
          </div>
        </div>
      </div>
      <Button className="ls-clear-all" onClick={setLocalClear}>
        <span>
          <FormattedMessage id="system.clear_all" />
        </span>
      </Button>
      <h2 className="mt100">
        <span>
          <FormattedMessage id="system.env" />
        </span>
      </h2>
      <div className="system-container">
        <span data-testid="browser">
          <FormattedMessage id="system.browser" values={{ browser, browserVersion }} />
        </span>
        <span data-testid="os">
          <FormattedMessage id="system.os" values={{ os }} />
        </span>
      </div>
    </div>
  );
};

export default System;
