/* eslint-disable react-hooks/exhaustive-deps */
import { FC, memo, useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import './Home.css';
// redux
import { useTitleStateStore } from '../../../store/Title';
import { setHeroes } from '../../../actions/heroes';
import { fetchHeroes } from '../../../api/heroes';
import { StateInterface } from '../../../reducers';
import { selectHeroes } from '../../../selectors/heroes';
import { Hero } from '../../../models/heroes';
// components
import HeroListItem from '../../Components/Heroes/HeroListItem';
import VirtualList from '../../Components/generic/VirtualList';
import Spinner from '../../Components/Spinner';
import Searchbox from '../../Components/Searchbox';

const ListItem: FC<ListItemProps> = ({ instance }) => (
  <div className="item flex justify-center ">
    <NavLink to={`/hero/${instance.id}`}>
      <HeroListItem hero={instance} />
    </NavLink>
  </div>
);

interface ListItemProps {
  instance: Hero;
  key: number | string;
  _idx: number;
}

const EmptyList: FC = () => (
  <div className="h100 w100 flex-cc">
    <FormattedMessage id="home.empty" />
  </div>
);

const Home: FC<HomeProps> = ({ heroes }) => {
  const { setTitle } = useTitleStateStore();
  const [loading, setLoading] = useState(false);
  const [filterString, setFilter] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    setTitle('title.home');
    setLoading(true);
    fetchHeroes({ limit: 50 })
      .then((res) => {
        dispatch(setHeroes(res));
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, []);

  if (loading) {
    return (
      <div className="h100 w100 flex-cc">
        <Spinner size={3} />
      </div>
    );
  }

  const onSearch = (value: string): void => {
    console.log('onSearch : ', value);
    setFilter(value);
  };

  const RowComponent = (idx: number) => (
    <ListItem key={heroes[idx].id} instance={heroes[idx]} _idx={idx} />
  );

  return (
    <div data-testid="home-view" className="flex-col grow home-view">
      <Searchbox
        onSearch={onSearch}
        placeholder="search.home.placeholder"
        testId="home-searchbox"
        text={filterString}
        debounced
      />
      <p>
        <FormattedMessage id="home.title" values={{ count: heroes.length }} />
      </p>
      <VirtualList
        className="mb50"
        instances={heroes}
        testId="home-list"
        RowComponent={RowComponent}
        EmptyComponent={EmptyList}
      />
    </div>
  );
};

interface HomeProps {
  heroes: Hero[];
}

const mapStateToProps = (state: StateInterface): { heroes: Hero[] } => ({
  heroes: selectHeroes(state),
});

/**
 * @param {*} props
 * @param {*} nextProps
 * @returns true if component does not need to re-render
 */
const equalProps = (props: HomeProps, nextProps: HomeProps) =>
  props.heroes.length === nextProps.heroes.length;

export default connect(mapStateToProps)(memo(Home, equalProps));
