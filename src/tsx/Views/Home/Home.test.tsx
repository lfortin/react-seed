import { describe, it, vi } from 'vitest';
import { screen } from '@testing-library/react';
import Home from './Home';
import { setHeroes } from '../../../actions/heroes';
import { fetchHeroes } from '../../../api/heroes';
import { renderWithProviders } from '../../../test-utils';
import { mockedHero } from '../../../models/heroes.test';
import { Hero } from '../../../models/heroes';

const mockHeroesArray = [mockedHero(1), mockedHero(2), mockedHero(3)];

vi.mock('../../../actions/heroes', () => ({
  setHeroes: vi.fn((heroes: Hero[]) => ({
    type: 'SET_HEROES',
    payload: { values: heroes },
  })),
}));

vi.mock('../../../api/heroes', () => ({
  fetchHeroes: vi.fn(async () => Promise.resolve(mockHeroesArray)),
}));

vi.mock('../../../selectors/heroes', () => ({
  selectHeroes: vi.fn(() => mockHeroesArray),
}));

describe('Home Component', () => {
  beforeEach(() => {
    vi.clearAllMocks();
    renderWithProviders(<Home />);
  });

  describe('Component Layout', () => {
    it('Renders loading spinner initially', () => {
      const spinner = screen.getByTestId('spinner');
      expect(spinner).toBeDefined();
    });

    it('Displays heroes count after loading', async () => {
      // Wait for loading to finish
      await screen.findByText(/^Here are your (\d+) heroes$/);

      // Check that the loading spinner is not present
      const spinner = screen.queryByTestId('spinner');
      expect(spinner).toBeNull();

      // Check that the message 'Here are your 3 heroes' is displayed
      const title = screen.findByText('Here are your 3 heroes');
      expect(title).toBeDefined();
    });

    it('Displays heroes list after loading', async () => {
      // Wait for loading to finish
      await screen.findByText(/^Here are your (\d+) heroes$/);
      expect(screen.getByTestId('home-list')).toBeDefined();
    });
  });

  describe('First rendering', () => {
    it('Assert fetchHeroes() function has been called once during component initialization', async () => {
      // Wait for loading to finish
      await screen.findByText(/^Here are your (\d+) heroes$/);
      // Check that fetchHeroes is called once per rendering
      expect(fetchHeroes).toHaveBeenCalledTimes(1);
      expect(fetchHeroes).toHaveReturnedWith(mockHeroesArray);
    });

    // TO DO : improve test by testing dispatch function call instead of action
    it('Dispatches a setHeroes action with heroesApi.fetchHeroes response', async () => {
      // Wait for loading to finish
      await screen.findByText(/^Here are your (\d+) heroes$/);

      expect(setHeroes).toHaveBeenCalledWith(mockHeroesArray);
      expect(setHeroes).toHaveBeenCalledTimes(1);
    });
  });
});
