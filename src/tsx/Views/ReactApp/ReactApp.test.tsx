import { describe, it } from 'vitest';
import { screen } from '@testing-library/react';
import ReactApp from './ReactApp';
import { renderWithProviders } from '../../../test-utils';

describe('ReactApp Component', () => {
  beforeEach(() => {
    renderWithProviders(<ReactApp />);
  });

  it('Renders react-app view', () => {
    const view = screen.getByTestId('tip-view');
    expect(view).toBeDefined();
  });
});
