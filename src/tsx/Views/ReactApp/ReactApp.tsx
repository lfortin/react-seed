/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import './ReactApp.css';
import logo from '../../../assets/logo.svg';
import Button from '../../Components/generic/Button';
import Icon from '../../Components/Icon';
import { ICON_TYPE } from '../../Components/Icon/Icon';
import { useTitleStateStore } from '../../../store/Title';

const Link: FC<LinkProps> = ({ url, text }) => (
  <a className="link" href={url} target="_blank" rel="noopener noreferrer">
    <FormattedMessage id={text} />
  </a>
);

interface LinkProps {
  url: string;
  text: string;
}

const ReactApp: FC = () => {
  const { setTitle } = useTitleStateStore();
  useEffect(() => {
    setTitle('title.tip');
  }, []);

  const visit = (url: string) => {
    const newTab = window.open('', '_blank')!;
    newTab.location.href = url;
  };

  return (
    <div data-testid="tip-view" className="container">
      <p>
        <span>
          <FormattedMessage id="tip.line1" />
        </span>
        <Link url="https://vitejs.dev/" text="tip.vitejs" />
      </p>
      <img className="react-logo logo" src={logo} alt="logo" />
      <Link url="https://reactjs.org" text="tip.learn_react" />
      <p className="flex-col">
        <span>
          <FormattedMessage id="tip.line2" />
        </span>
        <span>
          <FormattedMessage id="tip.line3" />
        </span>
      </p>
      <div className="tips-links">
        <Button
          className="tip-buymeacoffee"
          onClick={() => visit('https://www.buymeacoffee.com/louisfortin')}
        >
          <FormattedMessage id="tip.buymeacoffee" />
          <Icon icon={ICON_TYPE.BEER} />
        </Button>
        <Button className="tip-kofi" onClick={() => visit('https://ko-fi.com/F1F5UTSRS')}>
          <FormattedMessage id="tip.kofi" />
          <Icon icon={ICON_TYPE.COFFEE} />
        </Button>
      </div>
    </div>
  );
};

export default ReactApp;
