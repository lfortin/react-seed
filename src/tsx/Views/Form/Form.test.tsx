import { describe, it } from 'vitest';
import { screen } from '@testing-library/react';
import Form from './Form';
import { renderWithProviders } from '../../../test-utils';

describe('Form Component', () => {
  beforeEach(() => {
    renderWithProviders(<Form />);
  });

  it('Renders form view', () => {
    const view = screen.getByTestId('form-view');
    expect(view).toBeDefined();
  });
});
