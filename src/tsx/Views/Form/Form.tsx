/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  useForm,
  Control,
  FieldErrors,
  FieldValues,
  RegisterOptions,
  SubmitHandler,
  UseFormSetValue,
  Validate,
} from 'react-hook-form';
import './Form.css';
// zustand
import { useTitleStateStore } from '../../../store/Title';
// components
import GenericForm, { Field } from '../../Components/generic/Form';
import Dropdown, { DropdownOption, SelectedOption } from '../../Components/generic/Dropdown';
import Button from '../../Components/generic/Button';
import Checkbox from '../../Components/generic/Checkbox';
import FileUpload from '../../Components/generic/FileUpload';
import Input from '../../Components/generic/Input';
import Switch from '../../Components/generic/Switch';
import DatePicker from '../../Components/DatePicker';
import Icon from '../../Components/Icon';
import { ICON_TYPE } from '../../Components/Icon/Icon';
import Spinner from '../../Components/Spinner';

type IFormInput = FieldValues & {
  firstName: string;
  email: string;
  emailConfirm: string;
  password: string;
  gender: { label: string; value: SelectedOption };
  files?: File[];
  date?: Date;
  optionalCheck?: boolean;
  mandatoryCheck: boolean;
  switch?: boolean;
};

const emailRegex = /^[\w.-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,}$/;
const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

interface IControllerPropsType {
  control: Control<IFormInput>;
  errors: FieldErrors<IFormInput>;
  setValue: UseFormSetValue<IFormInput>;
}

const Form: FC = () => {
  const { setTitle } = useTitleStateStore();
  const [visiblePwd, setVisible] = useState(false);
  const [isCalendarOpen, setCalOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const intl = useIntl();

  useEffect(() => {
    setTitle('title.home');
  }, []);

  const genderOptions: DropdownOption[] = [
    { value: 'M', label: intl.formatMessage({ id: 'form.gender.male' }) },
    { value: 'F', label: intl.formatMessage({ id: 'form.gender.female' }) },
    { value: 'U', label: intl.formatMessage({ id: 'form.gender.undefined' }) },
  ];

  const {
    control,
    formState: { isValid, errors },
    handleSubmit,
    reset,
    setValue,
    trigger,
  } = useForm<IFormInput>({
    defaultValues: {
      firstName: '',
      email: '',
      emailConfirm: '',
      password: '',
      gender: genderOptions[0],
      files: [],
      date: new Date(),
      optionalCheck: false,
      mandatoryCheck: false,
      switch: false,
    },
    mode: 'onChange',
  });

  const validateEmailConfirm: Validate<string, FieldValues> = (value, values) =>
    value === values.email || intl.formatMessage({ id: 'form.view.email.not_matching' });

  const registerOptions: Record<keyof IFormInput, RegisterOptions> = {
    firstName: {
      required: intl.formatMessage({ id: 'form.view.name.required' }),
    },
    email: {
      required: intl.formatMessage({ id: 'form.view.email.required' }),
      pattern: {
        value: emailRegex,
        message: intl.formatMessage({ id: 'form.view.email.pattern' }),
      },
    },
    emailConfirm: {
      required: intl.formatMessage({ id: 'form.view.email.required' }),
      validate: validateEmailConfirm,
    },
    password: {
      required: intl.formatMessage({ id: 'form.view.password.required' }),
      minLength: {
        value: 8,
        message: intl.formatMessage({ id: 'form.view.password.minLength' }),
      },
      pattern: {
        value: passwordRegex,
        message: intl.formatMessage({ id: 'form.view.password.pattern' }),
      },
    },
    mandatoryCheck: {
      required: intl.formatMessage({ id: 'form.required' }),
    },
  };

  const formatData = (data: IFormInput) => ({
    ...data,
    gender: data.gender.value,
  });

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    setLoading(true);
    setTimeout(() => {
      console.log(formatData(data));
      setLoading(false);
      reset(undefined, { keepErrors: false, keepDirty: false, keepTouched: false });
    }, 1000);
  };

  const today = new Date();
  const minDate = today;
  const maxDate = new Date();
  maxDate.setFullYear(today.getFullYear() + 1);

  const inputPwdType = visiblePwd ? 'text' : 'password';
  const pwdIcon = visiblePwd ? ICON_TYPE.EYE : ICON_TYPE.EYE_OFF;

  const onPwdIconClick = () => setVisible(!visiblePwd);

  const controllerProps: IControllerPropsType = { control, errors, setValue };

  return (
    <div data-testid="form-view" className="flex-col w100">
      <p>
        <FormattedMessage id="form.title" />
      </p>
      <GenericForm<IFormInput>
        className="view-form"
        onSubmit={handleSubmit(onSubmit)}
        reset={reset}
      >
        <Field<IFormInput>
          name="firstName"
          className="app-input"
          type="text"
          trigger={trigger}
          Component={Input}
          registerOptions={registerOptions}
          componentProps={{ placeholder: 'form.placeholder.firstName' }}
          getComponentProps={(field) => ({ value: field.value })}
          {...controllerProps}
        />
        <Field<IFormInput>
          name="email"
          className="app-input"
          type="text"
          trigger={trigger}
          Component={Input}
          registerOptions={registerOptions}
          componentProps={{ placeholder: 'form.placeholder.email' }}
          getComponentProps={(field) => ({ value: field.value })}
          {...controllerProps}
        />
        <Field<IFormInput>
          name="emailConfirm"
          className="app-input"
          type="text"
          trigger={trigger}
          Component={Input}
          registerOptions={registerOptions}
          componentProps={{ placeholder: 'form.placeholder.emailConfirm' }}
          getComponentProps={(field) => ({ value: field.value })}
          {...controllerProps}
        />
        <Field<IFormInput>
          name="password"
          className="app-input"
          type="text"
          trigger={trigger}
          Component={Input}
          registerOptions={registerOptions}
          componentProps={{ type: inputPwdType, placeholder: 'form.placeholder.password' }}
          getComponentProps={(field) => ({ value: field.value })}
          iconComponent={
            <div onClick={onPwdIconClick}>
              <Icon icon={pwdIcon} />
            </div>
          }
          {...controllerProps}
        />
        <Field<IFormInput>
          name="gender"
          className="app-selector"
          type="dropdown"
          Component={Dropdown}
          registerOptions={registerOptions}
          componentProps={{ options: genderOptions }}
          getComponentProps={(field) => ({ selectedOption: (field.value as DropdownOption).value })}
          {...controllerProps}
        />
        <Field<IFormInput>
          name="files"
          className="app-file-selector"
          type="files"
          Component={FileUpload}
          registerOptions={registerOptions}
          getComponentProps={(field) => ({ validFiles: field.value as File[] })}
          {...controllerProps}
        />
        <Field<IFormInput>
          name="date"
          className="app-input"
          type="date"
          Component={DatePicker}
          registerOptions={registerOptions}
          componentProps={{
            maxDate,
            minDate,
            showCalendarIcon: false,
            isOpen: isCalendarOpen,
            setOpen: setCalOpen,
          }}
          getComponentProps={(field) => ({ selectedDate: field.value as Date })}
          iconComponent={
            <div
              data-testid="date-picker-icon"
              className="icon h100 flex-cc"
              onClick={() => (isCalendarOpen ? null : setCalOpen(true))}
            >
              <Icon icon={ICON_TYPE.CALENDAR} iconClassName="flex-cc" />
            </div>
          }
          {...controllerProps}
        />
        <Field<IFormInput>
          name="optionalCheck"
          className="app-checkbox"
          type="boolean"
          trigger={trigger}
          Component={Checkbox}
          registerOptions={registerOptions}
          getComponentProps={(field) => ({ checked: field.value as boolean })}
          {...controllerProps}
        />
        <Field<IFormInput>
          name="mandatoryCheck"
          className="app-checkbox"
          type="boolean"
          trigger={trigger}
          Component={Checkbox}
          registerOptions={registerOptions}
          getComponentProps={(field) => ({ checked: field.value as boolean })}
          {...controllerProps}
        />
        <Field<IFormInput>
          name="switch"
          className="app-checkbox"
          type="boolean"
          trigger={trigger}
          Component={Switch}
          registerOptions={registerOptions}
          getComponentProps={(field) => ({ checked: field.value as boolean })}
          {...controllerProps}
        />
        <div className="flex-cc">
          <Button type="submit" disabled={loading || !isValid}>
            <span>{intl.formatMessage({ id: 'form.send' })}</span>
            <Icon icon={ICON_TYPE.SEND} />
          </Button>
          {loading && <Spinner size={1} />}
        </div>
      </GenericForm>
    </div>
  );
};

export default Form;
