import { describe, it } from 'vitest';
import { screen } from '@testing-library/react';
import Notification from './Notification';
import { renderWithProviders } from '../../../test-utils';

describe('Notification Component', () => {
  beforeEach(() => {
    renderWithProviders(<Notification />);
  });

  it('Renders notifications view', () => {
    const view = screen.getByTestId('notification-view');
    expect(view).toBeDefined();
  });
});
