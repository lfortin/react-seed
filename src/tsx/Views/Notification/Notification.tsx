/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import './Notification.css';
import { useTitleStateStore } from '../../../store/Title';
// components
import Button from '../../Components/generic/Button';
import { notify, success, warn, error, system } from '../../../libs/Notifier';

const Notification: FC = () => {
  const { setTitle } = useTitleStateStore();
  useEffect(() => {
    setTitle('title.home');
  }, []);

  const notif = () => notify('notification.message');
  const succesNotif = () => success('notification.message');
  const warningNotif = () => warn('notification.message');
  const errorNotif = () => error('notification.message');
  const systemNotif = () => system('notification.system.title', 'notification.system.message');

  return (
    <div data-testid="notification-view" className="flex-col w100">
      <p>
        <FormattedMessage id="notification.title" />
      </p>
      <p>
        <FormattedMessage id="notification.push" />
      </p>
      <div className="notif-type">
        <Button onClick={notif} className="notify-button ">
          <span>
            <FormattedMessage id="notification.button.notify" />
          </span>
        </Button>
        <Button onClick={succesNotif} className="success-button ">
          <span>
            <FormattedMessage id="notification.button.success" />
          </span>
        </Button>
        <Button onClick={warningNotif} className="warning-button ">
          <span>
            <FormattedMessage id="notification.button.warning" />
          </span>
        </Button>
        <Button onClick={errorNotif} className="error-button ">
          <span>
            <FormattedMessage id="notification.button.error" />
          </span>
        </Button>
      </div>
      <p>
        <FormattedMessage id="notification.system" />
      </p>
      <div className="notif-type">
        <Button onClick={systemNotif} className="notify-button ">
          <span>
            <FormattedMessage id="notification.button.notify" />
          </span>
        </Button>
      </div>
    </div>
  );
};

export default Notification;
