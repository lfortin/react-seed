import { describe, it, vi } from 'vitest';
import { Location } from 'react-router-dom';
import { screen } from '@testing-library/react';
import HeroContainer from './HeroContainer';
import { setHero } from '../../../actions/heroes';
import { fetchHero } from '../../../api/heroes';
import { renderWithProviders } from '../../../test-utils';
import { mockedHero } from '../../../models/heroes.test';
import { Hero } from '../../../models/heroes';

const mockId = '1';

const mockLocation: Location = {
  pathname: `/hero/${mockId}`,
  state: {},
  key: '',
  search: '',
  hash: '',
};

const mockHero = mockedHero(Number(mockId));

vi.mock('../../../actions/heroes', () => ({
  setHero: vi.fn((id: string, value: Hero) => ({
    type: 'SET_HERO',
    payload: { id, value },
  })),
}));

vi.mock('../../../api/heroes', () => ({
  fetchHero: vi.fn(async () => Promise.resolve(mockHero)),
}));

vi.mock('../../../selectors/heroes', () => ({
  selectHero: vi.fn(() => mockHero),
}));

describe('HeroContainer Component', () => {
  beforeEach(() => {
    vi.clearAllMocks();
    renderWithProviders(<HeroContainer location={mockLocation} />);
  });

  describe('Component Layout', () => {
    it('Renders Hero view', () => {
      const view = screen.getByTestId('hero-view');
      expect(view).toBeDefined();
    });

    it('Contains back button', () => {
      const btn = screen.getByTestId('back-btn');
      expect(btn).toBeDefined();
    });

    it('Contains Hero card', () => {
      const card = screen.getByTestId('hero-card');
      expect(card).toBeDefined();
    });
  });

  describe('First rendering', () => {
    it('Assert fetchHero(id) function has been called once during component initialization', () => {
      screen.getByTestId('hero-view');
      // Check that fetchHero is called once per rendering
      expect(fetchHero).toHaveBeenCalledTimes(1);
      expect(fetchHero).toHaveBeenCalledWith(mockId);
      expect(fetchHero).toHaveReturnedWith(mockHero);
    });

    // TO DO : improve test by testing dispatch function call instead of action
    it('Dispatches a setHero action with heroesApi.fetchHero response', () => {
      screen.getByTestId('hero-view');
      expect(fetchHero).toHaveReturnedWith(mockHero);
      expect(setHero).toHaveBeenCalledTimes(1);
      expect(setHero).toHaveBeenCalledWith(mockId, mockHero);
    });
  });
});
