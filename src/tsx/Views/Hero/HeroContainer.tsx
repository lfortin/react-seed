/* eslint-disable react-hooks/exhaustive-deps */
import { FC, memo, useEffect } from 'react';
import { connect, useDispatch } from 'react-redux';
import { Location } from 'react-router-dom';
import './HeroContainer.css';
// redux
import { setHero } from '../../../actions/heroes';
import { fetchHero } from '../../../api/heroes';
import { StateInterface } from '../../../reducers';
import { selectHero } from '../../../selectors/heroes';
import { useTitleStateStore } from '../../../store/Title';
import { Hero } from '../../../models/heroes';
// components
import HeroComponent from '../../Components/Heroes/Hero';
import Icon from '../../Components/Icon';
import { ICON_TYPE } from '../../Components/Icon/Icon';
// libs
import { history } from '../../../libs/utils/navigation';

const getId = (location: Location): string => location.pathname.split('/').at(-1)!;

const HeroContainer: FC<Props> = ({ hero, location }) => {
  const dispatch = useDispatch();
  const { setTitle } = useTitleStateStore();

  useEffect(() => {
    const id: string = getId(location);
    setTitle('title.home');
    void fetchHero(id).then((hero) => {
      dispatch(setHero(id, hero));
    });
  }, [location]);

  if (!hero) return null;

  const onBackClick = (): void => {
    if (history.navigate) history.navigate('/home');
  };

  return (
    <div data-testid="hero-view" className="hero-container">
      <div data-testid="back-btn" className="back-button" onClick={onBackClick}>
        <Icon icon={ICON_TYPE.BACK} />
      </div>
      <HeroComponent hero={hero} />
    </div>
  );
};

interface Props {
  hero?: Hero;
  location: Location;
}

const mapStateToProps = (state: StateInterface, props: Props) => ({
  hero: selectHero(state, { id: getId(props.location) }),
});

/**
 * @param {*} props
 * @param {*} nextProps
 * @returns true if component does not need to re-render
 */
const equalProps = (props: Props, nextProps: Props): boolean =>
  !!(props.hero && nextProps.hero && props.hero.id === nextProps.hero.id);

export default connect(mapStateToProps)(memo(HeroContainer, equalProps));
