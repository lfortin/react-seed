import { FC } from 'react';
import { Location } from 'react-router-dom';
import { TabView } from '../../Components/generic/Tabs';
import { TABS } from '../../Components/Tab/constants';
import { TabType, TabsType } from '../../Components/generic/Tabs/constants';

interface TabsDataType {
  message: string;
}

const TabbedContainer: FC<Props> = ({ location }) => {
  const defaultTab = TABS.TAB_ALPHA;

  const getTab = (location: Location, TABS: TabsType): TabType => {
    const rootUrl = `/${location.pathname.split('/')[1]}`;
    return Object.values(TABS).find((t) => t.url === rootUrl) ?? defaultTab;
  };

  const activeTab = getTab(location, TABS);

  const tabsData: Record<number, TabsDataType> = {
    1: { message: 'toto' },
    2: { message: 'tata' },
    3: { message: 'titi' },
  };

  return (
    <div data-testid="tab-view" className="flex-col grow">
      <TabView
        className="tab-view flex-column overflow-hidden"
        defaultTab={defaultTab.name}
        disabledTabs={[3]}
        getTabFunc={getTab}
        location={location}
        tabsProps={tabsData[activeTab.key]}
        view="tabbed"
        TABS={TABS}
      />
    </div>
  );
};

interface Props {
  location: Location;
}

export default TabbedContainer;
