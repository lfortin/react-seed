import { describe, it } from 'vitest';
import { Location, MemoryRouter } from 'react-router-dom';
import { screen } from '@testing-library/react';
import TabbedContainer from './TabbedContainer';
import { renderWithProviders } from '../../../test-utils';

const mockLocation: Location = {
  pathname: '/tab-alpha',
  state: {},
  key: '',
  search: '',
  hash: '',
};

describe('Tabbed Component', () => {
  beforeEach(() => {
    renderWithProviders(
      <MemoryRouter initialEntries={['/tab-alpha']}>
        <TabbedContainer location={mockLocation} />
      </MemoryRouter>
    );
  });

  it('Renders tab view', () => {
    expect(screen.getByTestId('tab-view')).toBeDefined();
  });
});
