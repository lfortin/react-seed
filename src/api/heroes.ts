import { Hero } from '../models/heroes';
import { ApiResponseType, get } from '../libs/requests';

interface QueryParams {
  limit?: number;
  offset?: number;
  name?: string;
}

export const getQueryParams = (params: QueryParams = {}) => {
  let rep = '';
  if (params.limit) rep += `&limit=${params.limit}`;
  if (params.offset) rep += `&offset=${params.offset}`;
  if (params.name) rep += `&nameStartsWith=${params.name}`;
  return rep;
};

export const fetchHeroes = async (params?: QueryParams): Promise<Hero[]> => {
  type T = ApiResponseType<Hero[]>;
  const data = (await get<T>('/characters', getQueryParams(params))) as T;
  return data.results;
};

export const fetchHero = async (id: string): Promise<Hero> => {
  type T = ApiResponseType<Hero[]>;
  const data = (await get<T>(`/characters/${id}`, '')) as T;
  return data.results[0];
};
