 
import { describe, it, vi } from 'vitest';
import { fetchHeroes, fetchHero, getQueryParams } from './heroes';
import { mockedHero } from '../models/heroes.test';
import { get } from '../libs/requests';

vi.mock('../libs/requests');
describe('Heroes API Functions', () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  describe('fetchHeroes', () => {
    const mockData = [mockedHero(1), mockedHero(2)];

    it('Should fetch heroes and dispatch setHeroes action', async () => {
      vi.mocked(get).mockResolvedValue({ results: mockData });
      const rep = await fetchHeroes();

      expect(get).toHaveBeenCalledWith('/characters', '');
      expect(rep).toBe(mockData);
    });
  });

  describe('fetchHero', () => {
    const mockData = mockedHero(3);
    const heroId = '3';

    it('Should fetch a hero by id and dispatch setHero action', async () => {
      vi.mocked(get).mockResolvedValue({ results: [mockData] });
      const rep = await fetchHero(heroId);

      expect(get).toHaveBeenCalledWith(`/characters/${heroId}`, '');
      expect(rep).toBe(mockData);
    });
  });

  describe('getQueryParams', () => {
    it('Empty parameters should return an empty string', () => {
      const params = {};
      const res = getQueryParams(params);
      expect(res).toBe('');
    });

    it('limit parameter should be in the response', () => {
      const parameter = 'limit';
      const value = 50;
      const params = { [parameter]: value };
      const res = getQueryParams(params);
      expect(res).toBe(`&limit=${value}`);
    });

    it('offset parameter should be in the response', () => {
      const parameter = 'offset';
      const value = 10;
      const params = { [parameter]: value };
      const res = getQueryParams(params);
      expect(res).toBe(`&offset=${value}`);
    });

    it('name parameter should be in the response', () => {
      const parameter = 'name';
      const value = 'mocked-name';
      const params = { [parameter]: value };
      const res = getQueryParams(params);
      expect(res).toBe(`&nameStartsWith=${value}`);
    });

    it('Multiple parameters should be aggregated in the response', () => {
      const params = { limit: 20, name: 'mocked-name', offset: 20 };
      const res = getQueryParams(params);
      expect(res).toBe(`&limit=20&offset=20&nameStartsWith=mocked-name`);
    });

    it('Other parameters should not impact the response', () => {
      const params = { offset: 20, name: 'mocked-name', limit: 20, foo: 'bar', bar: false };
      const res = getQueryParams(params);
      expect(res).toBe(`&limit=20&offset=20&nameStartsWith=mocked-name`);
    });
  });
});
