import { createSelector } from 'reselect';
import { HERO } from '../constants/reduxTypes';
import { StateInterface } from '../reducers';

interface IProps {
  id: string;
}

const _heroes = (state: StateInterface) => state[HERO].collection;
const _id = (_state: StateInterface, props: IProps) => props.id;

export const selectHeroes = createSelector(_heroes, (heroes) => Object.values(heroes));
export const selectHero = createSelector([_heroes, _id], (heroes, id) => heroes[id]);
