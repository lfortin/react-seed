import { selectHeroes, selectHero } from './heroes';
import { StateInterface } from '../reducers';
import { Hero } from '../models/heroes';

const mockedMedia = {
  available: 0,
  collectionURI: 'http://mocked/collection/uri',
  items: [
    {
      ressourceURI: 'http://mocked/ressource/uri',
      name: 'mocked-name',
    },
  ],
  returned: 0,
};
const mockedUrl = { type: 'wiki', url: 'http:/wiki/mocked/url/1' };

const mockedHero = (id: number): Hero => ({
  id,
  name: `Mocked Hero ${id}`,
  description: `Mocked Hero description ${id}`,
  modified: '',
  thumbnail: {
    path: 'https://images.unsplash.com/photo-1712743072516-13b19bc38840?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    extension: 'jpg',
  },
  ressourceURI: 'http://mocked/uri/1',
  comics: [mockedMedia],
  series: [mockedMedia],
  stories: [mockedMedia],
  events: [mockedMedia],
  urls: [mockedUrl],
});

const mockState: StateInterface = {
  tooltips: {},
  heroes: {
    collection: {
      '1': mockedHero(1),
      '2': mockedHero(2),
      '3': mockedHero(3),
    },
  },
};

describe('Heroes Selectors', () => {
  describe('selectHeroes', () => {
    test('Should return all heroes', () => {
      const heroes = selectHeroes(mockState);
      expect(heroes).toHaveLength(3);
      expect(heroes).toEqual(expect.arrayContaining([mockState.heroes.collection['1']]));
      expect(heroes).toEqual(expect.arrayContaining([mockState.heroes.collection['2']]));
      expect(heroes).toEqual(expect.arrayContaining([mockState.heroes.collection['3']]));
    });
  });

  describe('selectHero', () => {
    test('Should return the selected hero', () => {
      const heroId = '2';
      const selectedHero = selectHero(mockState, { id: heroId });
      expect(selectedHero).toEqual(mockState.heroes.collection['2']);
    });

    test('Should return undefined if hero does not exist', () => {
      const heroId = '4';
      const selectedHero = selectHero(mockState, { id: heroId });
      expect(selectedHero).toBeUndefined();
    });
  });
});
