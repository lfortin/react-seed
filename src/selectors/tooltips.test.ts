import { describe, it } from 'vitest';
import { getTooltips, getVisibleTooltip } from './tooltips';
import { StateInterface } from '../reducers';
import { DimensionsType } from '../models/tooltips';

const mockedDimension: DimensionsType = {
  top: 100,
  left: 200,
  width: 50,
  height: 20,
  bottom: 120,
  right: 250,
  x: 0,
  y: 0,
};

const mockState: StateInterface = {
  tooltips: {
    tooltip1: {
      id: 'tooltip1',
      text: 'Tooltip 1',
      visible: false,
      dimensions: mockedDimension,
      direction: 'TOP',
    },
    tooltip2: {
      id: 'tooltip2',
      text: 'Tooltip 2',
      visible: true,
      dimensions: mockedDimension,
      direction: 'TOP',
    },
    tooltip3: {
      id: 'tooltip3',
      text: 'Tooltip 3',
      visible: false,
      dimensions: mockedDimension,
      direction: 'TOP',
    },
  },
  heroes: {
    collection: {},
  },
};

const noVisibleState = {
  ...mockState,
  tooltips: {
    ...mockState.tooltips,
    tooltip2: {
      ...mockState.tooltips.tooltip2,
      visible: false,
    },
  },
};

describe('Tooltip Selectors', () => {
  describe('getTooltips', () => {
    it('Should return all tooltips', () => {
      const tooltips = getTooltips(mockState);
      expect(tooltips).toHaveLength(3);
      expect(tooltips).toEqual(expect.arrayContaining([mockState.tooltips.tooltip1]));
      expect(tooltips).toEqual(expect.arrayContaining([mockState.tooltips.tooltip2]));
      expect(tooltips).toEqual(expect.arrayContaining([mockState.tooltips.tooltip3]));
    });
  });

  describe('getVisibleTooltip', () => {
    it('Should return the visible tooltip', () => {
      const visibleTooltip = getVisibleTooltip(mockState);
      expect(visibleTooltip).toEqual(mockState.tooltips.tooltip2);
    });

    it('Should return undefined if no tooltip is visible', () => {
      const visibleTooltip = getVisibleTooltip(noVisibleState);
      expect(visibleTooltip).toBeUndefined();
    });
  });
});
